﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Data.ResourceMaterials {

    [System.Serializable]
    public class ElementCost {

        [SerializeField]
        int[] _costs;

        public int[] Costs { get { return _costs; } }
    }
}