﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using RTS.Data.ResourceMaterials;
using RTS.Game;

namespace RTS.UtilEditor {

    [CustomPropertyDrawer(typeof(ElementCost))]
    public class E_ElementCost : PropertyDrawer {

        [SerializeField]
        bool foldoutVal = true;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            //base.OnGUI(position, property, label);

            Rect rect = new Rect(position);
            rect.height = 18;

            //foldoutVal = EditorGUI.Foldout(rect, foldoutVal, new GUIContent(property.displayName));
            foldoutVal = EditorGUI.BeginFoldoutHeaderGroup(rect, foldoutVal, property.displayName);

            if(foldoutVal) {
                SerializedProperty costsArray = property.FindPropertyRelative("_costs");
                costsArray.arraySize = GameDataMng.MaterialResourcesCount;

                rect.position += Vector2.up * 20 + Vector2.right * 20;
                rect.size -= Vector2.right * 50;


                for(int i = 0; i < costsArray.arraySize; i++) {
                    EditorGUI.PropertyField(rect, costsArray.GetArrayElementAtIndex(i), new GUIContent(GameDataMng.getMaterialResourceName(i)));

                    rect.position += Vector2.up * 20;
                }
            }

            EditorGUI.EndFoldoutHeaderGroup();
        }
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            return 50 + property.FindPropertyRelative("_costs").arraySize * 20;
        }
    }
}