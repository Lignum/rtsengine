﻿using RTS.Game;
using RTS.Game.Entities;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Data.Menus {
    public abstract class MenuButtonData : ScriptableObject {

        public string buttonName;

        public abstract void execute(HashSet<RTSTeamEntity> casters);

    }
}