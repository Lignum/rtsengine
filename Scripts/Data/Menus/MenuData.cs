﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Data.Menus {

    [CreateAssetMenu(fileName = "MenuData", menuName = "RTS/Menus/MenuData", order = 1)]
    public class MenuData : ScriptableObject {

        public MenuButtonData[] buttons;

    }
}