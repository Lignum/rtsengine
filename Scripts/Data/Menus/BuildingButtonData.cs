﻿using System.Collections.Generic;
using RTS.Buildings;
using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using UnityEngine;

namespace RTS.Data.Menus {

    [CreateAssetMenu(fileName = "BuildingButton", menuName = "RTS/Menus/BuildingButton", order = 1)]
    public class BuildingButtonData : MenuButtonData {

        [SerializeField]
        RTSBuilding buildingPrefab;

        HashSet<RTSTeamEntity> _casters;

        public override void execute(HashSet<RTSTeamEntity> casters) {
            _casters = casters;
            Player.RTSPlayerCtrl.INSTANCE.GetBuilder().setBuilding(buildingPrefab, createJob);
        }

        void createJob(HashSet<BuildingOrder> placedOrders) {

            foreach(var item in _casters) {
                (item as RTSAgent).setJob(new Jobs.BuildJob(placedOrders));
            }

            _casters = null;
        }
    }
}