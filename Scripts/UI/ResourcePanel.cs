﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace RTS.UI {
    public class ResourcePanel : MonoBehaviour {

        [SerializeField]
        TextMeshProUGUI[] _resourcesText;

        int[] resources;

        private void Start() {
            resources = Player.RTSPlayerCtrl.getTeam().Resources;
        }

        private void Update() {
            updateResources();
        }

        public void updateResources() {

            for(int i = 0; i < resources.Length; i++) {
                _resourcesText[i].text = resources[i].ToString();
            }
            
        }

    }

}