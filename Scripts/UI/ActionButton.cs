﻿using RTS.Data.Menus;
using RTS.Game.Entities;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace RTS.UI {
    public class ActionButton : MonoBehaviour {

        MenuButtonData _data;
        HashSet<RTSTeamEntity> _casters;

        public void setData(MenuButtonData data, HashSet<RTSTeamEntity> casters) {

            _data = data;
            _casters = casters;

            TextMeshProUGUI nameText = GetComponentInChildren<TextMeshProUGUI>();

            nameText.text = data.buttonName;
        }

        public void onClickAction() {
            _data.execute(_casters);
        }

    }
}