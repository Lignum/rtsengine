﻿using RTS.AI;
using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using RTS.Jobs;
using RTS.Player;
using RTS.UI.Elements;
using Shared.Collections.Pools;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.UI.Elements {
    public class EntityUICtrl : MonoBehaviour {

        [SerializeField]
        EntityUIDisplay _entityDisplayPre;
        [SerializeField]
        JobUIDisplay _jobDisplayPre;

        static SimpleMonoBehaviourPool<EntityUIDisplay> _entityDisplayPool;
        static SimpleMonoBehaviourPool<JobUIDisplay> _jobDisplayPool;

        static HashSet<AgentJobUICtrl> _jobsDisplays;

        private void Awake() {
            _entityDisplayPool = new SimpleMonoBehaviourPool<EntityUIDisplay>(_entityDisplayPre, transform, 20);
            _jobDisplayPool = new SimpleMonoBehaviourPool<JobUIDisplay>(_jobDisplayPre, transform, 5);

            _jobsDisplays = new HashSet<AgentJobUICtrl>();

            RTSPlayerCtrl.addOnEntitiesSelected(showEntitiesDisplay);
            RTSPlayerCtrl.addOnEntitiesSelected(showJobsDisplays);


            RTSPlayerCtrl.addOnEntitiesCleared(clearEntitiesDisplay);
            RTSPlayerCtrl.addOnEntitiesCleared(clearJobsDisplays);

            StartCoroutine(timedUpdate());
        }
        

        void showEntitiesDisplay(HashSet<RTSTeamEntity> entities) {

            clearEntitiesDisplay();

            EntityUIDisplay display;
            foreach(var entity in entities) {
                display = _entityDisplayPool.spawn();
                display.setTarget(entity);
            }
        }

        void showJobsDisplays(HashSet<RTSTeamEntity> entities) {

            clearJobsDisplays();

            A_AgentAI ai;
            foreach(var entity in entities) {
                ai = (entity as RTSAgent)?.getAI();
                
                if(ai != null) {
                    _jobsDisplays.Add(new AgentJobUICtrl(ai));
                }
            }
        }

        void clearEntitiesDisplay() {
            _entityDisplayPool.despawnAll();
        }

        void clearJobsDisplays() {

            foreach(var item in _jobsDisplays) {
                item.clear();
            }

            _jobsDisplays.Clear();
        }

        System.Collections.IEnumerator timedUpdate() {
            while(true) {

                yield return new WaitForSeconds(1);

                foreach(var item in _jobsDisplays) {
                    item.checkState();
                }
            }
        }



        public static void deSpawnEntityUI(EntityUIDisplay ui) {
            _entityDisplayPool.despawn(ui);
        }
        public static JobUIDisplay spawnJobUIDisplay() {
            return _jobDisplayPool.spawn();
        }
    }
}