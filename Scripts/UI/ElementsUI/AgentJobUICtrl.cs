﻿using RTS.AI;
using RTS.Game.Entities;
using RTS.Jobs;
using Shared.Collections.Pools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.UI.Elements {
    public class AgentJobUICtrl {

        A_AgentAI _worker;

        Job _currentJob;

        JobUIDisplay _display;
        RTSEntity _currentTarget;

        static Dictionary<RTSEntity, bool[]> _targetsCount = new Dictionary<RTSEntity, bool[]>(10);

        public AgentJobUICtrl(A_AgentAI worker) {
            _worker = worker;

            checkState();
        }

        public void checkState() {

            if(_worker.getJob() != _currentJob) {
                _currentJob = _worker.getJob();
                clear();
            }

            if(_currentJob != null && _currentJob.getTarget() != null) {
                if(_currentTarget != _currentJob.getTarget()) {

                    clearDisplay();

                    _currentTarget = _currentJob.getTarget();

                    if(_currentTarget != null) {

                        int pos = getNextTargetPosition(_currentTarget);

                        if(pos >= 0) {

                            _display = EntityUICtrl.spawnJobUIDisplay();
                            _display.setTarget(_currentTarget, pos);

                        } else {
                            clear();
                        }
                    }
                }


            } else {
                clear();
            }

        }

        string testTargetCounts(RTSEntity target) {
            string s = "";

            foreach(var item in _targetsCount[target]) {
                if(item) {
                    s += 1;
                } else {
                    s += 0;
                }

                s += ",";
            }

            return s;

        }

        int getNextTargetPosition(RTSEntity target) {
            if(_targetsCount.ContainsKey(target)) {
                
                for(int i = 0; i < _targetsCount[target].Length; i++) {
                    if(_targetsCount[target][i] == false) {
                        _targetsCount[target][i] = true;
                        return i;
                    }
                }

                return -1;

            } else {
                _targetsCount.Add(target, new bool[JobUIDisplay.MAX_DISPLAYS_PER_ENTITY]);
                _targetsCount[target][0] = true;
                return 0;
            }
        }

        void removeTargetCount(RTSEntity target, int pos) {
            if(_targetsCount.ContainsKey(target)) {

                _targetsCount[target][pos] = false;

                foreach(var item in _targetsCount[target]) {
                    if(item == true) {
                        return;
                    }
                }

                _targetsCount.Remove(target);
            }
        }

        void clearDisplay() {
            if(_display != null) {
                removeTargetCount(_display.Target, _display.Position);
                _display.gameObject.SetActive(false);
                _display = null;
            }
        }

        public void clear() {

            clearDisplay();

            _currentTarget = null;
        }
    }
}