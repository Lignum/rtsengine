﻿using RTS.CameraCtrls;
using RTS.Game;
using RTS.Game.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.UI.Elements {
    public class ElementUIDisplay : MonoBehaviour {

        protected const float CAMERA_DISTANCE = 25;

        public RTSEntity Target { get; private set; }

        public virtual void setTarget(RTSEntity target) {
            Target = target;
        }

        protected virtual void checkIfActive() {
            if(Target == null || !Target.gameObject.activeInHierarchy) {
                gameObject.SetActive(false);
            }
        }

        protected virtual void LateUpdate() {

            checkIfActive();

            setPosition();

            lookToCamera();
        }

        protected virtual void setPosition() {
            transform.position = MouseCtrl.worldToScreenPos(Target.Position + Vector3.up * RTSGameCtrl.TileHeight);
        }

        protected void lookToCamera() {
            transform.localScale = Vector3.one * CAMERA_DISTANCE / Vector3.Distance(RTSCameraController.INSTANCE.transform.position, Target.Position);
        }
    }
}