﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RTS.UI.Elements {

    [ExecuteInEditMode]
    public class EntityUILife : MonoBehaviour {

        [SerializeField]
        RectTransform _leftImage, _rightImage;

        [SerializeField, Min(0.1f)]
        float _maxValue = 1;
        public float MaxValue {
            get { return _maxValue; }
            set {
                _maxValue = value;
                updateLife();
            }
        }

        [SerializeField]
        float _value;
        public float Value {
            get { return _value; }
            set {
                _value = value;
                updateLife();
            }
        }

        Vector2 _maxSize = Vector3.one;
        Vector2 _minSize = Vector3.zero;


        void updateLife() {
            _maxSize.x = _value / _maxValue;
            _minSize.x = _maxSize.x;

            _leftImage.anchorMax = _maxSize;
            _rightImage.anchorMin = _minSize;
        }

        private void OnValidate() {
            _value = Mathf.Clamp(_value, 0, _maxValue);
            updateLife();
        }
    }
}