﻿using RTS.CameraCtrls;
using RTS.Game.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RTS.UI.Elements {
    public class EntityUIDisplay : ElementUIDisplay {
        
        [SerializeField]
        EntityUILife _lifeSlider;

        public override void setTarget(RTSEntity target) {
            base.setTarget(target);

            _lifeSlider.MaxValue = Target.EntityData.MaxLife;
        }

        protected override void LateUpdate() {
            base.LateUpdate();

            _lifeSlider.Value = Target.Life;
        }
    }
}