﻿using RTS.Game;
using RTS.Game.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.UI.Elements {
    public class JobUIDisplay : ElementUIDisplay {

        const float HEIGHT_DISTANCE = .25f;
        public const int MAX_DISPLAYS_PER_ENTITY = 6;

        public int Position { get; private set; }

        public void setTarget(RTSEntity target, int position) {
            base.setTarget(target);
            Position = position;
        }

        protected override void setPosition() {
            transform.position = MouseCtrl.worldToScreenPos(Target.Position + Vector3.up * RTSGameCtrl.TileHeight
                + Vector3.up * (Position - MAX_DISPLAYS_PER_ENTITY / 2) * HEIGHT_DISTANCE);
        }
    }
}