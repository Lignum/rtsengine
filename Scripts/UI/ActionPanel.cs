﻿using RTS.AI;
using RTS.Data.Menus;
using RTS.Game;
using RTS.Game.Entities;
using RTS.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RTS.UI {
    public class ActionPanel : MonoBehaviour {

        public static ActionPanel INSTANCE;

        [SerializeField]
        ActionButton buttonPre;

        Dictionary<MenuData, ActionGroup> _menuDatas;

        Shared.Collections.Pools.SimpleMonoBehaviourPool<ActionButton> _buttonPool;

        private void Awake() {
            INSTANCE = this;

            _buttonPool = new Shared.Collections.Pools.SimpleMonoBehaviourPool<ActionButton>(buttonPre, transform, 5);

            RTSPlayerCtrl.addOnEntitiesSelected(loadMenu);
            RTSPlayerCtrl.addOnEntitiesCleared(clearMenu);
        }

        void loadMenu(HashSet<RTSTeamEntity> casters) {
            _menuDatas = new Dictionary<MenuData, ActionGroup>(4);

            foreach(var caster in casters) {

                if(caster.getMenuData() == null) continue;

                if(!_menuDatas.ContainsKey(caster.getMenuData())) {
                    _menuDatas.Add(caster.getMenuData(), new ActionGroup());
                }

                _menuDatas[caster.getMenuData()].addCaster(caster);
            }

            clearMenu();
            foreach(var item in _menuDatas) {
                loadMenu(item.Key, item.Value);
                break;
            }
        }

        void clearMenu() {
            _buttonPool.despawnAll();
        }

        void loadMenu(MenuData data, ActionGroup group) {

            foreach(var button in data.buttons) {
                addButton(button, group);
            }
        }

        void addButton(MenuButtonData buttonData, ActionGroup group) {
            ActionButton created = _buttonPool.spawn();

            created.setData(buttonData, group.casters);
        }
    }

    public class ActionGroup {
        public HashSet<RTSTeamEntity> casters;

        public ActionGroup() {
            casters = new HashSet<RTSTeamEntity>();
        }

        public void addCaster(RTSTeamEntity caster) {
            casters.Add(caster);
        }
    }
}