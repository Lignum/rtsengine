﻿using RTS.AI;
using RTS.Buildings;
using RTS.Game;
using RTS.Game.Entities;
using RTS.Game.Teams;
using RTS.MapAI;
using RTS.MapAI.PathFinding;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RTS.Jobs {

    public enum JobState { Idle, Gather, Deposit, Take, Attack, OnMasterOrder }

    public abstract class Job {

        protected WorkerAI _worker;

        float _minDis;
        float _dis;
        protected RTSEntity _nextTarget;

        HashSet<RTSEntity> _targets;

        public Job(WorkerAI worker, RTSEntity target) : this (worker, new RTSEntity[1] { target }) {}
        public Job(WorkerAI worker, RTSEntity[] targets) : this(worker, new HashSet<RTSEntity>(targets)) {}
        public Job(WorkerAI worker, List<RTSEntity> targets) : this(worker, new HashSet<RTSEntity>(targets)) { }
        public Job(WorkerAI worker) : this(worker, new HashSet<RTSEntity>()) { }
        public Job(WorkerAI worker, HashSet<RTSEntity> targets) {
            _worker = worker;
            _targets = new HashSet<RTSEntity>(targets);
        }

        public void setWorker(WorkerAI worker) {
            _worker = worker;
        }

        public abstract void execute(float seconds);
        public abstract void checkMasterOrder(MasterOrder order);
        public RTSEntity getTarget() {
            return _nextTarget;
        }

        protected void calculateNextTarget(float range) {
            
            _minDis = int.MaxValue;
            _nextTarget = null;

            foreach(var item in _targets) {

                _dis = _worker.Agent.distanceTo(item.TilePosition);

                if(_dis <= _minDis) {

                    if(RTSTileMng.getClosestTileToRequester(item, _worker.Agent, range, (int)PathTileState.Empty) != null) {
                        _minDis = _dis;
                        _nextTarget = item;
                    }
                }
            }

            //return _nextTarget;
        }

        public void addTarget(RTSEntity target) {
            if(!_targets.Contains(target)) {
                _targets.Add(target);
            }
        }
        public void addTarget(HashSet<RTSEntity> targets) {
            foreach(var target in targets) {
                addTarget(target);
            }
        }

        public void removeTarget(RTSEntity target) {
            _targets.Remove(target);
        }
        public void removeTarget(HashSet<RTSEntity> targets) {
            foreach(var target in targets) {
                _targets.Remove(target);
            }
        }

        public I_ResourceBuilding getClossestResource(bool deposit, params int[] ResourceMaterialType) {
            I_ResourceBuilding selected = null;
            float minDistance = 1000;
            float dis;

            HashSet<I_ResourceBuilding> destinations = TeamsMng.getTeam(_worker.Team).getResourceBuildings(deposit, ResourceMaterialType);

            foreach(var des in destinations) {

                if(deposit && des.IsFull) continue;
                if(!deposit && des.IsEmpty) continue;

                dis = Vector3.Distance(des.Entity.WorldPosition, _worker.Agent.WorldPosition);
                if(dis < minDistance) {
                    minDistance = dis;
                    selected = des;
                }
            }

            return selected;
        }

        public void drawGizmos() {
            Gizmos.color = Color.black;

            foreach(var item in _targets) {
                CustomGizmos.drawArrowLine(_worker.Agent.WorldPosition, item.WorldPosition, .5f);
            }

        }

    }
}