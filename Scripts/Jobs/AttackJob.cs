﻿using System.Collections;
using System.Collections.Generic;
using RTS.AI;
using RTS.Game;
using RTS.Game.Entities;
using UnityEngine;

namespace RTS.Jobs {
    public class AttackJob : Job {

        public AttackJob(WorkerAI worker) : base(worker) { }
        public AttackJob(RTSTeamEntity target) : base(null, target) { }

        public override void execute(float seconds) {

            if(_worker.State != JobState.OnMasterOrder) {

                if(!_worker.HasTarget && _worker.CanTakeDecision) {
                    calculateNextTarget(_worker.AIData.AttackRange);

                    if(_nextTarget != null) {
                        _worker.setTarget(_nextTarget);
                    } else {
                        _worker.setIdle();
                    }
                } else if(_worker.HasTarget) {
                    _worker.attackTarget();
                } else {
                    _worker.setIdle();
                }
            }
        }

        public override void checkMasterOrder(MasterOrder order) {
        }

    }
}