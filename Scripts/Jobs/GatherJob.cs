﻿using RTS.AI;
using RTS.Buildings;
using RTS.Game;
using RTS.Game.Entities;
using RTS.MapAI;
using RTS.MapAI.PathFinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Jobs {

    public class GatherJob : Job {

        public int ResourceMaterialType { get; private set; }
        I_ResourceBuilding _fixedExtorage;

        public GatherJob(ResourceMaterialEntity target) : this(null, target) {}
        public GatherJob(WorkerAI worker, ResourceMaterialEntity target) : base(worker, target) {
            ResourceMaterialType = target.ResourceMaterialType;
        }


        public override void execute( float seconds) {

            if(_worker.CanCarryMore && _worker.State != JobState.Deposit) {
                calculateNextTarget(_worker.AIData.InteractRange);

                if(_nextTarget != null) {
                    _worker.attackResource(_nextTarget);

                    return;
                }
            }

            if(_worker.MaterialResourcesCount > 0) {
                I_ResourceBuilding storage;

                if(_fixedExtorage != null) {
                    storage = _fixedExtorage;
                } else {
                    storage = getClossestResource(true, ResourceMaterialType);
                }
                
                if(storage != null && !storage.IsFull) {
                    _worker.depositResources(storage, seconds);
                    return;
                }
            }

            _worker.setIdle();
        }

        public override void checkMasterOrder(MasterOrder order) {
            if(order.TargetObj is I_ResourceBuilding) {
                setFixedStorage(order.TargetObj as I_ResourceBuilding);
            } else if(order.TargetObj == null) {
                if(RTSTileMng.tryGetTile(order.ClickedPosition, out MapTile tile)) {
                    if(tile.State == PathTileState.Empty) {
                        setFixedStorage(GameDataMng.spawnMaterialResourcePile(tile));
                    } else if(tile.OcuppiedObj is I_ResourceBuilding) {
                        setFixedStorage(tile.OcuppiedObj as I_ResourceBuilding);
                    }
                }
            }
        }

        public void setFixedStorage(I_ResourceBuilding storage) {
            _fixedExtorage = storage;
        }
    }
}