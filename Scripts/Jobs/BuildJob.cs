﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RTS.AI;
using RTS.Buildings;
using RTS.Game;
using RTS.Game.Entities;
using UnityEngine;

namespace RTS.Jobs {

    public class BuildJob : Job {

        BuildingOrder _target;


        public BuildJob(BuildingOrder target) : this(null, target) { }
        public BuildJob(WorkerAI worker, BuildingOrder target) : base(worker, target) { }
        public BuildJob(HashSet<BuildingOrder> target) : this(null, target) { }
        public BuildJob(WorkerAI worker, HashSet<BuildingOrder> target) : base(worker, target.ToArray<RTSEntity>()) { }

        public override void execute(float seconds) {

            if(_target == null) {
                calculateNextTarget(_worker.AIData.InteractRange);
                _target = _nextTarget as BuildingOrder;
            }

            if(_target == null) {
                if(_worker.MaterialResourcesCount > 0) {
                    depositResources(seconds);
                } else {
                    _worker.clearJob();
                }
            } else if(_target.IsBuilded) {
                removeTarget(_target);
                _target = null;
            } else {

                if(!_target.IsFull) {

                    Dictionary<int, int> pendingResources = _target.getPendingResources(_worker);
                    int[] resources = pendingResources.Keys.ToArray();

                    if(_worker.isCarringResources(resources) && !(_worker.State == JobState.Take && _worker.CanCarryMore)) {
                        if(_target.hasResourceReserved(_worker)) {
                            _worker.depositResources(_target, seconds);
                        } else {
                            int reservedResources = _target.reserveResourceMaterial(_worker, _worker.CurrentMateriaResourceType, _worker.MaterialResourcesCount);
                            //Debug.Log($"Reserved: {reservedResources}");
                            if(reservedResources == 0) {
                                depositResources(seconds);
                            }
                        }
                    }else if(_target.hasResourceReserved(_worker)) {
                        _target.removeReserve(_worker);
                    }else if(_worker.isCarringOtherResources(resources)) {
                        depositResources(seconds);
                    } else {
                        I_ResourceBuilding resourceBuilding = getClossestResource(false, resources);

                        if(resourceBuilding != null) {
                            //int maxResources = target.reserveResourceMaterial(_worker, resourceBuilding);
                            _worker.takeResources(resourceBuilding, pendingResources[resourceBuilding.ResourceType], seconds);
                        } else {
                            _worker.setIdle();
                        }
                    }


                } else {
                    _worker.build(_target, seconds);
                }
            }
        }

        void depositResources(float seconds) {
            I_ResourceBuilding resourceBuilding = getClossestResource(true, _worker.CurrentMateriaResourceType);
            if(resourceBuilding != null) {
                _worker.depositResources(resourceBuilding, seconds);
            }
        }

        public override void checkMasterOrder(MasterOrder order) {
        }
    }
}