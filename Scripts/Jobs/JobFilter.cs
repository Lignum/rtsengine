﻿using RTS.Buildings;
using RTS.Game;
using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using RTS.MapAI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Jobs {

    public enum JobType { Gather, Build, Attack}

    [CreateAssetMenu(fileName = "JobFilter", menuName = "RTS/Jobs/JobFilter", order = 1)]
    public class JobFilter : ScriptableObject {

        public bool GatherJobs;
        public bool BuildJobs;
        public bool EnterBuildingJobs;
        public bool AttackJobs;

        public Job getJob(RTSAgent agent, TiledObj target) {

            if(target == null) return null;

            Job createdJob = null;

            if(GatherJobs && target is ResourceMaterialEntity) {
                createdJob = new GatherJob(target as ResourceMaterialEntity);

            } else if(BuildJobs && target is BuildingOrder) {
                createdJob = new BuildJob(target as BuildingOrder);

            }else if(target is RTSBuilding) {
                if(EnterBuildingJobs && (target as RTSBuilding).canEnterAgent(agent)) {
                    createdJob = new EnterBuildingJob(target as RTSBuilding);
                }
            
            }else if(AttackJobs && target is RTSTeamEntity) {
                RTSTeamEntity entityTarget = target as RTSTeamEntity;
                if(entityTarget.TeamKey >= 0 && entityTarget.TeamKey != agent.TeamKey) {
                    createdJob = new AttackJob(entityTarget);
                }
            }

            return createdJob;
        }

    }
}