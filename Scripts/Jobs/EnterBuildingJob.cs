﻿using System.Collections;
using System.Collections.Generic;
using RTS.AI;
using RTS.Buildings;
using UnityEngine;

namespace RTS.Jobs {

    public class EnterBuildingJob : Job {

        RTSBuilding _target;

        public EnterBuildingJob(RTSBuilding target) : base(null, target) { }

        public override void checkMasterOrder(MasterOrder order) {
        }

        public override void execute(float seconds) {

            if(_target == null) {
                calculateNextTarget(_worker.AIData.InteractRange);
                _target = _nextTarget as RTSBuilding;
            }

            if(_target != null && !_target.canEnterAgent(_worker.Agent)) {
                _target = null;
            }

            if(_target != null) {
                _worker.enterBuilding(_target, seconds);
            } else {
                _worker.clearJob();
            }


        }
    }
}