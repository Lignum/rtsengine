﻿using RTS.AI;
using RTS.Buildings;
using RTS.Game;
using RTS.MapAI;
using Shared.Collections.Pools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Player {

    [RequireComponent(typeof(RTSPlayerCtrl))]
    public class RTSPlayerBuilder : MonoBehaviour {

        public bool IsBuilding { get; private set; }

        [SerializeField]
        Material _ghostBuildingMaterial;
        
        [SerializeField]
        Material _ghostBuildingBlockedMaterial;

        RTSBuilding _currentBuildingPre;

        [SerializeField]
        RTSGhostBuilding ghostPre;
        SimpleMonoBehaviourPool<RTSGhostBuilding> _ghostPool;


        HashSet<BuildingOrder> placedOrders;

        int _buildLenght;
        Vector3Int _buildSize;
        Vector3Int _buildDir;
        bool _buildEnded;
        bool _savedPosition;
        Vector3Int _buildPosition;
        int _buildRotation = 0;

        Vector3Int startPosition = -Vector3Int.one;
        System.Action<HashSet<BuildingOrder>> _onBuildPlaced;

        private void Awake() {
            _ghostPool = new SimpleMonoBehaviourPool<RTSGhostBuilding>(ghostPre, transform);
        }


        public void setBuilding(RTSBuilding building, System.Action<HashSet<BuildingOrder>> onBuildPlaced) {
            IsBuilding = true;

            _currentBuildingPre = building;
            _onBuildPlaced = onBuildPlaced;
            

            startPosition = -Vector3Int.one;
            _buildEnded = false;

            placedOrders = new HashSet<BuildingOrder>();
        }


        public void updateBuild() {

            _ghostPool.despawnAll();

            if(MouseCtrl.getWorldMouseButtonDown(1)) {
                stopBuilding();
                return;
            }

            _buildLenght = 0;
            RTSGhostBuilding ghost;

            if(Input.GetKeyDown(KeyCode.R)) {
                _buildRotation++;
                _buildRotation %= 4;
            }

            if(Input.GetKeyDown(KeyCode.LeftShift)) {
                _savedPosition = false;
            }

            if(Input.GetKey(KeyCode.LeftShift)) {

                if(!_savedPosition) {
                    if(MouseCtrl.getWorldMouseButtonDown(0)) {
                        startPosition = MouseCtrl.MouseTilePos;
                        _savedPosition = true;
                        return;
                    }
                } else {
                    _buildDir = MouseCtrl.MouseTilePos - startPosition;

                    if(_buildDir.x != 0 || _buildDir.z != 0) {
                        _buildSize = TiledObj.calculateRotatedSize(_currentBuildingPre.BaseSize, _buildRotation);

                        if(Mathf.Abs(_buildDir.x) > Mathf.Abs(_buildDir.z)) {
                            _buildLenght = Mathf.Abs(_buildDir.x / _buildSize.x);

                            _buildDir.x = (_buildDir.x / Mathf.Abs(_buildDir.x)) * Mathf.Abs(_buildSize.x);
                            _buildDir.z = 0;
                        } else {
                            _buildLenght = Mathf.Abs(_buildDir.z / _buildSize.z);

                            _buildDir.x = 0;
                            _buildDir.z = (_buildDir.z / Mathf.Abs(_buildDir.z)) * Mathf.Abs(_buildSize.z);
                        }
                    }
                    _buildDir.y = 0;
                }
            } else {
                _savedPosition = false;
            }

            if(!_savedPosition) {
                startPosition = MouseCtrl.MouseTilePos;
                _buildDir = Vector3Int.one;
            }
            

            _buildPosition = startPosition;

            for(int i = 0; i <= _buildLenght; i++) {

                ghost = _ghostPool.spawn();

                ghost.setBuilding(_currentBuildingPre);
                ghost.setMaterial(_ghostBuildingMaterial);                
                ghost.setPosition(_buildPosition);                
                ghost.setRotation(_buildRotation);

                if(_currentBuildingPre.canBePlacedAt(ghost.TilePosition, ghost.TiledRotation)) {
                    ghost.setMaterial(_ghostBuildingMaterial);

                    if(MouseCtrl.getWorldMouseButtonDown(0)) {

                        placedOrders.Add(endBuilding(ghost));

                        _buildEnded = true;
                    }
                } else {
                    ghost.setMaterial(_ghostBuildingBlockedMaterial);
                }


                if(_buildDir.magnitude == 0) {
                    break;
                } else {
                    _buildPosition += _buildDir;
                }
            }

            if(_buildEnded) {
                _ghostPool.despawnAll();

                if(placedOrders.Count > 0) {
                    _onBuildPlaced(placedOrders);
                    //Jobs.BuildJob job = new Jobs.BuildJob(placedOrders);
                    //_worker.setJob(job);
                }

                placedOrders = null;
            }
        }

        void stopBuilding() {
            IsBuilding = false;
        }

        BuildingOrder endBuilding(RTSGhostBuilding ghost) {
            BuildingOrder order = GameDataMng.BuildingsOrderPool.spawn();

            order.place(ghost.TilePosition, ghost.Rotation, _currentBuildingPre);

            IsBuilding = false;

            return order;
        }

    }
}