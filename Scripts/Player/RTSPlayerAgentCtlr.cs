﻿using RTS.Game;
using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using RTS.MapAI;
using Shared.CustomInput;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Player {

    public class RTSPlayerAgentCtlr  {

        HashSet<RTSAgent> _seletedAgents = new HashSet<RTSAgent>();

        TiledObj _selectedTarget;

       //public  Dictionary<RTSAgent, Vector3Int> testPositions;

        public void update() {

            /*if(_seletedAgents.Count > 0) {
                AgentsFormation formation = new AgentsFormation(_seletedAgents);
                formation.getFormationIn(MouseCtrl.MouseTilePos, endCalc);
            }*/

            if(MouseCtrl.getWorldMouseButtonClick(1)) {

                checkSelectedAgents();

                if(MouseCtrl.HoveredEntity != null && !MouseCtrl.IsHoveredEntityWalkable) {
                    setAgentsTarget(MouseCtrl.HoveredEntity);
                } else {

                    //Debug.Log(MouseCtrl.MouseGroundPos);
                    setAgentsDestination(MouseCtrl.MouseTilePos);
                }
            }else if(MouseCtrl.getWorldMouseButtonClick(1)) {
                setAgentsDestination(MouseCtrl.MouseMiniMapTilePos);
            }

        }

        /*void endCalc(Dictionary<RTSAgent, Vector3Int> positions) {
            testPositions = positions;
        }*/

        public void setSelectedAgents(HashSet<AgentSelection> agentSelection) {
            _seletedAgents.Clear();

            foreach(var sel in agentSelection) {
                if(sel.Parent is RTSAgent) {
                    _seletedAgents.Add(sel.Parent as RTSAgent);
                }
            }
        }
        public void setSelectedAgents(HashSet<RTSAgent> agentSelection) {
            _seletedAgents = new HashSet<RTSAgent>(agentSelection);
        }
        public void setSelectedAgents(HashSet<RTSTeamEntity> agentSelection) {
            _seletedAgents.Clear();
            foreach(var item in agentSelection) {
                if(item is RTSAgent) {
                    _seletedAgents.Add(item as RTSAgent);
                }
            }
        }
        public void clearSelectedAgents() {
            _seletedAgents.Clear();
        }

        void checkSelectedAgents() {
            _seletedAgents.RemoveWhere(IsDisabled);
        }

        bool IsDisabled(RTSAgent agent) {
            return !agent.IsActive;
        }

        void setAgentsDestination(Vector3Int position) {

            AgentsFormation formation = new AgentsFormation(_seletedAgents);
            formation.setAgentsDestinations(position, true);
        }

        void setAgentsTarget(RTSEntity target) {
            foreach(var agent in _seletedAgents) {
                agent.setTarget(target);
            }
        }
    }
}