﻿using RTS.Game;
using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using RTS.Game.Teams;
using Shared.Collections.Pools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RTS.Player {

    [RequireComponent(typeof(RTSPlayerCtrl))]
    public class RTSPlayerAgentSelector : MonoBehaviour {

        [SerializeField]
        LayerMask _agentMask;

        [SerializeField]
        RectTransform selectorAreaPref;

        [SerializeField]
        Material agentSelHoverMat;
        [SerializeField]
        Material agentSelSelectedMat;

        RectTransform _selectorArea;

        Vector2 _selectionsStart;
        Vector2 _selectionsEnd;

        SimpleMonoBehaviourPool<AgentSelection> _agentsSelectionPool;

        HashSet<RTSAgent> _teamAgents;
        HashSet<AgentSelection> _selectedAgents = new HashSet<AgentSelection>();
        Dictionary<RTSAgent, AgentSelection> _hoveredAgents = new Dictionary<RTSAgent, AgentSelection>();
        public RTSTeamEntity SingleSelectedEntity { get; private set; }


        Rect _selectionRect;

        bool _isDrawing = false;

        public void init() {

             Canvas SelectorCanvas = GameObject.Instantiate(Resources.Load<Canvas>("UI/RTS_SelectorCanvas"));

            _selectorArea = GameObject.Instantiate(selectorAreaPref);
            _selectorArea.GetComponent<Image>().raycastTarget = false;
            _selectorArea.SetParent(SelectorCanvas.transform);
            _selectorArea.gameObject.SetActive(false);

            _teamAgents = TeamsMng.getTeam(RTSPlayerCtrl.PlayerTeam).agents;

            _agentsSelectionPool = new SimpleMonoBehaviourPool<AgentSelection>(Resources.Load<AgentSelection>("Agents/AgentSelection"));
            AgentSelection.setHoverMaterial(agentSelHoverMat);
            AgentSelection.setSelectedMaterial(agentSelSelectedMat);

            StartCoroutine(hoverAgents());
        }
                
        public void updateSelection() {
            createSelectionsArea();
            
        }

        void createSelectionsArea() {

            if(MouseCtrl.getWorldMouseButtonDown(0)) {
                _selectionsStart = MouseCtrl.MouseScreenPos;
                _isDrawing = true;
                _selectionRect = Rect.zero;

            } else if(MouseCtrl.getAnyMouseButton(0) && _isDrawing) {
                drawArea();
            } else if(MouseCtrl.getAnyMouseButtonUp(0) && _isDrawing) {
                clearSelectedAgents();

                selectAgents();                

                _selectionRect = Rect.zero;
            }
            
        }

        IEnumerator hoverAgents() {

            AgentSelection agentSel;

            Rect lastRect = Rect.zero;

            int agentCount = 0;

            while(true) {

                if(_selectionRect != Rect.zero) {
                    foreach(var agent in _teamAgents) {

                        if(!agent.IsActive) continue;

                        if( _selectionRect.Contains(agent.ScreenPos)) {
                            if(_hoveredAgents.Count < 5000 && !_hoveredAgents.ContainsKey(agent) ) {
                                agentSel = _agentsSelectionPool.spawn();
                                agentSel.showHovered(agent);
                                _hoveredAgents.Add(agent, agentSel);
                            }
                        } else {
                            if(_hoveredAgents.ContainsKey(agent)) {
                                _agentsSelectionPool.despawn(_hoveredAgents[agent]);
                                _hoveredAgents.Remove(agent);
                            }
                        }

                        agentCount++;

                        if(agentCount > 500) {
                            agentCount = 0;
                            yield return new WaitForEndOfFrame();
                        }
                    }
                }

                yield return new WaitForEndOfFrame();

            }
        }

        void drawArea() {
            _selectionsEnd = MouseCtrl.MouseScreenPos;

            _selectorArea.gameObject.SetActive(true);
            _selectorArea.position = Vector2.Min(_selectionsStart, _selectionsEnd);
            _selectorArea.sizeDelta = Vector2Extension.abs(_selectionsEnd - _selectionsStart);

            _selectionRect = new Rect(_selectorArea.rect);
            _selectionRect.position = _selectorArea.position;
        }

        void selectAgents() {

            _selectorArea.gameObject.SetActive(false);
            _isDrawing = false;

            if(_hoveredAgents.Count == 0) {
                selectSingleEntity();
            } else {
                SingleSelectedEntity = null;
                selectHoveredAgents();
                _hoveredAgents.Clear();
            }

            if(_selectedAgents.Count > 0) {

                HashSet<RTSTeamEntity> selectedEntities = new HashSet<RTSTeamEntity>();
                foreach(var item in _selectedAgents) {
                    selectedEntities.Add(item.Parent);
                }

                RTSPlayerCtrl.setSelectedEntities(selectedEntities);
            } else {
                RTSPlayerCtrl.clearSelectedEntities();
            }
        }

        void selectSingleEntity() {
            SingleSelectedEntity = MouseCtrl.HoveredEntity as RTSTeamEntity;

            if(SingleSelectedEntity != null && SingleSelectedEntity.IsActive && SingleSelectedEntity.TeamKey == RTSPlayerCtrl.PlayerTeam) {

                AgentSelection agentSel = _agentsSelectionPool.spawn();
                agentSel.showSelected(SingleSelectedEntity);
                _selectedAgents.Add(agentSel);
            }


        }

        void selectHoveredAgents() {
            foreach(var item in _hoveredAgents) {
                item.Value.showSelected();
                _selectedAgents.Add(item.Value);
            }
        }

        void clearHoveredAgents() {
            foreach(var item in _hoveredAgents) {
                _agentsSelectionPool.despawn(item.Value);
            }
            _hoveredAgents.Clear();
        }

        void clearSelectedAgents() {
            foreach(var item in _selectedAgents) {
                _agentsSelectionPool.despawn(item);
            }
            _selectedAgents.Clear();
        }
    }
}