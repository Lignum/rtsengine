﻿using RTS.Buildings;
using RTS.Game;
using RTS.Game.Entities;
using RTS.MapAI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Player {
    public class AgentSelection : MonoBehaviour {

        static Material HOVER_MATERIAL;
        static Material SELECTED_MATERIAL;

        static Vector3 BUILDING_SIZE_INCREMENT = new Vector3(.2f, 0, .2f);

        MeshRenderer _renderer;

        public RTSTeamEntity Parent { get; private set; }

        private void Awake() {
            _renderer = GetComponent<MeshRenderer>();
        }

        /*private void Update() {
            transform.position = Parent.transform.position;
        }*/

        public static void setHoverMaterial(Material mat) {
            HOVER_MATERIAL = mat;
        }

        public static void setSelectedMaterial(Material mat) {
            SELECTED_MATERIAL = mat;
        }

        void setParentEntity(RTSTeamEntity agent) {
            Parent = agent;

            transform.SetParent(agent.transform);

            Vector3 size = new Vector3(agent.Size.x * RTSGameCtrl.TileSize, 1, agent.Size.z * RTSGameCtrl.TileSize);

            if(agent is RTSBuilding) {
                size += BUILDING_SIZE_INCREMENT;

            }/* else {
                transform.position = agent.transform.position;// - Vector3.up * .07f;
            }*/
            transform.position = agent.transform.position + Vector3.up * .01f;

            transform.localScale = size;

        }

        public void showHovered(RTSTeamEntity agent) {
            setParentEntity(agent);
            _renderer.material = HOVER_MATERIAL;
        }

        public void showSelected() {
            _renderer.material = SELECTED_MATERIAL;
        }
        public void showSelected(RTSTeamEntity agent) {
            setParentEntity(agent);
            showSelected();
        }

    }
}