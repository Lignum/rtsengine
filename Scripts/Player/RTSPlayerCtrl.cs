﻿using RTS.Buildings;
using RTS.Game;
using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using RTS.Game.Teams;
using RTS.MapAI;
using RTS.MapAI.PathFinding;
using RTS.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RTS.Player {

    [RequireComponent(typeof(RTSPlayerAgentSelector))]
    [RequireComponent(typeof(RTSPlayerBuilder))]    
    public class RTSPlayerCtrl : MonoBehaviour {

        public static RTSPlayerCtrl INSTANCE;
        
        public static int PlayerTeam = 0;

        RTSPlayerAgentSelector _agentSelector;
        static RTSPlayerAgentCtlr _agentCtlr;
        RTSPlayerBuilder _builder;
        

        static Action<HashSet<RTSTeamEntity>> _onEntitiesSelected;
        static Action _onEntitiesCleared;

        private void Awake() {

            INSTANCE = this;

            _agentSelector = GetComponent<RTSPlayerAgentSelector>();
            _agentCtlr = new RTSPlayerAgentCtlr();
            _builder = GetComponent<RTSPlayerBuilder>();
            
        }
        
        // Start is called before the first frame update
        void Start() {
            _agentSelector.init();

        }

        // Update is called once per frame
        void Update() {

            if(!_builder.IsBuilding) {
                
                _agentSelector.updateSelection();
                _agentCtlr.update();
            } else {
                _builder.updateBuild();
            }

            if(MouseCtrl.HoveredEntity == _agentSelector.SingleSelectedEntity
                && _agentSelector.SingleSelectedEntity is RTSBuilding 
                &&  MouseCtrl.getWorldMouseButtonDobleClick(0)) {

                (_agentSelector.SingleSelectedEntity as RTSBuilding).onDoubleClick();
            }
        }

        public static void setSelectedAgents(RTSAgent entity) {
            setSelectedEntities(new HashSet<RTSTeamEntity>() { entity });
        }
        public static void setSelectedAgents(HashSet<RTSAgent> entities) {
            setSelectedEntities(new HashSet<RTSTeamEntity>(entities));
        }
        public static void setSelectedEntities(HashSet<RTSTeamEntity> entities) {
            _agentCtlr.setSelectedAgents(entities);

            _onEntitiesSelected?.Invoke(entities);
        }
        public static void clearSelectedEntities() {

            _onEntitiesCleared?.Invoke();

            _agentCtlr.clearSelectedAgents();
        }

        public RTSPlayerAgentCtlr GetAgentCtlr() {
            return _agentCtlr;
        }
        public RTSPlayerBuilder GetBuilder() {
            return _builder;
        }

        public static Team getTeam() {
            return TeamsMng.getTeam(PlayerTeam);
        }

        public static void addOnEntitiesSelected(Action<HashSet<RTSTeamEntity>> action) {
            _onEntitiesSelected += action;
        }
        public static void addOnEntitiesCleared(Action action) {
            _onEntitiesCleared += action;
        }

        private void OnDrawGizmos() {
            if(Application.isPlaying) {
                getTeam().drawGizmos();
            }
        }
        
    }
}