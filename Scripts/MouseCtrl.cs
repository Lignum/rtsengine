﻿using RTS.Game;
using RTS.Game.Entities;
using RTS.MapAI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace RTS {
    public class MouseCtrl : MonoBehaviour {

        [SerializeField]
        LayerMask groundLayer;

        [SerializeField]
        LayerMask entityLayer;

        static int _entityWalkableLayer;

        public static MouseCtrl INSTANCE;
        static Camera _camera;
        RaycastHit _hitInfo;

        public static Vector2 MouseScreenPos { get; private set; }

        public static Vector3 MouseGroundPos { get; private set; }
        public static Vector3 MouseFloorPos { get; private set; }

        public static Vector3Int MouseTilePos { get; private set; }
        public static Vector3 MouseWorldTiledPos { get; private set; }

        Plane _floorPlane;

        Ray _mouseRay;
        
        MiniMap _miniMap;

        public static bool IsMouseOverMiniMap { get; private set; }
        public static Vector2 MouseMiniMapPos { get; private set; }
        public static Vector3 MouseMiniMapFloorPos { get; private set; }
        public static Vector3 MouseMiniMapGroundPos { get; private set; }
        public static Vector3Int MouseMiniMapTilePos { get; private set; }

        public static RTSEntity HoveredEntity { get; private set; }
        public static bool IsHoveredEntityWalkable { get; private set; }

        //static float _lastClickTime;
        static float[] _lastClickTime = new float[3];

        static Vector2 _lastMousePosition;
        static bool[] _didMouseDrag = new bool[3];

        private void Awake() {

            if(INSTANCE != null) {
                Debug.LogError("Too many MouseCtrl instances", gameObject);
            }
            INSTANCE = this;

            _entityWalkableLayer = LayerMask.NameToLayer("Walkable");

            _camera = Camera.main;
            _floorPlane = new Plane(Vector3.up, Vector3.zero);

            _miniMap = FindObjectOfType<MiniMap>();
        }
        
        public static Vector2 worldToScreenPos(Vector3 pos) {
            return _camera.WorldToScreenPoint(pos);
        }
        public static Vector3 screenToWorldPos(Vector2 pos) {
            return _camera.ScreenToWorldPoint(pos);
        }


        void Update() {


            MouseScreenPos = Input.mousePosition;

            _mouseRay = _camera.ScreenPointToRay(MouseScreenPos);

            checkWorld();

            checkEntities();

            if(_miniMap) {
                checkMiniMap();
            }
        }

        private void LateUpdate() {

            checkLastClick();
        }

        void checkLastClick() {

            for(int i = 0; i < _lastClickTime.Length; i++) {
                if(Input.GetMouseButtonDown(i)) {
                    _lastClickTime[i] = .5f;
                } else if(_lastClickTime[i] > 0){
                    _lastClickTime[i] -= Time.deltaTime;
                }

                if(!_didMouseDrag[i] && Input.GetMouseButton(i)) {
                    if(Vector2.Distance(_lastMousePosition, MouseScreenPos) > 7) _didMouseDrag[i] = true;
                } else if( Input.GetMouseButtonUp(i)) {
                    _didMouseDrag[i] = false;
                }
            }

            _lastMousePosition = MouseScreenPos;
        }

        void checkWorld() {
            if(Physics.Raycast(_mouseRay, out _hitInfo, 1000, groundLayer)) {
                MouseGroundPos = _hitInfo.point;
            }

            if(_floorPlane.Raycast(_mouseRay, out float distance)) {
                MouseFloorPos = _mouseRay.GetPoint(distance);
            }

            MouseTilePos = RTSGameCtrl.worldToTilePosition(MouseGroundPos);

            MouseWorldTiledPos = RTSGameCtrl.tileToWorldPosition(MouseTilePos);
        }

        void checkEntities() {
            HoveredEntity = null;
            if(Physics.Raycast(_mouseRay, out _hitInfo, 1000, entityLayer)) {
                HoveredEntity = _hitInfo.collider.GetComponentInParent<RTSEntity>();
                IsHoveredEntityWalkable = _hitInfo.collider.gameObject.layer == _entityWalkableLayer;
            }

            if(HoveredEntity == null) {
                HoveredEntity = RTSTileMng.getTileObjAt<RTSEntity>(MouseTilePos);
                //Debug.Log($"Tile: {MouseTilePos} -> {HoveredEntity}");
            }
        }

        void checkMiniMap() {
            IsMouseOverMiniMap = _miniMap.isPointOver(MouseScreenPos);
            if(IsMouseOverMiniMap) {
                MouseMiniMapPos = _miniMap.getPosition(MouseScreenPos);
                MouseMiniMapFloorPos = _miniMap.miniMapToWorldPosition(MouseMiniMapPos);
                

                if(Physics.Raycast(new Ray(MouseMiniMapFloorPos + (Vector3.up * 100), Vector3.down), out _hitInfo)) {
                    MouseMiniMapGroundPos = _hitInfo.point;
                }

                MouseMiniMapTilePos = RTSGameCtrl.worldToTilePosition(MouseMiniMapGroundPos);
            }
        }

        public static bool getAnyMouseButton(int button) {
            return Input.GetMouseButton(button);
        }
        public static bool getAnyMouseButtonDown(int button) {
            return Input.GetMouseButtonDown(button);
        }
        public static bool getAnyMouseButtonUp(int button) {
            return Input.GetMouseButtonUp(button);
        }

        public static bool getWorldMouseButton(int button) {
            return Input.GetMouseButton(button) && !EventSystem.current.IsPointerOverGameObject();
        }
        public static bool getWorldMouseButtonDown(int button) {
            return Input.GetMouseButtonDown(button) && !EventSystem.current.IsPointerOverGameObject();
        }
        public static bool getWorldMouseButtonUp(int button) {
            return Input.GetMouseButtonUp(button) && !EventSystem.current.IsPointerOverGameObject();
        }
        public static bool getWorldMouseButtonClick(int button) {
            if(getWorldMouseButtonUp(button)) {
                if(!_didMouseDrag[button]) {
                    return true;
                }
            }

            return false;
        }
        public static bool getWorldMouseButtonDobleClick(int button) {
            if(getWorldMouseButtonDown(button)){
                if(_lastClickTime[button] > 0) {
                    return true;
                }
            }

            return false;
        }

        public static bool getMiniMapMouseButtonDown(int button) {
            return IsMouseOverMiniMap && Input.GetMouseButtonDown(button);
        }

        private void OnDrawGizmos() {
            Gizmos.color = Color.magenta;

            if(RTSGameCtrl.getInstance() != null && RTSTileMng.IsMapLoaded) {
                MapTile.drawTileGizmo(MouseTilePos);

                MapTile tile = RTSTileMng.getTile(MouseTilePos);
                if(tile != null) {
                    Gizmos.DrawCube(MouseWorldTiledPos, Vector3.one * .5f);
                    tile.drawConnectionsGizmos();
                }
               
            }

            Gizmos.DrawRay(MouseGroundPos, Vector3.up);
        }
    }
}