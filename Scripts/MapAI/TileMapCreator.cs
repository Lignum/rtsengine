﻿using RTS.Game;
using RTS.MapAI.PathFinding;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace RTS.MapAI {
    public static class TileMapCreator {

        readonly static RotatedSize[] _validSizes;

        static ConcurrentQueue<MapArea> _pendingRecalculationAreas = new ConcurrentQueue<MapArea>();
        static Thread _mapCalculationThread;

        static float _maxStepHeight;
        static int _maxMapAreaSize;
        static LayerMask _worldLayer;
        static Ray worldRay = new Ray(Vector3.one, Vector3.down);

        static TileStateMask _blocketState = new TileStateMask(PathTileState.Blocked | PathTileState.Disabled);

        static TileMapCreator() {

            Vector3Int[] sizes = new Vector3Int[9] {
                new Vector3Int(3, 1, 3), new Vector3Int(3, 1, 2), new Vector3Int(2, 1, 3), new Vector3Int(2, 1, 2),
                new Vector3Int(3, 1, 1), new Vector3Int(1, 1, 3), new Vector3Int(2, 1, 1), new Vector3Int(1, 1, 2), new Vector3Int(1, 1, 1) };

            HashSet<RotatedSize> allSizes = new HashSet<RotatedSize>();
            Vector3Int rotatedSize;

            foreach(var size in sizes) {
                for(int i = 0; i < 4; i++) {
                    rotatedSize = TiledObj.calculateRotatedSize(size, i);
                    allSizes.Add(new RotatedSize(rotatedSize, TiledObj.calculateOcuppationTiles(rotatedSize) ));
                }
            }

            _validSizes = allSizes.ToArray();

        }

        public static void setDimmensions(float maxStepHeight, int maxMapAreaSize, LayerMask worldLayer) {
            _maxStepHeight = maxStepHeight;
            _maxMapAreaSize = maxMapAreaSize;

            _worldLayer = worldLayer;
        }


        public static Map buildTiles(Vector3Int mapSize) {
            Debug.Log("Build Tiles");

            Dictionary<Vector3Int, MapTile> activeTiles = new Dictionary<Vector3Int, MapTile>();

            createTiles(mapSize, activeTiles);

            Map createdMap = new Map(activeTiles);

            createConnections(createdMap, _maxStepHeight, activeTiles);


            Thread mapCalculationThread = new Thread(new ThreadStart(() => calculateAreas(createdMap, _maxMapAreaSize, activeTiles)));
            mapCalculationThread.Start();

            return createdMap;
        }

        public static void stopBuildTiles() {
            MapAreaContructor.stopBuildTiles();
        }

        static void calculateAreas(Map currentMap, int maxMapAreaSize, Dictionary<Vector3Int, MapTile> activeTiles) {
            
            calculateTilesSizes(activeTiles, currentMap);

            System.DateTime lastUpdate = System.DateTime.Now;
            HashSet<MapArea> areas = MapAreaContructor.createAreas(maxMapAreaSize, activeTiles, 1);
            //Debug.Log($"Create Areas: {System.DateTime.Now - lastUpdate}");

            foreach(var item in areas) {
                item.createSubdivisions();
            }

            currentMap.addAreas(areas);
        }

        static void createTiles(Vector3Int mapSize, Dictionary<Vector3Int, MapTile> activeTiles) {
            RaycastHit hit;

            Vector3Int pos;

            for(int i = 0; i < mapSize.x; i++) {
                for(int e = 0; e < mapSize.z; e++) {

                    worldRay.origin = RTSGameCtrl.tileToWorldPosition(new Vector3Int(i, mapSize.y, e));
                    if(Physics.Raycast(worldRay, out hit, 100, _worldLayer)) {
                        pos = RTSGameCtrl.worldToTilePosition(hit.point);
                        activeTiles.Add(pos, new MapTile(hit.point, hit.normal));
                    }
                }
            }
        }


        static void createConnections(Map currentMap, float maxStepHeight, Dictionary<Vector3Int, MapTile> createdTiles) {
            Vector3Int pos;

            Vector3Int dirRight = new Vector3Int(1, 0, 0);
            Vector3Int dirForward = new Vector3Int(0, 0, 1);
            Vector3Int dirBackwards = new Vector3Int(0, 0, -1);
            Vector3Int dirLeft = new Vector3Int(-1, 0, 0);

            HashSet<Vector3Int> checkConnections = new HashSet<Vector3Int> {dirRight, dirForward, dirLeft, dirBackwards};

            pos = Vector3Int.zero;
            MapTile tile;
            bool hasSideR, hasSideU, hasSideL, hasSideD;

            foreach(var item in createdTiles) {

                if(((int)item.Value.State & _blocketState) > 0 ) continue;

                hasSideU = false;
                hasSideR = false;
                hasSideL = false;
                hasSideD = false;

                foreach(var conP in checkConnections) {
                    for(int y = -1; y <= 1; y++) {
                        pos = item.Key + conP;
                        pos.y += y;
                        if(pos.y < 0) continue;

                        if(currentMap.tryGetTile(pos, ~_blocketState, out tile)) {

                            if(item.Value.canConnectWith(tile, maxStepHeight)) {
                                item.Value.addConnection(tile);

                                hasSideR = hasSideR || conP == dirRight;
                                hasSideU = hasSideU || conP == dirForward;
                                hasSideL = hasSideL || conP == dirLeft;
                                hasSideD = hasSideD || conP == dirBackwards;
                            } 
                        } 
                    }
                }


                if((hasSideU || hasSideD) && (hasSideR || hasSideL)) {
                    for(int y = -1; y <= 1; y++) {
                        pos = item.Key;
                        pos.y += y;
                        if(pos.y < 0) continue;

                        if(hasSideU && hasSideR) {
                            if(currentMap.tryGetTile(pos + new Vector3Int(1, 0, 1), ~_blocketState, out tile)) {
                                if(item.Value.canConnectWith(tile, _maxStepHeight)) {
                                    item.Value.addConnection(tile);
                                }
                            }
                        }

                        if(hasSideU && hasSideL) {
                            if(currentMap.tryGetTile(pos + new Vector3Int(-1, 0, 1), ~_blocketState, out tile)) {
                                if(item.Value.canConnectWith(tile, maxStepHeight)) {
                                    item.Value.addConnection(tile);
                                }
                            }
                        }

                        if(hasSideD && hasSideR) {
                            if(currentMap.tryGetTile(pos + new Vector3Int(1, 0, -1), ~_blocketState, out tile)) {
                                if(item.Value.canConnectWith(tile, maxStepHeight)) {
                                    item.Value.addConnection(tile);
                                }
                            }
                        }

                        if(hasSideD && hasSideL) {
                            if(currentMap.tryGetTile(pos + new Vector3Int(-1, 0, -1), ~_blocketState, out tile)) {
                                if(item.Value.canConnectWith(tile, maxStepHeight)) {
                                    item.Value.addConnection(tile);
                                }
                            }
                        }
                    }
                }
            }
        }

        static void calculateTilesSizes(Dictionary<Vector3Int, MapTile> activeTile, Map map) {

            HashSet<MapTile> expanded;// = tile.getExpansion(1);

            bool valid = false;
            MapTile compareTile;

            HashSet<Vector3Int> sizes;

            //int t = 1000;
            System.DateTime lastUpdate = System.DateTime.Now;

            Vector3Int tilePos;

            foreach(var tile in activeTile) {
                expanded = tile.Value.getExpansion(1);

                tilePos = tile.Value.TilePosition;

                sizes = new HashSet<Vector3Int>();

                foreach(var size in _validSizes) {

                    foreach(var o in size.sizes) {

                        valid = false;

                        for(int y = -1; y <= 1; y++) {
                            if(map.tryGetTile(o + tilePos + new Vector3Int(0,y,0), out compareTile) && expanded.Contains(compareTile)) {
                                valid = true;
                                break;
                            }
                        }

                        if(!valid) {
                            break;
                        }
                    }

                    if(valid) {
                        sizes.Add(size.baseSize);
                    }

                }

                tile.Value.setValidSizes(sizes.ToArray());

                //t--;
                //if(t < 0) break;
            }
            Debug.Log($"calculateTilesSizes: {System.DateTime.Now - lastUpdate}");
        }

        public static void addAreaForRecalculation(MapArea area) {

            if(!RTSTileMng.IsMapLoaded) return;

            _pendingRecalculationAreas.Enqueue(area);

            if(_mapCalculationThread == null) {
                _mapCalculationThread = new Thread(new ThreadStart(recalculationThread));
                _mapCalculationThread.Start();
            }
        }

        static void recalculationThread() {
            Thread.Sleep(1000);

            HashSet<MapArea> areas = new HashSet<MapArea>();
            Dictionary<Vector3Int, MapTile> tiles = new Dictionary<Vector3Int, MapTile>(100);
            HashSet<Vector3Int> blocketTiles = new HashSet<Vector3Int>();
            Vector3Int pos;

            while(_pendingRecalculationAreas.Count > 0) {
                areas.Add(_pendingRecalculationAreas.Dequeue());
            }

            foreach(var area in areas) {
                area.clearConnections();
                foreach(var child in area.Childs) {
                    pos = RTSGameCtrl.worldToTilePosition(child.WorldPosition);

                    child.clearParents();
                    child.clearConnections();

                    if(!tiles.ContainsKey(pos) && ((int)child.State & _blocketState) == 0) {
                        tiles.Add(RTSGameCtrl.worldToTilePosition(child.WorldPosition), child as MapTile);
                    }
                }
            }

            /*oreach(var item in tiles) {
                item.Value.clearParents();
                item.Value.clearConnections();

                if(((int)item.Value.State & _blocketState) > 0) {
                    Debug.Log(item.Value.State);
                    blocketTiles.Add(item.Key);
                }
            }
            foreach(var item in blocketTiles) {
                tiles.Remove(item);
            }*/

            RTSTileMng.LoadedMap.removeAreas(areas);
            
            createConnections(RTSTileMng.LoadedMap, _maxStepHeight, tiles);
            calculateAreas(RTSTileMng.LoadedMap, _maxMapAreaSize, tiles);

            if(_pendingRecalculationAreas.Count > 0) {
                _mapCalculationThread = new Thread(new ThreadStart(recalculationThread));
                _mapCalculationThread.Start();
            } else {
                _mapCalculationThread = null;
            }
        }
    }

    struct RotatedSize {
        public Vector3Int baseSize;
        public HashSet<Vector3Int> sizes;

        public RotatedSize(Vector3Int size, HashSet<Vector3Int> sizes) {
            baseSize = size;
            this.sizes = sizes;
        }
    }
}