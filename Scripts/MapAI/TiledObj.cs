﻿using RTS.Game;
using RTS.Game.Entities;
using RTS.MapAI.PathFinding;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.MapAI {
    public abstract class TiledObj : MonoBehaviour {

        public Vector3 Position {
            get { return transform.position; }// + (RTSGameCtrl.tileToWorldPosition(calculateMidSize(Size)) - RTSGameCtrl.FlatTileDes); }
            set {

                    //Debug.Log($"{Size} - {RTSGameCtrl.tileToWorldPosition(calculateMidSize(Size))} - {RTSGameCtrl.FlatTileDes}");
                    //transform.position = value - (RTSGameCtrl.tileToWorldPosition(calculateMidSize(Size)) - RTSGameCtrl.FlatTileDes);

                transform.position = value;
                WorldPosition = value;
            }
        }

        public Vector3Int TilePosition {
            set { Position = RTSGameCtrl.tileToWorldPosition(value) - CenterPos; }
            get { return RTSGameCtrl.worldToTilePosition(WorldPosition + CenterPos); }
        }
        public virtual Vector3Int nextTilePosition {
            get { return TilePosition; }
        }

        public Vector3 WorldPosition { get; private set; }

        HashSet<MapTile> CurrentTiles = new HashSet<MapTile>();
        HashSet<MapTile> _reservedTiles = new HashSet<MapTile>();

        [SerializeField]
        EntityBaseData _entityData;
        public virtual EntityBaseData EntityData {
            get { return _entityData; }
            protected set {
                _entityData = value;
                recalculateSizes();
            }
        }

        public LogicArea CurrentArea { get; private set; }
        protected List<LogicArea> SubscribedAreas = new List<LogicArea>();

        public Vector3Int Size { get; private set; } = Vector3Int.one;
        public Vector3Int BaseSize {
            get {
                return EntityData.Size;
            }
        }

        public Vector3 CenterPos { get; private set;  }

        protected Transform Body { get; private set; }

        float _rotation = 0;
        public float Rotation {
            get {
                return _rotation;
            }
            protected set {
                _rotation = value % 360;

                Body.rotation = Quaternion.Euler(0, _rotation, 0);

                recalculateSizes();
            }
        }
        public int TiledRotation {
            set {
                Rotation = value * 90;
            }
            get { return Mathf.RoundToInt(_rotation / 90); }
        }

        protected virtual void Awake() {
            Body = transform.GetChild(0);

            TiledRotation = quaternionToTiledRotation(Body.rotation);
        }

        protected virtual void Start() {
            WorldPosition = transform.position;

            initPosition();
            //CurrentTiles = RTSTileMng.getTile(TilePosition);
        }

        void initPosition() {
            TilePosition = TilePosition;
            //occupieTiles(TilePosition, TiledRotation);
        }

        protected void recalculateSizes() {
            Size = calculateRotatedSize(BaseSize, TiledRotation);

            CenterPos = calculateRotatedCenter(TiledRotation);
        }

        protected void emptyCurrentTiles() {
            foreach(var tile in CurrentTiles) {
                tile.emptyTile(this);
            }
            CurrentTiles.Clear();
        }

        protected void reserveTiles(MapTile tile, int rotation) {
            reserveTiles(tile.TilePosition, rotation);
        }

        protected void reserveTiles(Vector3Int tilePos, int rotation) {
            foreach(var rTile in _reservedTiles) {
                rTile.emptyTile(this);
            }

            _reservedTiles.Clear();
            
            HashSet<Vector3Int> rSize = calculateOcuppationTiles(BaseSize, rotation);
            MapTile tile;

            foreach(var pos in rSize) {
                tile = RTSTileMng.getTile(tilePos + pos);
                tile.reserveTile(this);
                _reservedTiles.Add(tile);
            }
        }

        protected void occupieReservedTiles(PathTileState state) {
            emptyCurrentTiles();
            foreach(var item in _reservedTiles) {
                CurrentTiles.Add(item);
                item.occupyTile(this, state);
            }

            _reservedTiles.Clear();
        }

        protected void occupieTiles(Vector3Int tilePos, int rotation) {
            reserveTiles(tilePos, rotation);
            occupieReservedTiles(PathTileState.Ocuppied);
        }

        protected void blockTiles(Vector3Int tilePos, int rotation) {
            reserveTiles(tilePos, rotation);
            occupieReservedTiles(PathTileState.Blocked);
        }
        
        public static float getAngleDirection(Vector3 direction) {
            direction.y = 0;
            float angle = Vector3.Angle(Vector3.forward, direction);
            if(direction.x < 0) { angle = 360 - angle; }

            return angle;
        }
        public static int getTiledAngleDirection(Vector3 direction) {
            return Mathf.RoundToInt(getAngleDirection(direction) / 90);
        }
        public static Vector3 normalizeDirection(Vector3 direction) {
            int rotation = getTiledAngleDirection(direction);
            return (Quaternion.Euler(0, rotation * 90, 0) * Vector3.forward);
        }

        public static Vector3Int calculateRotatedSize(Vector3Int size, int rotation) {
            return (Quaternion.Euler(0, rotation * 90, 0) * size).toVector3IntRound();
        }
        public static Vector3 rotationToVector(int rotation) {
            return Quaternion.Euler(0, rotation * 90, 0) * Vector3.forward;
        }

        public static int quaternionToTiledRotation(Quaternion quaternion) {
            return Mathf.FloorToInt(getAngleDirection(quaternion * Vector3.forward) / 90);
        }

        protected Vector3 calculateRotatedCenter(int rotation) {
            Vector3 center = Vector3Int.zero;

            if(BaseSize.x % 2 == 0) center.x -= .5f;
            if(BaseSize.z % 2 == 0) center.z -= .5f;

            return Quaternion.Euler(0, rotation * 90, 0) * center;
        }

        public void setArea(LogicArea area) {
            CurrentArea = area;
        }

        public static Vector3Int calculateMidSize(Vector3Int size) {

            if(size.x > 0 && size.x % 2 == 0) size.x -= 1;
            if(size.z > 0 && size.z % 2 == 0) size.z -= 1;
           
            Vector3Int midSize = size / 2;

            midSize.x = Mathf.Abs(midSize.x);
            midSize.y = 0;
            midSize.z = Mathf.Abs(midSize.z);

            return midSize;
        }

        public static HashSet<Vector3Int> calculateOcuppationTiles(Vector3Int size) {
            HashSet<Vector3Int> tiles = new HashSet<Vector3Int>();
            Vector3Int midSize = calculateMidSize(size);

            Vector3Int aSize = new Vector3Int(Mathf.Abs(size.x), 0, Mathf.Abs(size.z));

            Vector3Int start = - midSize;
            Vector3Int last = start + aSize;

            for(int x = start.x; x < last.x; x++) {
                for(int z = start.z; z < last.z; z++) {
                    tiles.Add(new Vector3Int(x, 0, z));
                }
            }

            return tiles;
        }

        public static HashSet<Vector3Int> calculateOcuppationTiles(Vector3Int size, int rotation) {
            return calculateOcuppationTiles(calculateRotatedSize(size, rotation));
           
        }

        public float distanceTo(Vector3Int tile) {
            float minDist = Vector3Int.Distance(TilePosition, tile);
            float dis;

            HashSet<Vector3Int> tiles = calculateOcuppationTiles(BaseSize, TiledRotation);

            foreach(var t in tiles) {
                dis = Vector3Int.Distance(t + TilePosition, tile);
                if(dis < minDist) {
                    minDist = dis;
                }
            }

            return minDist;
        }

        public float nextPositionDistanceTo(Vector3Int tile) {
            float minDist = Vector3Int.Distance(TilePosition, tile);
            float dis;

            HashSet<Vector3Int> tiles = calculateOcuppationTiles(BaseSize, TiledRotation);

            foreach(var t in tiles) {
                dis = Vector3Int.Distance(t + nextTilePosition, tile);
                if(dis < minDist) {
                    minDist = dis;
                }
            }

            return minDist;
        }
    }
}