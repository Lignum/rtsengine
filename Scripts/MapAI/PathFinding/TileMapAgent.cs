﻿using RTS.AI;
using RTS.Game;
using RTS.Game.Entities;
using RTS.Jobs;
using System;
using System.Collections;
using UnityEngine;

namespace RTS.MapAI.PathFinding {

    public enum PathState { Pending, Calculated}

    public class TileMapAgent : RTSTeamEntity {

        public override Vector3Int nextTilePosition {
            get {
                if(HasPath) {
                    return RTSGameCtrl.worldToTilePosition(_activePath.EndPoint.WorldPosition);
                }
                return base.nextTilePosition;
            }
        }

        /*[Min(0.1f)]
        public float Speed = 1;

        [Min(1f)]
        public float RotationSpeed = 2;

        public bool CanMoveWhileRotating = true;*/

        public Vector3 Direction { get; private set; }
        Vector3 _lastDirection = Vector3.forward;

        public bool HasPath {
            get { return _activePath != null; }
        }
        public MapTile PathDestination {
            get { return _activePath.EndPoint as MapTile; }
        }

        Path _activePath;
        Coroutine _walkCoroutine;

        //EVENTS
        protected Action _onWalkPathStart, _onWalkPathEnd;
        protected Action<MasterOrder> _onRecibeMasterOrder;
        protected Action _onPathReceived;

        Coroutine _rotationCoroutine;

        public void warp(Vector3 position) {
            Position = position;
        }

        public void setPath(Path path) {
            _activePath = path;

            if(TeamKey == 1) Debug.DrawRay(_activePath.EndPoint.WorldPosition, Vector3.up * 2, Color.green, 5);

            onPathCalculated();
        }

        public void setDestination(Vector3Int clickedPosition, MapTile destinationTile, Vector3 endDirection, bool isMasterOrden) {

            if(isMasterOrden) {
                MasterOrder order = new MasterOrder(clickedPosition, destinationTile, null);
                _onRecibeMasterOrder?.Invoke(order);

                if(order.CtrlModifier) return;
            }

            reserveTiles(destinationTile, getTiledAngleDirection(endDirection));
            PathCalculator.requestPath(this, destinationTile, endDirection);
        }
        
        void onPathCalculated() {

            _onPathReceived?.Invoke();

            if(_walkCoroutine != null) {
                StopCoroutine(_walkCoroutine);
            }

            /*if(ReservedTile != null) {
                ReservedTile.emptyTile(this);
                ReservedTile = null;
            }*/

            if(_activePath == null) {
                Debug.Log("NUUL" + TilePosition, gameObject);
            }
            _walkCoroutine = StartCoroutine(walkPath(_activePath));

        }

        IEnumerator walkPath(Path path) {

            float e = 0;
            Vector3 nextPos = Position;
            float distance = 1;

            if(path.Length > 1) {

                emptyCurrentTiles();

                reserveTiles(RTSGameCtrl.worldToTilePosition(path.EndPoint.WorldPosition), TiledObj.getTiledAngleDirection(path.EndDirection));

                _onWalkPathStart?.Invoke();

                Vector3 preDes = Vector3.zero;
                Vector3 prev = Position;
                A_PathTile next;

                int i = 0;
                e = 1;

                if(!EntityData.CanMoveWhileRotating && TiledRotation != TiledObj.getTiledAngleDirection(path.StartDirection)) {
                    yield return moveToStartPosition(path[1].WorldPosition, path.StartDirection);
                    
                    preDes = Position - path[1].WorldPosition;
                    i++;
                }

                while(i <= path.Length + 1 ) {

                    if(e >= 1) {
                        e = 0;
                        i++;
                        
                        if(i == path.Length) break;

                        prev = nextPos;
                        next = path[i];

                        Direction = (next.WorldPosition - (prev - preDes)).normalized;                        

                        if(i < path.Length - 1 ) {
                            preDes = calculatePathDes(Direction, next.WorldPosition, path[i + 1].WorldPosition);
                        }
                        
                        if(EntityData.CanMoveWhileRotating) {
                            starLookPathCoroutine(Direction);
                        } else {
                            yield return lookPath(Direction);
                        }

                        if(i == path.Length - 1) {
                            nextPos = next.WorldPosition + calculateLastPositionDes(path.EndDirection);
                        } else {
                            nextPos = next.WorldPosition + preDes;
                        }

                        distance = Vector3.Distance(prev, nextPos);
                    }
                    
                    e += Time.deltaTime * EntityData.Speed / distance;

                    Position = Vector3.Lerp(prev, nextPos, e);

                    yield return new WaitForEndOfFrame();

                }
                
            } else {
            
                nextPos = path.EndPoint.WorldPosition + calculateLastPositionDes(path.EndDirection);
                distance = Vector3.Distance(Position, nextPos);
                while(e <= 1) {
                    e += Time.deltaTime * EntityData.Speed / distance;
                    Position = Vector3.Lerp(Position, nextPos, e);
                }
            }

            if(Vector3.Distance(path.EndPoint.WorldPosition, WorldPosition) < 1) {
                starLookPathCoroutine(path.EndDirection);

                occupieReservedTiles(PathTileState.Ocuppied);
                _activePath = null;

                _onWalkPathEnd?.Invoke();
            }
        }

        IEnumerator moveToStartPosition(Vector3 firstPos, Vector3 direction) {
            
            Vector3 des = calculatePathDes(direction, Position + CenterPos , firstPos);

            Vector3 startPos = Position;
            Vector3 endPos = firstPos + des;

            yield return lookPath((endPos - Position).normalized);

            float e = 0;
            float distance;

            distance = Vector3.Distance(startPos, endPos);
            
            while(e <= 1) {
                Position = Vector3.Lerp(startPos, endPos, e);

                e += Time.deltaTime * EntityData.Speed / distance;

                yield return new WaitForEndOfFrame();
            }
        }

        Vector3 calculatePathDes(Vector3 currentDir, Vector3 current, Vector3 next) {
            
            float s = 0;
            if(BaseSize.x % 2 == 0) s = .5f * RTSGameCtrl.TileSize;

            if(s == 0) return Vector3.zero;

            Vector3 nextDir = (next - current).normalized;
            nextDir.y = 0;

            float a = Vector2.SignedAngle(currentDir.toVector2Plane(), -nextDir.toVector2Plane());
            if(a < 0) a = 360 + a;
            a /= 2;

            Vector3 desNormal = (Quaternion.Euler(0, a, 0) * nextDir).normalized;
            Vector3 des;

            if(a < 90) {
                des = desNormal * s / Mathf.Cos(Mathf.Deg2Rad * (90 - a));
            } else if(a > 90) {
                des = desNormal * s / Mathf.Cos(Mathf.Deg2Rad * (a - 90));
            } else {
                des = desNormal * s;
            }

            return des;
        }

        Vector3 calculateLastPositionDes(Vector3 currentDir) {

            if(BaseSize.z % 2 == 0) {
                //  return currentDir * .5f;
                return Quaternion.LookRotation(currentDir, Vector3.up) * new Vector3(.5f, 0, .5f);
            }

            return Vector3.zero;
        }

        void starLookPathCoroutine(Vector3 nextDirecion) {
            if(_rotationCoroutine != null) {
                StopCoroutine(_rotationCoroutine);
            }
            _rotationCoroutine = StartCoroutine(lookPath(nextDirecion));
        }

        IEnumerator lookPath(Vector3 nextDirecion) {

            float nextAngle = getAngleDirection(nextDirecion);

            if(nextAngle != Rotation) {

                float e = 0;
                float angle = Rotation;

                if(angle > nextAngle && angle - nextAngle > 180) nextAngle += 360;
                else if(angle < nextAngle && nextAngle - angle > 180) nextAngle -= 360;

                while(e <= 1) {

                    e += Time.deltaTime * EntityData.RotationSpeed / (Mathf.Abs((nextAngle - angle)) % 360);

                    angle = Mathf.Lerp(angle, nextAngle, e);

                    Rotation = angle;

                    yield return new WaitForEndOfFrame();
                }
            }
        }


        public void lookTo(Vector3 direction) {
            direction.y = 0;

            if(direction.magnitude != 0) {
                float angle = Vector3.Angle(Vector3.forward, direction);
                if(direction.x < 0) { angle = 360 - angle; }

                Rotation = angle;
                //_body.forward = direction;
            }
            //_body.LookAt(transform.position + direction);
        }

        public void addOnWalkPathEnd(Action action) {
            _onWalkPathEnd += action;
        }

        public void addOnRecibeMasterOrder(Action<MasterOrder> action) {
            _onRecibeMasterOrder += action;
        }

        protected override void OnDrawGizmosSelected() {

            base.OnDrawGizmosSelected();

            if(_activePath != null) {
                Gizmos.color = Color.red;
                _activePath.drawGizmos();
            }
        }
    }
}