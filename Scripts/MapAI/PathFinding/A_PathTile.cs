﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.MapAI.PathFinding {


    public abstract class A_PathTile {

        public A_PathTile[] Parents { get; private set; } = new A_PathTile[3];
        public HashSet<A_PathTile> Connections { get; protected set; }
        public HashSet<A_PathTile> Childs { get; protected set; }

        public Vector3 WorldPosition { get; protected set; }
        public PathTileState State { get; protected set; } = PathTileState.Empty;

        public virtual int Level { get; protected set; } = 0;

        public void setParent(int level, A_PathTile p) {
            Parents[level] = p;
        }
        public void clearParents() {
            Parents[0] = null;
            Parents[1] = null;
            Parents[2] = null;
        }

        public HashSet<A_PathTile> getConnectedAreas(int level) {
            HashSet<A_PathTile> connectedAreas = new HashSet<A_PathTile>();

            foreach(var item in Connections) {
                if(item.Parents != Parents) connectedAreas.Add(item.Parents[level]);
            }

            return connectedAreas;
        }


        public virtual void removeConnection(A_PathTile tile) {
            Connections.Remove(tile);
        }

        public virtual void clearConnections() {
            foreach(var item in Connections) {
                item.removeConnection(this);
                item.onConnectionRemoved();
            }

            Connections.Clear();
            onConnectionRemoved();
        }

        public virtual void onConnectionRemoved() {

        }

        public bool contains(A_PathTile child) {
            if(Childs == null) return false;

            if(child.Level < Level) {

                if(Childs.Contains(child)) {
                    return true;
                } else {
                    foreach(var item in Childs) {
                        if(item.contains(child)) return true;
                    }
                }
            }

            return false;

        }

        public static A_PathTile getLowestLevelTile(A_PathTile p1, A_PathTile p2) {
            if(p1.Level < p2.Level) return p1;
            if(p1.Level > p2.Level) return p2;

            return null;
        }
        public static A_PathTile getBiggestLevelTile(A_PathTile p1, A_PathTile p2) {
            if(p1.Level > p2.Level) return p1;
            if(p1.Level < p2.Level) return p2;

            return null;
        }

        public float distanceTo(A_PathTile target) {
            if(Level == target.Level) {
                return Vector3.Distance(WorldPosition, target.WorldPosition);
            } else {
                float minDist = Vector3.Distance(WorldPosition, target.WorldPosition);
                float dis;

                A_PathTile biggest = getBiggestLevelTile(this, target);
                foreach(var item in biggest.Childs) {
                    dis = item.distanceTo(this);
                    if(dis < minDist) {
                        minDist = dis;
                    }
                }
                
                return minDist;
            }
        }

        public abstract bool canEnterFrom(A_PathTile from, Vector3Int size);
        public abstract bool canHaveSize(Vector3Int size);

        public virtual int getDirectionRotationTo(A_PathTile tile) {
            return TiledObj.getTiledAngleDirection(tile.WorldPosition - WorldPosition);
        }

        public override string ToString() {
            return $"A_PathTile: {WorldPosition}";
        }
    }
}