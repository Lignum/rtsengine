﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.MapAI.PathFinding {

    public enum PathTileState { Empty = 1, Reserved = 2, Ocuppied = 4, Blocked = 8, Disabled = 16 }

    public struct TileStateMask  {

        public static TileStateMask Any = 1 | 2 | 4 | 8 | 16;

        public int Value { get; set; }

        public TileStateMask(PathTileState state) {
            Value = (int)state;
        }

        public static TileStateMask GetMask(params PathTileState[] states) {
            TileStateMask state = new TileStateMask();

            foreach(var item in states) {
                state |= StateToMask(item);
            }

            return state;
        }

        public static int StateToMask(PathTileState name) {
            return (int)name;
        }

        public static implicit operator int(TileStateMask mask) {
            return mask.Value;
        }
        public static implicit operator TileStateMask(int intVal) {
            TileStateMask state = new TileStateMask();
            state.Value = intVal;

            return state;
        }
        public static implicit operator TileStateMask(PathTileState intVal) {
            TileStateMask state = new TileStateMask();
            state.Value = (int)intVal;

            return state;
        }

    }
}