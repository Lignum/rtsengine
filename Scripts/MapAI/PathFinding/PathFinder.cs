﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.MapAI.PathFinding {

    public static class PathFinder {

        public static A_PathTile[] findPath(A_PathTile start, A_PathTile end, Vector3Int size) {
            List<A_PathTile> path = calculatePathIN(start, end, end, size, out A_PathTile newEnd);

            return path.ToArray();
        }

        static List<A_PathTile> calculatePathIN(A_PathTile start, A_PathTile end, A_PathTile target, Vector3Int size, out A_PathTile newEnd) {
            HashSet<A_PathTile> validChids = null;
            List<A_PathTile> path;

            //Debug.Log($"s: {start} -> e: {end}");

            if(start.Parents[size.x - 1] != null && end.Parents[size.x - 1] != null && start.Parents != end.Parents) {
                List<A_PathTile> parentPath = calculatePathIN(start.Parents[size.x-1], end.Parents[size.x - 1], end, size, out newEnd);
                validChids = getValidChilds(parentPath);
                end = getClossestEndTile(newEnd.Childs, end);
            }

            path = getDirectPath(start, end, target, size, validChids);

            newEnd = path[path.Count - 1];

            return path;
        }

        static List<A_PathTile> getDirectPath(A_PathTile start, A_PathTile end, A_PathTile target, Vector3Int size, HashSet<A_PathTile> validChids) {
            Dictionary<A_PathTile, PathTile> clossedTiles = new Dictionary<A_PathTile, PathTile>();
            Dictionary<A_PathTile, PathTile> activeTiles = new Dictionary<A_PathTile, PathTile>();

            A_PathTile current;
            A_PathTile previous = null;

            float nextWalkCost;

            current = start;
            clossedTiles.Add(current, new PathTile(previous != null ? clossedTiles[previous] : null, current, end, 0));

            while(current != end && !end.contains(current)) {

                foreach(var conn in current.Connections) {
                    if(conn.State == PathTileState.Blocked) continue;
                    if(clossedTiles.ContainsKey(conn)) continue;
                    if(validChids != null && !validChids.Contains(conn)) continue;
                    if(!conn.canEnterFrom(current, size)) continue;

                    nextWalkCost = clossedTiles[current].WalkCost + Vector3.Distance(current.WorldPosition, conn.WorldPosition);

                    if(activeTiles.ContainsKey(conn)) {
                        activeTiles[conn].updateCost(nextWalkCost, clossedTiles[current]);
                    } else {
                        activeTiles.Add(conn, new PathTile(clossedTiles[current], conn, end, nextWalkCost));
                    }
                }

                current = getNextBestTile(activeTiles);

                if(current != null) {
                    //Debug.DrawRay(current.WorldPosition, Vector3.up * 2, Color.cyan, 5);
                    clossedTiles.Add(current, activeTiles[current]);
                    activeTiles.Remove(current);
                } else {
                    //found = false;
                    break;
                }

            }
            
            List<A_PathTile> path = createPath(clossedTiles, target);

            return path;
        }

        static List<A_PathTile> getPathToParents(A_PathTile start, List<A_PathTile> parentPath, A_PathTile target, Vector3Int size) {
            List<A_PathTile> path = new List<A_PathTile>();
            A_PathTile last = start;

            foreach(var parent in parentPath) {
                List<A_PathTile> path1 = getDirectPath(last, parent, parent, size, null);
                path.AddRange(path1);

                last = path[path.Count - 1];
            }

            path.AddRange(getDirectPath(last, target, target, size, null));

            return path;

        }

        static HashSet<A_PathTile> getValidChilds(List<A_PathTile> path) {
            HashSet<A_PathTile> childs = new HashSet<A_PathTile>();

            foreach(var item in path) {
                childs.UnionWith(item.Childs);
                foreach(var conn in item.Connections) {
                    childs.UnionWith(conn.Childs);
                }
            }

            return childs;
        }

        static A_PathTile getNextBestTile(Dictionary<A_PathTile, PathTile> activeTiles) {
            
            float minCost = 10000000;
            A_PathTile best = null;

            foreach(var item in activeTiles) {
                if(item.Value.Cost < minCost) {
                    minCost = item.Value.Cost;
                    best = item.Key;
                }
            }

            return best;

        }

        static List<A_PathTile> createPath(Dictionary<A_PathTile, PathTile> clossedTiles, A_PathTile target) {

            List<A_PathTile> path = new List<A_PathTile>();

            PathTile p = clossedTiles[getClossestEndTile(clossedTiles, target)];

            while(p != null) {
                path.Add(p.Reference);
                p = p.Parent;
            }

            path.Reverse();

            return path;
            
        }

        static A_PathTile getClossestEndTile(Dictionary<A_PathTile, PathTile> clossedTiles, A_PathTile target) {
            if(clossedTiles.ContainsKey(target)) return target;

            float dis;
            float minDis = 10000;
            A_PathTile best = null;

            foreach(var item in clossedTiles) {

                if(target.contains(item.Key)) {
                    best = item.Key;
                    break;
                }else if(item.Key.contains(target)) {
                    best = item.Key;
                    break;
                } else {
                    dis = Vector3.Distance(item.Key.WorldPosition, target.WorldPosition);
                }

                if(dis < minDis) {
                    minDis = dis;
                    best = item.Key;

                }
            }

            //Debug.DrawLine(best.WorldPosition, target.WorldPosition, Color.green, 10);
            return best;
        }
        static A_PathTile getClossestEndTile(HashSet<A_PathTile> clossedTiles, A_PathTile end) {
            if(clossedTiles.Contains(end)) return end;

            float dis;
            float minDis = 10000;
            A_PathTile best = null;

            foreach(var item in clossedTiles) {
                dis = item.distanceTo(end);// Vector3.Distance(item.WorldPosition, end.WorldPosition);
                if(dis < minDis) {
                    minDis = dis;
                    best = item;
                }
            }

            return best;
        }

    }




    class PathTile {

        public float Cost { get {
                return WalkCost + _distance * 2;
            }
        }

        float _distance;
        public float WalkCost { get; private set; }

        public PathTile Parent { get; private set; }
        public A_PathTile Reference { get; private set; }

        public PathTile(PathTile parent, A_PathTile reference, A_PathTile target, float walkCost) {
            Parent = parent;
            Reference = reference;
            WalkCost = walkCost;

            _distance = reference.distanceTo(target);// Vector3.Distance(Reference.WorldPosition, target.WorldPosition);
        }

        public void updateCost(float newWalkCost, PathTile newParent) {
            if(newWalkCost < WalkCost) {
                WalkCost = newWalkCost;
                Parent = newParent;
            }
        }
    }

}