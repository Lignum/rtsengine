﻿using RTS.MapAI.PathFinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RTS.MapAI.PathFinding {
    public class Path: IEnumerable {

        A_PathTile[] _tilePath;
        public A_PathTile this[int i] {
            get { return _tilePath[i]; }
        }
        public int Length {
            get { return _tilePath != null ? _tilePath.Length : 0; }
        }

        public A_PathTile Start { get; private set; }
        public A_PathTile Target { get; private set; }
        public Vector3 EndDirection { get; private set; }

        public Vector3 StartDirection { get { return (_tilePath[1].WorldPosition - _tilePath[0].WorldPosition).normalized;} }

        public A_PathTile EndPoint {
            get { return _tilePath[_tilePath.Length - 1]; }
        }

        public Path(A_PathTile start, A_PathTile target, Vector3 endDirection) {
            Start = start;
            Target = target;
            EndDirection = endDirection;
        }

        public void updateStart(A_PathTile start) {
            Start = start;
        }

        public Path(A_PathTile[] p) {
            setPath(p);
        }

        public void setPath (A_PathTile[] p) {
            _tilePath = p;
        }

        public void insertStartStep(A_PathTile step) {
            A_PathTile[] aux = new A_PathTile[_tilePath.Length + 1];
            aux[0] = step;

            _tilePath.CopyTo(aux, 1);

            _tilePath = aux;
        }

        public IEnumerator GetEnumerator() {
            foreach(var item in _tilePath) {
                yield return item;
            }
        }

        public void drawGizmos() {
            if(_tilePath != null) {
                for(int i = 0; i < _tilePath.Length - 1; i++) {
                    CustomGizmos.drawArrowLine(_tilePath[i].WorldPosition, _tilePath[i + 1].WorldPosition, .5f);
                }

                Gizmos.color = Color.yellow;
                Vector3 lastPos = _tilePath[_tilePath.Length - 1].WorldPosition;
                CustomGizmos.drawArrowLine(lastPos, lastPos + EndDirection * .5f, .25f);

            } 

            
            if(Start != null && Target != null) {
                Gizmos.color = Color.green;
                CustomGizmos.drawArrowLine(Start.WorldPosition, Target.WorldPosition, .5f);
            }
        }

    }
}