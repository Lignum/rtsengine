﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace RTS.MapAI.PathFinding {
    public class PathCalculator : MonoBehaviour {

        static ConcurrentDictionary<TileMapAgent, PathData> _pendingPaths = new ConcurrentDictionary<TileMapAgent, PathData>();
        static ConcurrentQueue<PathData> _pendingEnds = new ConcurrentQueue<PathData>();
        //static ConcurrentBag<TileMapAgent> _activeRequesters

        static Thread _calculationThread = new Thread(new ThreadStart(calculate));

        static bool isPLaying = true;

        static PathData _activePath;
        static PathData _currentEnd;

        static PathCalculator(){
            _calculationThread.Start();
        }
        private void Update() {
            while(_pendingEnds.Count > 0) {
                _currentEnd = _pendingEnds.Dequeue();
                _currentEnd.Agent.setPath(_currentEnd.Path);
            }
        }

        static void stop() {
            isPLaying = false;
        }

        public static void requestPath(TileMapAgent requester, A_PathTile target, Vector3 endDirection) {


            //if(start == null) Debug.LogError("Start Path is null");
            if(target == null) Debug.LogError("Target Path is null");
            if(requester == null) Debug.LogError("Target Requester is null");

            if(/*start != null &&*/ target != null && requester != null) {
                _pendingPaths.AddOrUpdate(requester, new PathData(requester, target, endDirection), updatePendingPath);

            }
            
        }

        public static A_PathTile getAgentDestination(TileMapAgent requester) {

            if(_pendingPaths.TryGetValue(requester, out PathData data)) {
                return data.Target;
            }

            return null;
        }

        static PathData updatePendingPath(TileMapAgent requester, PathData newData) {
            return newData;
        }

        static void calculate() {

            while(isPLaying) {

                //Thread.Sleep(100);

                if(_pendingPaths.Count > 0) {
                    if(_pendingPaths.TryRemove(_pendingPaths.Keys.First(), out _activePath)) {

                        _activePath.updatePathStart();
                        _activePath.Path.setPath(PathFinder.findPath(_activePath.Path.Start, _activePath.Path.Target, _activePath.Agent.BaseSize));

                        _pendingEnds.Enqueue(_activePath);
                    }
                } else {
                    Thread.Sleep(100);
                }

            }

        }

        private void OnApplicationQuit() {
            stop();
        }


        public struct PathData {
            public Path Path;
            public TileMapAgent Agent;
            public A_PathTile Target;

            public PathData(TileMapAgent a, A_PathTile target, Vector3 endDirection) {
                //Path = p;
                Agent = a;
                Target = target;
                
                Path = new Path(RTSTileMng.getTile(a.TilePosition), target, endDirection);
            }

            public void updatePathStart() {
                Path.updateStart( RTSTileMng.getTile(Agent.TilePosition));
            }
        }
    }
}