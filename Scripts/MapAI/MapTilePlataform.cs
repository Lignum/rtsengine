﻿using RTS.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.MapAI {

    [CreateAssetMenu(fileName = "TilePlataform", menuName = "RTS/Map/TilePlatafomr", order = 0)]
    public class MapTilePlataform : ScriptableObject {

        [SerializeField]
        Vector3[] _tilePositions;

        [SerializeField]
        FixedConnection[] _fixedConnections;

        public void createTiles(Vector3 position, int rotation) {
            MapTile created;
            Dictionary<Vector3Int, MapTile> createdList = new Dictionary<Vector3Int, MapTile>();

            //position = (position - Vector3.one * .5f);

            Vector3 rotatedPos;
            foreach(var item in _tilePositions) {
                rotatedPos = rotatePos(item, rotation);

                created = RTSTileMng.addTileAt<MapTile>(position + rotatedPos);

                createdList.Add(created.TilePosition, created);
            }

            Vector3Int tiledPos = RTSGameCtrl.worldToTilePosition(position);
            MapTile toTile;
            foreach(var item in _fixedConnections) {

                created = createdList[tiledPos + rotateTilePos(item.from, rotation)];
                toTile = RTSTileMng.getTile(tiledPos + rotateTilePos(item.to, rotation));

                created.addFixedConnection(toTile);

            }

        }

        Vector3 rotatePos(Vector3 pos, int rotation) {
            return Quaternion.Euler(0, rotation * 90, 0) * pos;
        }
        Vector3Int rotateTilePos(Vector3Int pos, int rotation) {
            return rotatePos(pos, rotation).toVector3IntRound();
        }

        [System.Serializable]
        public struct FixedConnection {

            public Vector3Int from, to;

        }

        public void drawGizmos(Vector3 position, int rotation) {

            Vector3Int tiledPos = RTSGameCtrl.worldToTilePosition(position);

            Vector3 rotatedPos;

            if(_tilePositions != null) {
                foreach(var item in _tilePositions) {
                    rotatedPos = rotatePos(item, rotation);

                    Gizmos.DrawCube(position + rotatedPos, Vector3.one * .2f);
                    MapTile.drawTileGizmo(RTSGameCtrl.worldToTilePosition(position + rotatedPos));
                }
            }

            if(_fixedConnections != null) {
                foreach(var item in _fixedConnections) {

                    CustomGizmos.drawArrowLine(RTSGameCtrl.tileToWorldPosition(tiledPos + rotateTilePos(item.from, rotation)) + Vector3.up * RTSGameCtrl.TileHeight * .5f,
                        RTSGameCtrl.tileToWorldPosition(tiledPos + rotateTilePos(item.to, rotation)) + Vector3.up * RTSGameCtrl.TileHeight * .5f, .2f);

                }
            }
        }
    }
}