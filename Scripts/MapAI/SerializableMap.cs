﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.MapAI {

    [CreateAssetMenu(fileName = "Map", menuName = "RTS/AI/Map", order = 1)]
    public class SerializableMap : ScriptableObject {

        [SerializeField]
        List<Vector3Int> tilesPositions;

        [SerializeField]
        List<SerializableMapTile> Tiles;

        [SerializeField]
        List<SerializableMapArea> Areas;

        public SerializableMap(Map map) {
            Dictionary<Vector3Int, MapTile> activeTiles = map.getActiveTiles();
            HashSet<MapArea> areas = map.getAreas();

            tilesPositions = new List<Vector3Int>();
            Tiles = new List<SerializableMapTile>();

            Dictionary<MapTile, SerializableMapTile> tileSerial = new Dictionary<MapTile, SerializableMapTile>();
            Dictionary<MapArea, SerializableMapArea> areaSerial = new Dictionary<MapArea, SerializableMapArea>();


            serializeTiles(activeTiles, tileSerial);
            serializeTileConnections(activeTiles, tileSerial);
            serializeAreas(areas, areaSerial, tileSerial);
            setTilesAreas(activeTiles, areaSerial, tileSerial);

        }

        void serializeTiles(Dictionary<Vector3Int, MapTile> activeTiles, Dictionary<MapTile, SerializableMapTile> tileSerial) {

            SerializableMapTile current;

            foreach(var item in activeTiles) {
                tilesPositions.Add(item.Key);

                current = new SerializableMapTile(item.Value);
                Tiles.Add(current);
                tileSerial.Add(item.Value, current);
            }
        }

        void serializeTileConnections(Dictionary<Vector3Int, MapTile> activeTiles, Dictionary<MapTile, SerializableMapTile> tileSerial) {

            SerializableMapTile current;
            foreach(var item in activeTiles) {
                current = tileSerial[item.Value];
                //current.setConnections(item.Value.Connections, tileSerial);
            }
        }

        void serializeAreas(HashSet<MapArea> mapAreas, Dictionary<MapArea, SerializableMapArea> areaSerial, Dictionary<MapTile, SerializableMapTile> tileSerial) {

            SerializableMapArea current;
            foreach(var item in mapAreas) {
                current = new SerializableMapArea(item, tileSerial);
                areaSerial.Add(item, current);
            }

            foreach(var item in mapAreas) {
                current = areaSerial[item];
               // current.setConnections(item.Connections, areaSerial);
            }
        }

        void setTilesAreas(Dictionary<Vector3Int, MapTile> activeTiles, Dictionary<MapArea, SerializableMapArea> areaSerial, Dictionary<MapTile, SerializableMapTile> tileSerial) {

            SerializableMapTile current;
            foreach(var item in activeTiles) {
                current = tileSerial[item.Value];
                //current.setArea(item.Value.Area, areaSerial);
            }

        }
    }


    [System.Serializable]
    public class SerializableMapTile {
        public SerializableMapArea Area;
        public Vector3 WorldPos;
        public Vector3 Normal;

        public List<SerializableMapTile> Connections;

        public SerializableMapTile(MapTile tile) {
            WorldPos = tile.WorldPosition;
            Normal = tile.Normal;
        }

        public void setConnections(HashSet<MapTile> conns, Dictionary<MapTile, SerializableMapTile> definition) {
            Connections = new List<SerializableMapTile>();

            foreach(var item in conns) {
                Connections.Add(definition[item]);
            }
        }

        public void setArea(MapArea mapArea, Dictionary<MapArea, SerializableMapArea> areaSerial) {
            Area = areaSerial[mapArea];
        }
    }

    [System.Serializable]
    public class SerializableMapArea {
        public List<SerializableMapTile> Tiles;
        public List<SerializableMapArea> Connections;

        public SerializableMapArea(MapArea area, Dictionary<MapTile, SerializableMapTile> tileSerial) {

            Tiles = new List<SerializableMapTile>();
            /*foreach(var item in area.Childs) {
                Tiles.Add(tileSerial[item]);
            }*/
        }

        public void setConnections(HashSet<MapArea> conn, Dictionary<MapArea, SerializableMapArea> areaSerial) {
            Connections = new List<SerializableMapArea>();

            foreach(var item in conn) {
                Connections.Add(areaSerial[item]);
            }
        }
    }
}