﻿using RTS.Game;
using RTS.MapAI.PathFinding;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RTS.MapAI {

    public static class MapAreaContructor {

        static bool _stopBuilding = false;
        public static void stopBuildTiles() {
            _stopBuilding = true;
        }

        public static HashSet<MapArea> createAreas(int maxMapAreaSize, Dictionary<Vector3Int, MapTile> activeTiles, int level) {
            HashSet<MapArea> areas = new HashSet<MapArea>();
            HashSet<A_PathTile> pendingTiles = new HashSet<A_PathTile>(activeTiles.Values);
            HashSet<A_PathTile> areaTiles;
            HashSet<A_PathTile> pendingConnections;
            HashSet<A_PathTile> annalizedConnections;

            A_PathTile current;


            bool inside;

            while(pendingTiles.Count > 0 && !_stopBuilding) {

                //Debug.Log($"Pending Tiles: {pendingTiles.Count} - Total Areas: {areas.Count}");

                areaTiles = new HashSet<A_PathTile>();
                pendingConnections = new HashSet<A_PathTile>();
                annalizedConnections = new HashSet<A_PathTile>();

                current = pendingTiles.First();

                pendingConnections.Add(current);

                while(pendingConnections.Count > 0) {

                    current = pendingConnections.First();
                    pendingConnections.Remove(current);
                    annalizedConnections.Add(current);

                    if(pendingTiles.Contains(current)) {
                        inside = true;
                        foreach(var item in areaTiles) {
                            if(maxMapAreaSize == -1 || 
                                Mathf.Abs(item.WorldPosition.x - current.WorldPosition.x) > maxMapAreaSize ||
                                Mathf.Abs(item.WorldPosition.z - current.WorldPosition.z) > maxMapAreaSize) {
                                inside = false;
                                break;
                            }
                        }

                        if(inside) {
                            pendingTiles.Remove(current);
                            areaTiles.Add(current);

                            foreach(var con in current.Connections) {
                                if(!annalizedConnections.Contains(con)) {
                                    pendingConnections.Add(con);
                                }
                            }
                        }
                    }
                }

                areas.Add(new MapArea(areaTiles, level));

            }

            return areas;

        }

        public static HashSet<MapArea>[] createAreaSubdivisions(MapArea area) {

            Dictionary<Vector3Int, MapTile> tiles_2 = new Dictionary<Vector3Int, MapTile>(20);
            Dictionary<Vector3Int, MapTile> tiles_3 = new Dictionary<Vector3Int, MapTile>(20);

            Vector3Int[] sizes_2 = new Vector3Int[2] { new Vector3Int(2, 1, 2), new Vector3Int(2, 1, 1) };
            Vector3Int[] sizes_3 = new Vector3Int[3] { new Vector3Int(3, 1, 3), new Vector3Int(3, 1, 2), new Vector3Int(3, 1, 1) };

            bool valid_2;
            bool valid_3;

            MapTile tile;

            foreach(var item in area.Childs) {

                tile = item as MapTile;

                valid_2 = false;
                valid_3 = false;

                foreach(var size in sizes_2) {
                    for(int i = 0; i < 4; i++) {
                        if(tile.canHaveSize(TiledObj.calculateRotatedSize(size, i))) {
                            valid_2 = true;
                            break;
                        }
                    }

                    if(valid_2) {
                        break;
                    }
                }

                foreach(var size in sizes_3) {
                    for(int i = 0; i < 4; i++) {
                        if(tile.canHaveSize(TiledObj.calculateRotatedSize(size, i))) {
                            valid_3 = true;
                            break;
                        }
                    }

                    if(valid_3) {
                        break;
                    }
                }

                if(valid_2) {
                    tiles_2.Add(tile.TilePosition, tile);
                }
                if(valid_3) {
                    tiles_3.Add(tile.TilePosition, tile);
                }
            }

            HashSet<MapArea>[] subdivisions = new HashSet<MapArea>[2];
            subdivisions[0] = createAreas(50, tiles_2, 2);
            subdivisions[1] = createAreas(50, tiles_3, 3);

            return subdivisions;
        }


    }
}
