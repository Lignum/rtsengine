﻿using RTS.MapAI.PathFinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.MapAI {

    public class MapArea : A_PathTile {

        public override int Level { get => 1; }

        HashSet<MapTile> _borderTiles;

        int _areaMaxObjSize;

        public MapArea (HashSet<A_PathTile> tiles, int objSize) {
            Childs = tiles;
            calculateCenter();

            _areaMaxObjSize = objSize;

            foreach(var item in tiles) {
                item.setParent(_areaMaxObjSize - 1, this);
            }

            createConnections();
            
        }

        public void createSubdivisions() {

            HashSet<MapArea>[] subdivisions = MapAreaContructor.createAreaSubdivisions(this);
        }

        void createConnections() {
            Connections = new HashSet<A_PathTile>();
            _borderTiles = new HashSet<MapTile>();

            foreach(var item in Childs) {
                foreach(var area in item.getConnectedAreas(_areaMaxObjSize - 1)) {
                    if(area == this) continue;
                    if(area != null) {
                        addConnection(area);
                    }
                    _borderTiles.Add(item as MapTile); 
                }
            }
        }

        void addConnection(A_PathTile area) {
            Connections.Add(area);
            area.Connections.Add(this);
        }

        void calculateCenter() {
            Vector3 center = Vector3.zero;
            foreach(var item in Childs) {
                center += item.WorldPosition;
            }

            center /= Childs.Count;

            WorldPosition = center;
        }
        
        public override bool canHaveSize(Vector3Int size) {

            if(Mathf.Abs(size.x) <= _areaMaxObjSize && Mathf.Abs(size.z) <= _areaMaxObjSize) return true;

            return false;
        }

        public override bool canEnterFrom(A_PathTile from, Vector3Int size) {

            if(Mathf.Abs(size.x) == 1 && Mathf.Abs(size.z) == 1) return true;

            foreach(var tile in _borderTiles) {
                foreach(var areaTile in tile.getConnectionsToArea(from as MapArea)) {
                    if(areaTile.canEnterFrom(tile, size)) return true;
                }
            }

            return false;
        }

        public void drawGizmos() {

            Gizmos.color = Color.yellow;
            Gizmos.DrawRay(WorldPosition, Vector3.up * 1);

            foreach(var item in Connections) {
                CustomGizmos.drawArrowLine(WorldPosition + Vector3.up * 1, item.WorldPosition + Vector3.up * 1, 2);
            }

            /*Gizmos.color = Color.blue;
            foreach(var item in _tiles) {
                Gizmos.DrawLine(Center, item.WorldPos);
            }*/

        }
    }
}