﻿using RTS.Game;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace RTS.MapAI {
    public class MapCtr : MonoBehaviour {

        static MapCtr INSTANCE;

        [SerializeField]
        Vector3Int mapSize;

        [SerializeField]
        Map preMap;

        /*[SerializeField]
        public string mapSaveDirectory;
        [SerializeField]
        public string mapName;*/

        public static Vector3Int MapSize { get { return getInstance().mapSize; } }
        public static Vector3 WorldMapSize { get; private set; }

        public static MapCtr getInstance() {

            if(INSTANCE == null) {
                INSTANCE = FindObjectOfType<MapCtr>();

               
                WorldMapSize = new Vector3(INSTANCE.mapSize.x * RTSGameCtrl.TileSize, INSTANCE.mapSize.y * RTSGameCtrl.TileHeight, INSTANCE.mapSize.z * RTSGameCtrl.TileSize);

                if(INSTANCE == null) {
                    Debug.LogError("Create a MapCtr object");
                }
            }
            return INSTANCE;
        }

        private void Awake() {
            calculateMap();
        }

        [ContextMenu("CalculateMap")]
        public void calculateMap() {
            TileMapCreator.setDimmensions(RTSGameCtrl.MaxStepHeight, RTSGameCtrl.MaxMapAreaSize, RTSGameCtrl.WorldLayer);
            preMap = TileMapCreator.buildTiles(mapSize);
            RTSTileMng.loadMap(preMap);

            Debug.Log("Created Map");
        }
        
        public Map getMap() {
            return preMap;
        }

        private void OnApplicationQuit() {
            TileMapCreator.stopBuildTiles();
        }
       

        private void OnDrawGizmosSelected() {


            RTSTileMng.drawGizmos();

            Gizmos.color = Color.red;
            Vector3 size = new Vector3(getInstance().mapSize.x * RTSGameCtrl.TileSize, getInstance().mapSize.y * RTSGameCtrl.TileHeight, getInstance().mapSize.z * RTSGameCtrl.TileSize);
            Gizmos.DrawWireCube(size / 2, size);
        }


    }
}