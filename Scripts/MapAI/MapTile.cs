﻿using RTS.Game;
using RTS.MapAI.PathFinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.MapAI {


    public class MapTile : A_PathTile {

        public Vector3Int TilePosition { get { return RTSGameCtrl.worldToTilePosition(WorldPosition); } }

        public Vector3 Normal { get; private set; }

        public TiledObj OcuppiedObj { get; private set; }
        TiledObj _reserveObj;

        public Vector3Int[] _validSizes;

        MapTile _overridedTile;
        HashSet<MapTile> _fixedConnections;

        public MapTile(Vector3 worldPos, Vector3 normal) {
            
            WorldPosition = worldPos;
            Normal = normal;

            State = PathTileState.Empty;
            Connections = new HashSet<A_PathTile>();

            _fixedConnections = new HashSet<MapTile>();
        }

        public void addFixedConnection(MapTile tile) {

            if(tile == null) return;
            if(_fixedConnections == null) _fixedConnections = new HashSet<MapTile>();

            _fixedConnections.Add(tile);
            tile._fixedConnections.Add(this);

            /*if(tile is FixedConectionMapTile) {
                (tile as FixedConectionMapTile)._fixedConnections.Add(this);
            }

            addConnection(tile);*/
        }

        public override void clearConnections() {
            //base.clearConnections();

            foreach(var item in Connections) {
                if(_fixedConnections == null || !_fixedConnections.Contains(item as MapTile)) {
                    item.removeConnection(this);
                }
            }

            Connections.Clear();

            if(_fixedConnections != null) {
                foreach(var item in _fixedConnections) {
                    addConnection(item);
                }
            }
        }

        public void setOverridedTile(MapTile tile) {
            _overridedTile = tile;
        }

        public void reserveTile(TiledObj obj) {
            _reserveObj = obj;
            updateState(PathTileState.Reserved);
        }

        public void emptyTile(TiledObj obj) {
            if(OcuppiedObj == obj || (OcuppiedObj == null && _reserveObj == obj)) {
                updateState(PathTileState.Empty);
                OcuppiedObj = null;
                _reserveObj = null;
            }
        }

        public void occupyTile(TiledObj obj, PathTileState state) {
            OcuppiedObj = obj;
            _reserveObj = null;
            updateState(state);
        }

        public void disableTile() {
            updateState(PathTileState.Disabled);
        }

        void updateState(PathTileState newState) {
            if(newState != PathTileState.Reserved) {
                if((State != PathTileState.Blocked && newState == PathTileState.Blocked && Parents[0] != null)
                    || (State == PathTileState.Blocked && newState != PathTileState.Blocked)) {

                    recalculateArea();
                }
            }

            State = newState;
        }

        public void recalculateArea() {

            MapArea area;
            if(Parents[0] == null) {
                area = getClossestArea();
                area.Childs.Add(this);
            } else {
                area = Parents[0] as MapArea;
            }
            TileMapCreator.addAreaForRecalculation(area);
        }

        public MapArea getClossestArea() {
            if(Parents[0] != null) {
                return Parents[0] as MapArea;
            } else {
                return RTSTileMng.LoadedMap.getClossetsArea(WorldPosition);
            }
        }

        public void addConnection(MapTile connection) {
            Connections.Add(connection);
            connection.Connections.Add(this);
        }

        public void setValidSizes(Vector3Int[] sizes) {
            _validSizes = sizes;
        }

        public HashSet<MapTile> getConnectionsToArea(MapArea area) {
            HashSet<MapTile> border = new HashSet<MapTile>();
            foreach(var item in Connections) {
                for(int i = 0; i < Parents.Length; i++) {
                    if(item.Parents[i] == area) border.Add(item as MapTile);
                }
            }

            return border;
        }

        public HashSet<MapTile> getExpansion(int size) {
            int cSize = 0;

            HashSet<MapTile> expandend = new HashSet<MapTile>();
            expandend.Add(this);

            Queue<A_PathTile> pending = new Queue<A_PathTile>();
            pending.Enqueue(this);

            A_PathTile current;

            int pSize;

            while(cSize < size) {

                pSize = pending.Count;

                for(int i = 0; i < pSize; i++) {
                    current = pending.Dequeue();

                    foreach(var item in current.Connections) {
                        if(!expandend.Contains(item as MapTile)) {
                            expandend.Add(item as MapTile);
                            pending.Enqueue(item);
                        }
                    }

                }
                cSize++;
            }

            return expandend;
        }

        public HashSet<MapTile> getExpansionTillState(int maxSize, TileStateMask stateMask, TiledObj requester) {
            int cSize = 0;

            HashSet<MapTile> expandend = new HashSet<MapTile>();

            if((stateMask & (int)State) > 0 || OcuppiedObj == requester) {
                expandend.Add(this);
            } else { 
                bool found = false;
                Queue<A_PathTile> pending = new Queue<A_PathTile>();
                pending.Enqueue(this);

                A_PathTile current;

                int pSize;

                while(cSize < maxSize) {

                    pSize = pending.Count;

                    for(int i = 0; i < pSize; i++) {
                        current = pending.Dequeue();

                        foreach(var item in current.Connections) {
                            if(!expandend.Contains(item as MapTile)) {
                                pending.Enqueue(item);

                                if((stateMask & (int)item.State) > 0 || (item as MapTile).OcuppiedObj == requester) {
                                    expandend.Add(item as MapTile);
                                    found = true;
                                }
                            }
                        }

                        if(found) break;

                    }
                    cSize++;
                    if(found) break;
                }

            }

            return expandend;
        }

        public virtual bool canConnectWith(MapTile t2, float maxStepHeight) {

            return minStepDistance(this, t2) <= maxStepHeight;

        }



        public static float minStepDistance(MapTile t1, MapTile t2) {

            Quaternion t1Rotation = Quaternion.FromToRotation(Vector3.up, t1.Normal);

            Vector3[] t1C = new Vector3[4] {
                t1.WorldPosition + t1Rotation * new Vector3(1, 0, 1) * RTSGameCtrl.TileSize / 2,
                t1.WorldPosition + t1Rotation * new Vector3(1, 0, -1) * RTSGameCtrl.TileSize / 2,
                t1.WorldPosition + t1Rotation * new Vector3(-1, 0, -1) * RTSGameCtrl.TileSize / 2,
                t1.WorldPosition + t1Rotation * new Vector3(-1, 0, 1) * RTSGameCtrl.TileSize / 2
            };

            Quaternion t2Rotation = Quaternion.FromToRotation(Vector3.up, t2.Normal);

            Vector3[] t2C = new Vector3[4] {
                t2.WorldPosition + t2Rotation * new Vector3(1, 0, 1) * RTSGameCtrl.TileSize / 2,
                t2.WorldPosition + t2Rotation * new Vector3(1, 0, -1) * RTSGameCtrl.TileSize / 2,
                t2.WorldPosition + t2Rotation * new Vector3(-1, 0, -1) * RTSGameCtrl.TileSize / 2,
                t2.WorldPosition + t2Rotation * new Vector3(-1, 0, 1) * RTSGameCtrl.TileSize / 2
            };

            float minDist = Vector3.Distance(t1C[0], t2C[0]);
            float dis;

            foreach(var t1Pos in t1C) {

                foreach(var t2Pos in t2C) {
                    dis = Vector3.Distance(t1Pos, t2Pos);
                    if(dis < minDist) {
                        minDist = dis;
                    }
                }

            }


            return minDist;
        }
        
        public override bool canEnterFrom(A_PathTile from, Vector3Int size) {

            if(Mathf.Abs(size.x) == 1 && Mathf.Abs(size.z) == 1) return true;

            Vector3 dir = (WorldPosition - from.WorldPosition).normalized;
            //Debug.Log(dir);
            //string s = "";

            HashSet<int> checkDirs = new HashSet<int>();

            if(dir.z > 0) checkDirs.Add(0);
            else if(dir.z < 0) checkDirs.Add(2);

            if(dir.x > 0) checkDirs.Add(1);
            else if(dir.x < 0) checkDirs.Add(3);

            /*foreach(var item in checkDirs) {
                s += item + ", ";
            }
            Debug.Log(s);*/

            foreach(var d in checkDirs) {
                if(canHaveSize(TiledObj.calculateRotatedSize(size, d))) return true;
            }

            return false;
        }

        public override bool canHaveSize(Vector3Int size) {
            foreach(var item in _validSizes) {
                if(item == size) return true;
            }
            return false;
        }
        
        public void drawGizmos() {

            switch(State) {
                case PathTileState.Empty:
                    return;
                    //Gizmos.color = Color.white;
                    //break;
                case PathTileState.Reserved:
                    Gizmos.color = Color.blue;
                    break;
                case PathTileState.Ocuppied:
                    Gizmos.color = Color.red;
                    break;
                case PathTileState.Blocked:
                    Gizmos.color = Color.black;
                    break;
                case PathTileState.Disabled:
                    Gizmos.color = Color.yellow;
                    break;
            }

            drawTileGizmo(RTSGameCtrl.worldToTilePosition(WorldPosition));
            
            if(OcuppiedObj != null) {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(WorldPosition, OcuppiedObj.WorldPosition);
            }
            if(_reserveObj != null) {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(WorldPosition, _reserveObj.WorldPosition);
            }
        }

        public static void drawTileGizmo(Vector3Int position) {

            //Gizmos.DrawWireCube(position + RTSGameCtrl.TileDes, Vector3.one * RTSGameCtrl.TileSize);
            Gizmos.DrawWireCube(RTSGameCtrl.tileToWorldPosition(position) + Vector3.up * RTSGameCtrl.TileHeight/2, RTSGameCtrl.TileSizeV);
            
        }
        public void drawConnectionsGizmos() {

            //Gizmos.DrawWireCube(position + RTSGameCtrl.TileDes, Vector3.one * RTSGameCtrl.TileSize);
            Gizmos.color = Color.yellow;
            Gizmos.DrawRay(WorldPosition, Normal * 2);

            if(Parents[0] != null) {
                Gizmos.color = Color.gray;
                Debug.DrawLine(WorldPosition, Parents[0].WorldPosition);
            }

            if(Parents[1] != null) {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(WorldPosition, Parents[1].WorldPosition);
            }

            if(Parents[2] != null) {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(WorldPosition, Parents[2].WorldPosition);
            }

            Gizmos.color = Color.blue;
            foreach(var item in Connections) {
                CustomGizmos.drawArrowLine(WorldPosition, (item as MapTile).WorldPosition, .5f);
            }


            
        }
    }
    
}