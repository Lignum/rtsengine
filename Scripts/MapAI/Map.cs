﻿using RTS.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.MapAI {

    public class Map/* : ScriptableObject*/ {

        Dictionary<Vector3Int, MapTile> _activeTiles;// = new Dictionary<Vector3Int, MapTile>();
        HashSet<MapArea> _areas;// = new HashSet<MapArea>();

        public bool isFinish = false;

        public Map(Dictionary<Vector3Int, MapTile> activeTiles) {
            _activeTiles = activeTiles;
            _areas = new HashSet<MapArea>();
        }

        public void addAreas(HashSet<MapArea> areas) {
            _areas.UnionWith(areas);
            isFinish = true;
        }
        public void removeAreas(HashSet<MapArea> areas) {
            foreach(var item in areas) {
                _areas.Remove(item);
            }
        }

        public Dictionary<Vector3Int, MapTile> getActiveTiles() {
            return new Dictionary<Vector3Int, MapTile>(_activeTiles);
        }

        public HashSet<MapArea> getAreas() {
            return new HashSet<MapArea>(_areas);
        }

        public MapTile getTile(Vector3Int tilePosition) {
            if(_activeTiles.TryGetValue(tilePosition, out MapTile tile)) {
                return tile;
            }
            return null;
        }

        public void addTile(MapTile tile) {
            _activeTiles.Add(tile.TilePosition, tile);
        }
        public void overrideTile(MapTile tile) {
            MapTile current = getTile(tile.TilePosition);
            current.clearConnections();
            current.disableTile();

            tile.setOverridedTile(current);

            _activeTiles.Remove(tile.TilePosition);
            _activeTiles.Add(tile.TilePosition, tile);
        }

        public bool tryGetTile(Vector3Int tilePosition, out MapTile tile) {
            return _activeTiles.TryGetValue(tilePosition, out tile);
        }
        public bool tryGetTile(Vector3Int tilePosition, int stateMask, out MapTile tile) {
            if( _activeTiles.TryGetValue(tilePosition, out tile)) {
                if(((int)tile.State & stateMask) > 0) {
                    return true;
                }
            }
            return false;
        }

        public T getTileObjAt<T>(Vector3Int tilePosition) where T : TiledObj {
            if(_activeTiles.TryGetValue(tilePosition, out MapTile tile)) {
                return tile.OcuppiedObj as T;
            }

            return null;
        }

        public MapArea getClossetsArea(Vector3 position) {
            MapArea clossets = null;
            float dis;
            float minDis = float.MaxValue;

            foreach(var area in _areas) {
                dis = Vector3.Distance(position, area.WorldPosition);

                if(dis < minDis) {
                    minDis = dis;
                    clossets = area;
                }
            }

            return clossets;
        }

        public void drawGizmos() {
            if(_activeTiles != null) {
                foreach(var item in _activeTiles) {
                    item.Value.drawGizmos();
                }
            }
            
            if(_areas != null) {
                foreach(var item in _areas) {
                    item.drawGizmos();
                }
            }
        }

    }
}