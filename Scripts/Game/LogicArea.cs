﻿using RTS.Buildings;
using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using RTS.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Game {
    public class LogicArea  {
        
        List<RTSTeamEntity> _agents;
        List<RTSTeamEntity> _subAgents;

        public LogicArea() {
            _agents = new List<RTSTeamEntity>();
            _subAgents = new List<RTSTeamEntity>();
        }

        public void subscribeAgent(RTSTeamEntity agent) {
            _subAgents.Add(agent);
        }
        public void unSubscribedAgent(RTSTeamEntity agent) {
            _subAgents.Remove(agent);
        }

        public void setForUpdate(RTSAgent agent) {
            LogicAreasMng.addForUpdate(this, agent);
        }

        public void updatedAgent(RTSAgent nAgent) {
            foreach(var item in _subAgents) {
                if(item == nAgent) continue;
                if(item.canSeeAgent(nAgent)) item.addVisibleEntity(nAgent);
                else item.removeVisibleEntity(nAgent);
            }
        }

        public void addEntity(RTSTeamEntity agent) {
            _agents.Add(agent);
        }

        public void removeEntity(RTSTeamEntity agent) {
            _agents.Remove(agent);
        }


        public void drawGizmos(Vector3 center) {

            /*Gizmos.color = Color.yellow;
            foreach(var agent in _agents) {
                Gizmos.DrawLine(center, agent.WorldPosition);
            }*/
        }
        
        public List<RTSTeamEntity> getEntitiesAtDinstace(Vector3 position, float distance) {
            List<RTSTeamEntity> auxAgents = new List<RTSTeamEntity>();

            foreach(var agent in _agents) {
                if(Vector3.Distance(position, agent.WorldPosition) <= distance) {
                    auxAgents.Add(agent);
                }
            }

            return auxAgents;
        }
    }
}