﻿using RTS.Buildings;
using RTS.Data.ResourceMaterials;
using RTS.MapAI;
using Shared.Collections.Pools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RTS.Game {

    public static class GameDataMng {
        
        public static SimpleMonoBehaviourPool<BuildingOrder> BuildingsOrderPool;
        public static SimpleMonoBehaviourPool<ResourcePile> ResourcePilesPool;

        //static Dictionary<string, ElementCost> _elementsCosts;

        public static int MaterialResourcesCount { get { return resourcesNames.Length; } }
        static string[] resourcesNames = new string[4] { "Food", "Wood", "Stone", "Metal" };
        static Color[] ResourceColors { get; } = new Color[4] { Color.green, Color.red, Color.gray, Color.black };

        public static void init() {
            BuildingsOrderPool = new SimpleMonoBehaviourPool<BuildingOrder>(Resources.Load<BuildingOrder>("Buildings/BuildingOrder"));
            ResourcePilesPool = new SimpleMonoBehaviourPool<ResourcePile>(Resources.Load<ResourcePile>("Buildings/ResourcePile"));
        }



        public static string getMaterialResourceName(int resource) {
            return resourcesNames[resource];
        }

        public static ResourcePile spawnMaterialResourcePile(MapTile tile) {
            ResourcePile pile = ResourcePilesPool.spawn(tile.WorldPosition);

            pile.place(tile.TilePosition);

            return pile;
        }

       /* public static ElementCost getElementCost(string elementName) {
            Debug.Log(elementName);
            return _elementsCosts[elementName];
        }*/

        public static void drawMaterialResourcesGizmos(Vector3 position, int type, int cuantity) {
            if(type <= -1) return;

            Gizmos.color = ResourceColors[type];
            for(int i = 0; i < cuantity; i++) {
                Gizmos.DrawCube(position + Vector3.up * i * .25f, Vector3.one * .25f);
            }
        }

    }

}