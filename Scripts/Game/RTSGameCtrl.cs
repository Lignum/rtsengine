﻿using UnityEngine;
using RTS.MapAI.PathFinding;

namespace RTS.Game {

    [RequireComponent(typeof(EntitiesCtlr))]
    [RequireComponent(typeof(LogicAreasMng))]
    [RequireComponent(typeof(PathCalculator))]
    public class RTSGameCtrl : MonoBehaviour {

        private static RTSGameCtrl INSTANCE;
        
        [SerializeField]
        float _tileSize;
        [SerializeField, Min(1)]
        float _tileHeight;

        [SerializeField]
        LayerMask _worldLayer;

        [SerializeField, Min(0)]
        float _maxStepHeight;
        [SerializeField, Min(10)]
        int _maxMapAreaSize = 10;


        public static float TileSize {
            get {
                return getInstance()._tileSize;
            }
        }
        public static float TileHeight {
            get {
                return getInstance()._tileHeight;
            }
        }
        public static float MaxStepHeight {
            get {
                return getInstance()._maxStepHeight;
            }
        }
        public static int MaxMapAreaSize {
            get {
                return getInstance()._maxMapAreaSize;
            }
        }
        public static LayerMask WorldLayer {
            get {
                return getInstance()._worldLayer;
            }
        }

        public static Vector3 TileDes { get; private set; }
        public static Vector3 FlatTileDes { get; private set; }
        public static Vector3 TileSizeV { get; private set; }

        private void Awake() {
            GameDataMng.init();
        }

        public static RTSGameCtrl getInstance() {
            if(INSTANCE == null) {
                INSTANCE = FindObjectOfType<RTSGameCtrl>();

                TileDes = new Vector3(1, 0, 1) * TileSize / 2f + Vector3.up * TileHeight / 2f;
                FlatTileDes = new Vector3(1, 0, 1) * TileSize / 2f;

                TileSizeV = new Vector3(TileSize, TileHeight, TileSize);
            }

            return INSTANCE;
        }

        //private void Awake() {
            //INSTANCE = this;
       // }

        
        public static int worldToTileDistance(float distance) {
            return Mathf.RoundToInt(distance * TileSize);
        }

        public static Vector3Int worldToTilePosition(Vector3 pos) {
            return new Vector3Int((int)(pos.x / TileSize), (int)(pos.y / TileHeight), (int)(pos.z / TileSize));
            //return (pos / TileSize).toVector3IntFloor();
        }
        public static Vector3 tileToWorldPosition(Vector3Int pos) {
            return new Vector3(pos.x * TileSize, pos.y * TileHeight, pos.z * TileSize) + FlatTileDes;
            //return ((Vector3)pos * TileSize) + getInstance().FlatTileDes;
        }

        private void OnApplicationQuit() {
        }

    }
}
