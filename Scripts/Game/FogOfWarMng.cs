﻿using RTS.Game.Entities;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace RTS.Game {
    
    public class FogOfWarMng : MonoBehaviour {

        static FogOfWarMng INSTANCE;

        [SerializeField]
        Color _fogColor;

        [SerializeField]
        Color _mistColor;

        [SerializeField]
        Material[] materials;

        Texture2D _fogTexture;
        Color[] _colors;
        Color[] _colorsNew;
        bool _colorsChange = false;
        float[,] _levelMap;
        
        int _width;
        int _height;

        bool _updatingTexture;
        int _areaSize;

        ConcurrentQueue<KeyValuePair<Vector3Int, RTSTeamEntity>> _pendingAddAreas = new ConcurrentQueue<KeyValuePair<Vector3Int, RTSTeamEntity>>();

        //Dictionary<Vector3Int, HashSet<RTSTeamEntity>> _pendingAddAreas = new Dictionary<Vector3Int, HashSet<RTSTeamEntity>>();
        Dictionary<Vector3Int, HashSet<RTSTeamEntity>> _updatedAreas = new Dictionary<Vector3Int, HashSet<RTSTeamEntity>>();

        HashSet<Vector3Int> _clearedAreas = new HashSet<Vector3Int>();
        //HashSet<RTSTeamEntity> _clearedAgents = new HashSet<RTSTeamEntity>();

        Thread _updateThread;
        bool _isPlaying = true;

        public static FogOfWarMng getInstance() {
            if(INSTANCE == null) {
                INSTANCE = FindObjectOfType<FogOfWarMng>();
            }
            return INSTANCE;
        }

        private void Awake() {

            INSTANCE = FindObjectOfType<FogOfWarMng>();
            
            _areaSize = LogicAreasMng.getInstance().AreaSize;

            createTexture();

            _updateThread = new Thread(new ThreadStart(updateThreaded));
            _updateThread.Start();
        }

        private void Update() {
            //if(!_updatingTexture) {
            if(_colorsChange) {
                updateTexture();
                _colorsChange = false;
            }
        }

        [ContextMenu("Show All")]
        public void showAll() {

            for(int i = 0; i < _colorsNew.Length; i++) {
                _colorsNew[i] = Color.white;
            }
        }

        void updateThreaded() {

            while(_isPlaying) {

                Thread.Sleep(100);
                updateVisibleFog();
            }
        }

        public void addUpdatedArea(RTSTeamEntity caster, HashSet<Vector3Int> position) {
            Vector3Int key;
            foreach(var item in position) {
                key = new Vector3Int(item.x, 0, item.z);

                _pendingAddAreas.Enqueue(new KeyValuePair<Vector3Int, RTSTeamEntity>(key, caster));
            }
        }

        void addPendingAreas() {
            while(_pendingAddAreas.Count > 0) {
                KeyValuePair<Vector3Int, RTSTeamEntity> area = _pendingAddAreas.Dequeue();

                if(!_updatedAreas.ContainsKey(area.Key)) {
                    _updatedAreas.Add(area.Key, new HashSet<RTSTeamEntity>());
                }
                
                _updatedAreas[area.Key].Add(area.Value);
            }
        }

        void createTexture() {
            _width = RTS.MapAI.MapCtr.MapSize.x;
            _height = RTS.MapAI.MapCtr.MapSize.z;

            _fogTexture = new Texture2D(_width, _height, TextureFormat.BGRA32, false);//, TextureFormat.RGBA32, false);
            _fogTexture.name = "FOG_texture";
            _fogTexture.wrapMode = TextureWrapMode.Clamp;
            _fogTexture.filterMode = FilterMode.Trilinear;

            _levelMap = new float[_width, _height];

            _colors = new Color[_width * _height];
            _colorsNew = new Color[_width * _height];

            for(int x = 0; x < _width; x++) {
                for(int z = 0; z < _height; z++) {
                    //Debug.Log($"{x}, {z} -> {(z * _heigth) + x}");
                    _colorsNew[(z * _width) + x] = _mistColor;

                    //_colorsNew[(z * _width) + x] = _fogColor;
                    //_colorsNew[(z * _width) + x].a = 0;
                }
            }

            foreach(var mat in materials) {
                mat.SetTexture("_FogTexture", _fogTexture);
                mat.SetFloat("_MapWidth", _width);
            }            
        }
        

        public void updateVisibleFog() {
            Vector3Int start;

            Vector3Int pos = Vector3Int.zero;

            _updatingTexture = true;

            bool clean;
            bool visible;

            _clearedAreas.Clear();
            //_clearedAgents.Clear();

            //Vector3 areaCenter;

            addPendingAreas();


            foreach(var area in _updatedAreas) {
                clean = false;

                area.Value.RemoveWhere(disabledEntity);

                start = area.Key * _areaSize;

                for(int x = start.x; x < start.x + _areaSize; x++) {
                    for(int z = start.z; z < start.z + _areaSize; z++) {

                        if(x < 0 || z < 0 || x >= _width || z >= _height) continue;

                        pos.x = x;
                        pos.z = z;

                        visible = false;

                        foreach(var entity in area.Value) {

                            if(Vector3.Distance(entity.WorldPosition, RTSGameCtrl.tileToWorldPosition(pos)) <= entity.VisibleDistance) {
                                _colorsNew[(z * _height) + x] = Color.white;
                                clean = true;
                                visible = true;
                                //_clearedAgents[item] = true;
                                break;
                            }
                        }

                        if(!visible && _colorsNew[(z * _height) + x] == Color.white) {
                            _colorsNew[(z * _height) + x] = _fogColor;
                        }
                    }
                }
                    

                /*areaCenter = (area.Key * _areaSize) + (new Vector3(1,0,1) * _areaSize * .5f);
                

                foreach(var entity in area.Value) {
                    if(Vector3.Distance(entity.WorldPosition, areaCenter) > (entity.VisibleDistance + (_areaSize * .5f))) {
                        _clearedAgents.Add(entity);
                    }
                }
                foreach(var item in _clearedAgents) {
                    area.Value.Remove(item);
                }

                _clearedAgents.Clear();*/

                if(clean == false) {
                    _clearedAreas.Add(area.Key);
                }

                //Debug.Log(area);


            }

            _colorsNew.CopyTo(_colors, 0);
            _colorsChange = true;

            foreach(var item in _clearedAreas) {
                _updatedAreas.Remove(item);
            }

            //_updatedAreas.Clear();

            _updatingTexture = false;
        }

        bool disabledEntity(RTSTeamEntity entity) {
            return !entity.IsActive || entity.TeamKey != Player.RTSPlayerCtrl.PlayerTeam;
        }

        void updateTexture() {

            _fogTexture.SetPixels(_colors);
            _fogTexture.Apply(false);
        }


        private void OnDrawGizmosSelected() {
            Vector3 center;

            
            foreach(var item in _updatedAreas) {

                if(_updatingTexture) break;

                center = (item.Key * LogicAreasMng.getInstance().AreaSize) + (Vector3.one * LogicAreasMng.getInstance().AreaSize * .5f);
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireCube(center, Vector3.one * LogicAreasMng.getInstance().AreaSize);

                Gizmos.color = Color.red;
                foreach(var entity in item.Value) {

                    Gizmos.DrawLine(center, entity.WorldPosition);
                }

            }
        }

        private void OnApplicationQuit() {
            _updateThread.Abort();
            _isPlaying = false;
        }

    }
}