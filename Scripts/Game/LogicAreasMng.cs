﻿using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Game {
    public class LogicAreasMng : MonoBehaviour {

        static LogicAreasMng INSTANCE;

        [SerializeField, Range(5, 100)]
        int _areaSize = 10;
        public int AreaSize { get { return _areaSize; } }

        static Vector3Int _numAreas;

        static Dictionary<Vector3Int, LogicArea> _areas;
        static HashSet<KeyValuePair<LogicArea, RTSAgent>> _pendingUpdateAreas = new HashSet<KeyValuePair<LogicArea, RTSAgent>>();

        public static LogicAreasMng getInstance() {
            if(INSTANCE == null) {
                INSTANCE = FindObjectOfType<LogicAreasMng>();
            }

            return INSTANCE;
        }

        void Awake() {
            createDivisions();
        }

        void createDivisions() {
            _areas = new Dictionary<Vector3Int, LogicArea>();

            Vector3Int mapSize = RTS.MapAI.MapCtr.MapSize;

            _numAreas = mapSize / _areaSize;

            _numAreas = Vector3Int.Max(_numAreas, Vector3Int.one);

            Vector3Int pos = Vector3Int.zero;

            for(pos.x = 0; pos.x < _numAreas.x; pos.x++) {
                for(pos.y = 0; pos.y < _numAreas.y; pos.y++) {
                    for(pos.z = 0; pos.z < _numAreas.z; pos.z++) {

                        _areas.Add(pos, new LogicArea());

                    }
                }
            }
        }

        public static void addForUpdate(LogicArea area, RTSAgent agent) {
            _pendingUpdateAreas.Add(new KeyValuePair<LogicArea, RTSAgent>(area, agent));
        }

        public static void updatePendingAreas() {
            foreach(var item in _pendingUpdateAreas) {
                item.Key.updatedAgent(item.Value);
            }

            _pendingUpdateAreas.Clear();
        }

        public static void updateEntityArea(RTSTeamEntity agent) {
            LogicArea area = getAreaAt(agent.WorldPosition);
            if(agent.CurrentArea != area) {
                if(agent.CurrentArea != null)
                    agent.CurrentArea.removeEntity(agent);

                area.addEntity(agent);
                agent.setArea(area);
            }
        }


        public static LogicArea getAreaAt(Vector3Int tilePosition) {
            return _areas[tileToAreaPosition(tilePosition)];
        }

        public static LogicArea getAreaAt(Vector3 position) {
            return _areas[worldToAreaPosition(position)];
        }

        public static HashSet<RTSTeamEntity> getEntitiesAtDistance(Vector3 position, float distance) {
            List<LogicArea> areas = getAreasAtDistance(position, distance);

            HashSet<RTSTeamEntity> agents = new HashSet<RTSTeamEntity>();
            List<RTSTeamEntity> aux;

            foreach(var item in areas) {
                aux = item.getEntitiesAtDinstace(position, distance);
                foreach(var ag in aux) {
                    agents.Add(ag);
                }
            }

            return agents;
        }

       public static List<LogicArea> getAreasAtDistance(Vector3 position, float distance) {
            Vector3Int min = worldToAreaPosition(position - Vector3.one * distance);
            Vector3Int max = worldToAreaPosition(position + Vector3.one * distance);

            List<LogicArea> areas = new List<LogicArea>(10);

            Vector3Int pos = Vector3Int.zero;

            for(pos.x = min.x; pos.x <= max.x; pos.x++) {
                for(pos.y = min.y; pos.y <= max.y; pos.y++) {
                    for(pos.z = min.z; pos.z <= max.z; pos.z++) {
                        if(isAreaPositionInside(pos)) {
                            areas.Add(_areas[pos]);
                        }
                    }
                }
            }

            return areas;
        }

        public static HashSet<Vector3Int> getAreasPositionsAtDistance(Vector3 position, float distance) {
            Vector3Int min = worldToAreaPosition(position - Vector3.one * distance);
            Vector3Int max = worldToAreaPosition(position + Vector3.one * distance);

            HashSet<Vector3Int> areas = new HashSet<Vector3Int>();

            Vector3Int pos = Vector3Int.zero;

            for(pos.x = min.x; pos.x <= max.x; pos.x++) {
                for(pos.y = min.y; pos.y <= max.y; pos.y++) {
                    for(pos.z = min.z; pos.z <= max.z; pos.z++) {
                        //Debug.Log($"## {pos} ##");
                        areas.Add(pos);
                    }
                }
            }

            return areas;
        }

        static bool isAreaPositionInside(Vector3Int pos) {
            if(pos.x < 0) return false;
            if(pos.y < 0) return false;
            if(pos.z < 0) return false;
            if(pos.x >= _numAreas.x) return false;
            if(pos.y >= _numAreas.y) return false;
            if(pos.z >= _numAreas.z) return false;

            return true;

        }

        public static Vector3Int worldToAreaPosition(Vector3 position) {
            return (position / getInstance()._areaSize).toVector3IntFloor();
        }
        public static Vector3Int tileToAreaPosition(Vector3Int tilePosition) {
            return worldToAreaPosition(RTSGameCtrl.tileToWorldPosition(tilePosition));
        }

        private void OnDrawGizmosSelected() {
            
            if(_areas != null) {

                Vector3 center;
                foreach(var item in _areas) {
                    center = (item.Key * _areaSize) + (Vector3.one * _areaSize * .5f);
                    Gizmos.color = Color.cyan;
                    Gizmos.DrawWireCube(center, Vector3.one * _areaSize);
                    item.Value.drawGizmos(center);
                }

            }

        }

        private void OnApplicationQuit() {
            _areas = null;
        }

    }
}
