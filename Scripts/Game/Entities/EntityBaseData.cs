﻿using RTS.Data.Menus;
using RTS.Data.ResourceMaterials;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Game.Entities {

    [CreateAssetMenu(fileName = "EntityData", menuName = "RTS/Entities/EntityData", order = 1)]
    public class EntityBaseData : ScriptableObject{

        [HideInInspector]
        int _uniqueKey = -1;
        public int UniqueKey {
            get { return _uniqueKey; }
            set { setKey(value); }
        }

        public void setKey(int key) {
            if(_uniqueKey == -1) {
                _uniqueKey = key;
            } else {
                Debug.LogError($"Can't edit uniqueKey of {Name} : {_entityName}");
            }
        }
        public void resetKey() {
            _uniqueKey = -1;
        }


        //TileObj
        [SerializeField]
        Vector3Int _size = Vector3Int.one;
        public Vector3Int Size { get { return _size; } }

        //RTSEntity
        [SerializeField]
        string _entityName;
        public string Name { get { return _entityName; } }

        [SerializeField, Min(1)]
        int _maxLife = 100;
        public int MaxLife { get { return _maxLife; } }

        //RTSTeamEntity
        [SerializeField, Min(0)]
        float _visibleDistance = 10;
        public float VisibleDistance { get { return _visibleDistance * RTSGameCtrl.TileSize; } }

        [SerializeField]
        MenuData _menuData;
        public MenuData MenuData { get { return _menuData; } }

        [SerializeField]
        bool _canBeTeam = true;
        public bool CanBeTeam { get { return _canBeTeam; } }

        //TileMapAgent
        [SerializeField, Min(0.1f)]
        float _speed = 5;
        public float Speed { get { return _speed; } }

        [SerializeField, Min(1f)]
        float _rotationSpeed = 100;
        public float RotationSpeed { get { return _rotationSpeed; } }
        
        [SerializeField]
        bool _canMoveWhileRotating = true;
        public bool CanMoveWhileRotating { get { return _canMoveWhileRotating; } }

        [SerializeField]
        ElementCost _buildCost;
        public ElementCost BuildCost { get { return _buildCost; } }
        
        [SerializeField]
        int _buildTickTime = 20;
        public int BuildTickTime { get { return _buildTickTime; } }


    }
}