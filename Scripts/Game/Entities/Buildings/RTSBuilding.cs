﻿using RTS.AI;
using RTS.Game;
using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using RTS.Game.Teams;
using RTS.MapAI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace RTS.Buildings {
    
    //[RequireComponent(typeof(NavMeshObstacle))]
    public class RTSBuilding : RTSTeamEntity {
        
        public A_BuildingAI[] BuildingsAI { get; private set; }

        Queue<AgentState> _insideAgentsKeys;

        Action _onPlaced;

        protected override void Start() {
            base.Start();

            EntitiesCtlr.addEntity(this);
            //EntitiesCtlr.addBuilding(this);
        }

        protected override void Awake() {
            base.Awake();
            BuildingsAI = GetComponents<A_BuildingAI>();
        }


        public bool canBePlacedAt(Vector3Int tilePosition, int rotation) {
            return RTSTileMng.canTilablePlaceAt(tilePosition, BaseSize, rotation);
        }

        public void place(Vector3Int tilePosition, int tiledRotation) {
            TiledRotation = tiledRotation;
            TilePosition = tilePosition;

            _insideAgentsKeys = new Queue<AgentState>();

            blockTiles(TilePosition, TiledRotation);

            setTeam(TeamsMng.NO_TEAM);

            updateCurrentArea();
            subscribedToVisibleAreas();
            
            _onPlaced?.Invoke();
        }

        public Vector3Int getSize() {
            return BaseSize;
        }

        public virtual bool canEnterAgent(RTSAgent agent) {
            if(!EntityData.CanBeTeam) return false;
            if(TeamKey != -1 && TeamKey != agent.TeamKey) return false;

            return true;
        }

        public virtual bool enterAgent(RTSAgent agent) {

            if(!canEnterAgent(agent)) return false;
            
            //_insideAgentsKeys.Add(EntitiesCtlr.hideAgent(agent));
            _insideAgentsKeys.Enqueue(new AgentState(agent));
            EntitiesCtlr.deSpawnEntity(agent);

            if(_insideAgentsKeys.Count == 1) {
                //EntitiesCtlr.addEntity(this);
                setTeam(agent.TeamKey);
            }

            Debug.Log($"ENTER: {_insideAgentsKeys.Count}");


            return true;
        }

        void exitAgent() {
            AgentState state = _insideAgentsKeys.Dequeue();

            Vector3Int spawnPos = TilePosition + Vector3Int.right * 2;

            RTSAgent agent = EntitiesCtlr.spawnEntity(state.EntityID, spawnPos) as RTSAgent;
            
            agent.setState(TeamKey, spawnPos, state);
            agent.setDestination(RTSTileMng.getTile(spawnPos), true);

            Debug.Log($"EXIT: {_insideAgentsKeys.Count} -> {spawnPos} -> {agent.WorldPosition}");

            if(_insideAgentsKeys.Count == 0) {
                setTeam(TeamsMng.NO_TEAM);
            }
        }

        public virtual void onDoubleClick() {
            if(TeamKey != Player.RTSPlayerCtrl.PlayerTeam) return;

            exitAgent();
        }

        public void addOnPlaced(Action action) {
            _onPlaced += action;
        }

    }
}