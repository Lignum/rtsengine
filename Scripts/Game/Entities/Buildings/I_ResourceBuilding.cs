﻿using RTS.AI;
using RTS.Game;
using RTS.Game.Entities;

namespace RTS.Buildings {
    public interface I_ResourceBuilding {

        RTSEntity Entity { get; }

        int ResourceType { get; }
        int ResourcesCount { get; }

        int addResource(WorkerAI worker, int resourceMatType, int cuantity);
        int takeResource(int cuantity);

        bool IsEmpty { get; }
        bool IsFull { get; }
    }
}
