﻿using RTS.AI;
using RTS.Data.ResourceMaterials;
using RTS.Game;
using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Buildings {
    public class BuildingOrder : RTSBuilding, I_ResourceBuilding {
        
        float state;
        RTSBuilding _buildingRef;

        public ElementCost BuildingCost { get; private set; }

        public RTSEntity Entity => this;

        public int ResourceType => -1;

        public bool IsEmpty => !hasResources();

        public bool IsFull => hasAllResources();

        public bool IsBuilded {
            get {
                return state >= 1;
            }
        }

        public int ResourcesCount => throw new System.NotImplementedException();

        public int[] ResourceMaterials;
        int[] _reservedResourceMaterials;
        HashSet<WorkerAI> _reservedResourcesWorkers;

        protected override void Awake() {
            base.Awake();

            ResourceMaterials = new int[GameDataMng.MaterialResourcesCount];
            _reservedResourceMaterials = new int[GameDataMng.MaterialResourcesCount];

            setTeam(-1);
        }

        protected override void Start() {
            base.Start();

        }

        private void Update() {
            Body.localScale = new Vector3(1, state , 1);

            if(Input.GetKeyDown(KeyCode.K)) {
                emptyCurrentTiles();
            }

            if(state >= 1) {

                RTSBuilding created = EntitiesCtlr.spawnEntity(_buildingRef.UniqueKey) as RTSBuilding;
                //RTSBuilding created = Instantiate(_buildingRef);
                created.place(TilePosition, TiledRotation);

                GameDataMng.BuildingsOrderPool.despawn(this);
            }
        }

        public virtual void place(Vector3Int tilePosition, float rotation, RTSBuilding building) {

            setBuilding(building);

            Rotation = rotation;
            TilePosition = tilePosition;
            blockTiles(TilePosition, TiledRotation);

            state = .01f;
            Body.localScale = new Vector3(1, state, 1);

            createMiniMapProjection();
        }

        public void setBuilding(RTSBuilding building) {

            _buildingRef = building;

            BuildingCost = building.EntityData.BuildCost;//GameDataMng.getElementCost(_buildingRef.name);

            _reservedResourcesWorkers = new HashSet<WorkerAI>();

            for(int i = 0; i < ResourceMaterials.Length; i++) {
                ResourceMaterials[i] = 0;
            }

            for(int i = 0; i < _reservedResourceMaterials.Length; i++) {
                _reservedResourceMaterials[i] = 0;
            }

            EntityData = _buildingRef.EntityData;
            //BaseSize = _buildingRef.BaseSize;

            clearChildrens(Body);
            Transform created = createChilds(Body, _buildingRef.transform);

            created.localRotation = Quaternion.identity;
            created.localPosition = Vector3.zero;
        }

        public void build() {
            state += 1f / _buildingRef.EntityData.BuildTickTime;

            if(state > 1) state = 1;
        }

        void clearChildrens(Transform parent) {

            int t = 0;
            foreach(Transform child in parent) {
                Destroy(child.gameObject);
                t++;
                if(t > 10) break;
            }
        }

        Transform createChilds(Transform parent, Transform oParent) {


            Transform created = new GameObject().transform;
            created.SetParent(parent);
            created.localPosition = oParent.transform.localPosition;
            created.localRotation = oParent.transform.localRotation;

            if(oParent.TryGetComponent(out MeshFilter filter)) {                
                created.gameObject.AddComponent<MeshRenderer>().sharedMaterial = oParent.GetComponent<MeshRenderer>().sharedMaterial;
                created.gameObject.AddComponent<MeshFilter>().mesh = filter.sharedMesh;
            }

            foreach(Transform item in oParent) {
                createChilds(created, item);
            }

            return created;
        }

        bool hasAllResources() {

            for(int i = 0; i < ResourceMaterials.Length; i++) {
                if(ResourceMaterials[i] < BuildingCost.Costs[i]) return false;
            }

            return true;
        }
        bool hasResources() {

            for(int i = 0; i < ResourceMaterials.Length; i++) {
                if(ResourceMaterials[i] > 0) return true;
            }

            return false;
        }

        public int addResource(WorkerAI worker, int resourceMatType, int cuantity) {
            int rest = 0;
            if(ResourceMaterials[resourceMatType] + cuantity > BuildingCost.Costs[resourceMatType]) {
                rest = (ResourceMaterials[resourceMatType] + cuantity) - BuildingCost.Costs[resourceMatType];
                ResourceMaterials[resourceMatType] = BuildingCost.Costs[resourceMatType];
            } else {
                ResourceMaterials[resourceMatType] += cuantity;
            }


            _reservedResourceMaterials[resourceMatType] -= cuantity - rest;
            //Debug.Log($"{ResourceMaterials[resourceMatType]} + {_reservedResourceMaterials[resourceMatType]} <= { BuildingCost.Costs[resourceMatType]}");

            return rest;
        }

        public int takeResource(int cuantity) {
            return 0;
        }

        public int reserveResourceMaterial(WorkerAI worker, I_ResourceBuilding resourceBuilding) {


            int maxCuantity = worker.getMaxCarryReourceMaterial(resourceBuilding.ResourceType);
            if(resourceBuilding.ResourcesCount < maxCuantity) {
                maxCuantity = resourceBuilding.ResourcesCount;
            }

            return reserveResourceMaterial(worker, resourceBuilding.ResourceType, maxCuantity);

        }

        public int reserveResourceMaterial(WorkerAI worker, int resourceType, int cuantity) {

            if(_reservedResourcesWorkers.Contains(worker)) {
                return -1;
            }

            if(getTotalReservedResoruces(resourceType) + cuantity > BuildingCost.Costs[resourceType]) {
                cuantity = (getTotalReservedResoruces(resourceType) + cuantity) - BuildingCost.Costs[resourceType];
            }

            if(cuantity > 0) {
                _reservedResourceMaterials[resourceType] += cuantity;
                _reservedResourcesWorkers.Add(worker);
            }

            return cuantity;
        }

        public void removeReserve(WorkerAI worker) {
            _reservedResourcesWorkers.Remove(worker);
        }

        int getTotalReservedResoruces(int resourceMatType) {
            return ResourceMaterials[resourceMatType] + _reservedResourceMaterials[resourceMatType]; ;
        }

        public Dictionary<int, int> getPendingResources(WorkerAI worker) {
            Dictionary<int, int> pending = new Dictionary<int, int>(ResourceMaterials.Length);

            int totalMat;

            for(int i = 0; i < ResourceMaterials.Length; i++) {
                totalMat = ResourceMaterials[i];/* + _reservedResourceMaterials[item.Key]*/;

                if(totalMat < BuildingCost.Costs[i]) pending.Add(i, BuildingCost.Costs[i] - totalMat);
            }

            return pending;
        }

        public bool hasResourceReserved(WorkerAI worker) {
            if(worker.CurrentMateriaResourceType == -1) return false;

            return _reservedResourcesWorkers.Contains(worker);
        }

        public override bool enterAgent(RTSAgent agent) {
            return false;
        }

        protected override void OnDrawGizmos() {
            base.OnDrawGizmos();
            
            for(int i = 0; i < ResourceMaterials.Length; i++) {
                GameDataMng.drawMaterialResourcesGizmos(WorldPosition + Vector3.right * i * .5f, i, ResourceMaterials[i]);
            }

        }
    }
}
