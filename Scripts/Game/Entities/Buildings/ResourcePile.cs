﻿using RTS.AI;
using RTS.Game;
using RTS.Game.Entities;
using RTS.Game.Teams;
using UnityEngine;

namespace RTS.Buildings {
    public class ResourcePile : RTSTeamEntity, I_ResourceBuilding {

        public RTSEntity Entity { get { return this; } }

        [SerializeField]
        int _resourceType;
        public int ResourceType { get { return _resourceType; } }

        [SerializeField]
        int maxResources;

        public int ResourcesCount { get; private set; } = 0;

        public bool IsEmpty { get { return ResourcesCount <= 0; } }
        public bool IsFull { get { return ResourcesCount >= maxResources; } }

        protected override void Awake() {
            base.Awake();

            setTeam(TeamsMng.ALL_TEAMS);
        }

        public void place(Vector3Int tilePosition) {

            Rotation = 0;
            TilePosition = tilePosition;
            blockTiles(TilePosition, TiledRotation);

            ResourcesCount = 0;

            updateCurrentArea();
        }

        public int addResource(WorkerAI worker, int resourceMatType, int cuantity) {

            if(resourceMatType != _resourceType) {
                Debug.LogError($"Expected Resource{_resourceType} - Recived: {resourceMatType}");
                return cuantity;
            }

            int rest = 0;
            if(ResourcesCount + cuantity > maxResources) {
                rest = (ResourcesCount + cuantity) - maxResources;
                ResourcesCount = maxResources;
            } else {
                ResourcesCount += cuantity;
            }

            return rest;
        }

        public int takeResource(int cuantity) {

            if(ResourcesCount < cuantity) {
                cuantity = ResourcesCount;
            }

            ResourcesCount -= cuantity;
            return cuantity;
        }

        protected override void OnDrawGizmos() {
            base.OnDrawGizmos();

            Gizmos.color = Color.green;
            Gizmos.DrawRay((WorldPosition - RTSGameCtrl.FlatTileDes) + (RTSGameCtrl.TileSize * Vector3.right), Vector3.up * (ResourcesCount / (float)maxResources));

            GameDataMng.drawMaterialResourcesGizmos(WorldPosition, ResourceType, ResourcesCount);
        }
    }
}