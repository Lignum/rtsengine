﻿using RTS.Game;
using RTS.Game.Entities;
using RTS.MapAI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Buildings {

    public class RTSGhostBuilding : TiledObj {

        protected RTSBuilding _buildingRef;

        protected override void Awake() {
            EntityData = new EntityBaseData();

            base.Awake();
        }

        public void setPosition(Vector3Int position) {
            TilePosition = position;
        }

        public void clear() {

            if(Body.childCount > 0) {
                Destroy(Body.GetChild(0).gameObject);
            }
        }

        public void setBuilding(RTSBuilding building) {

            if(_buildingRef == building) return;

            clear();

            _buildingRef = building;

            EntityData = building.EntityData;
            //BaseSize = _buildingRef.BaseSize;

            Transform created = createChildGhosts(Body, _buildingRef.transform);
            created.localRotation = Quaternion.identity;
            created.localPosition = Vector3.zero;
        }
        
        public void setRotation(int rotation) {
            TiledRotation = rotation;
        }

        public void rotate(int dir) {
            TiledRotation += dir;
        }

        Transform createChildGhosts(Transform parent, Transform oParent) {

            Transform created = new GameObject().transform;
            created.SetParent(parent);
            created.localPosition = oParent.transform.localPosition;
            created.localRotation = oParent.transform.localRotation;

            if(oParent.TryGetComponent(out MeshFilter filter)) {
                created.gameObject.AddComponent<MeshRenderer>();
                created.gameObject.AddComponent<MeshFilter>().mesh = filter.sharedMesh;
            }

            foreach(Transform item in oParent) {
                createChildGhosts(created, item);
            }

            return created;
        }

        public void setMaterial(Material mat) {

            MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();

            foreach(var item in renderers) {
                item.material = mat;
            }

        }
    }
}