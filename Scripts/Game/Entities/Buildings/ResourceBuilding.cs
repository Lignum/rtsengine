﻿using RTS.AI;
using RTS.Game;
using RTS.Game.Entities;
using UnityEngine;

namespace RTS.Buildings {

    public class ResourceBuilding : RTSBuilding, I_ResourceBuilding {

        public RTSEntity Entity { get { return this; } }

        [SerializeField]
        int _resourceType;
        public int ResourceType { get { return _resourceType; } }

        [SerializeField]
        int maxResources;

        public int ResourcesCount { get; private set; } = 0;

        public bool IsEmpty { get { return ResourcesCount <= 0; } }
        public bool IsFull { get { return ResourcesCount >= maxResources; } }

        public int addResource(WorkerAI worker, int resourceMatType, int cuantity) {

            if(resourceMatType != _resourceType) {
                Debug.LogError($"Expected Resource{_resourceType} - Recived: {resourceMatType}");
                return cuantity;
            }

            int rest = 0;
            if(ResourcesCount + cuantity > maxResources) {
                rest = (ResourcesCount + cuantity) - maxResources;
                ResourcesCount = maxResources;
            } else {
                ResourcesCount += cuantity;
            }
            
            getTeam().onResourcesChange();

            return rest;
        }

        public int takeResource(int cuantity) {

            if(ResourcesCount < cuantity) {
                cuantity = ResourcesCount;
            }

            ResourcesCount -= cuantity;

            getTeam().onResourcesChange();
            return cuantity;
        }

        protected override void OnDrawGizmos() {
            base.OnDrawGizmos();
            
            if(Application.isPlaying)
                GameDataMng.drawMaterialResourcesGizmos(WorldPosition, ResourceType, ResourcesCount);

        }
    }
}