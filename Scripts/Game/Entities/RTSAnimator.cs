﻿using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RTS.Game {

    public class RTSAnimator  {

        //Animation _animation;
        MonoBehaviour _reference;
        
        //public string CurrentAnimation { get; private set; }

        Coroutine _lookCoroutine;
        //Coroutine _stopCoroutine;

        public RTSAnimator(MonoBehaviour obj) {
            _reference = obj;
            //_animation = obj.gameObject.AddComponent<Animation>();
            //_animation.enabled = false;
        }

        /*public void addClip(AnimationClip clip, string name) {
            if(clip != null) {
                _animation.AddClip(clip, name);
            }
        }

        public void play(string name) {
            if(CurrentAnimation != name) {
                _animation.Play(name);

                CurrentAnimation = name;
            }
        }*/

        public void playAgentLookTo(RTSAgent agent, RTSEntity target, float speed) {
            if(_lookCoroutine == null) {
                _lookCoroutine = _reference.StartCoroutine(lookAtAnimation(agent, target.WorldPosition - agent.WorldPosition, speed));
            }
        }
        public void stopLooking() {
            if(_lookCoroutine != null) {
                _reference.StopCoroutine(_lookCoroutine);
                _lookCoroutine = null;
            }
        }

        IEnumerator lookAtAnimation(RTSAgent agent, Vector3 direction, float speed) {

            float e = 0;
            Vector3 dir;
            

            while (e<= 1) {

                dir = Vector3.Lerp(agent.Forward, direction, e);
                agent.lookTo(dir);

                e += speed * Time.deltaTime;

                yield return new WaitForEndOfFrame();
            }

            agent.lookTo(direction);
            _lookCoroutine = null;

        }

        /*public void startNextAnimation(string next) {

            if(CurrentAnimation != next) {
                if(CurrentAnimation != null) {
                    //_animation.Stop(CurrentAnimation);
                    _animation[CurrentAnimation].clip.SampleAnimation(_reference.gameObject, 0);
                }

                _animation.Play(next);

                CurrentAnimation = next;

                if(CurrentAnimation == "IDLE_ANI") {
                    Debug.Log("--------------------IDLE--------------------");
                }
            }
        }

        public void startNextAnimationQueued(string next) {

            if(CurrentAnimation != next) {

                if(_stopCoroutine != null) {
                    _reference.StopCoroutine(_stopCoroutine);
                }
                _stopCoroutine = _reference.StartCoroutine(stopLoopAnimation(CurrentAnimation, next));
                if(next == "IDLE_ANI") {
                    Debug.Log("--------------------IDLE--------------------");
                }

            }
        }

        IEnumerator stopLoopAnimation(string stopAni, string nextAni) {

            if(stopAni != null) {

                if(_animation.IsPlaying(stopAni)) {
                    float time = _animation[stopAni].length - _animation[stopAni].time % _animation[stopAni].length;
                    yield return new WaitForSeconds(time);
                }
                
                _animation[stopAni].clip.SampleAnimation(_reference.gameObject, 0);
                
                _animation.Play(nextAni);

            }

            CurrentAnimation = nextAni;
        }*/

    }
}