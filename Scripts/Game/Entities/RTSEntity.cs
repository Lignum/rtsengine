﻿using RTS.MapAI;
using Shared.Collections.Pools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Game.Entities {
    
    public class RTSEntity : TiledObj, I_UniqueKey {

        public int UniqueKey { get { return EntityData.UniqueKey; } set{ EntityData.setKey(value); } }
        public void resetKey() => EntityData.resetKey();

        [HideInInspector]
        public bool _isActive = true;
        public bool IsActive {
            get { return _isActive; }
            set {
                _isActive = value;
                emptyCurrentTiles();
            }
        }

        //[SerializeField]
        //string _entityName;
        public string EntityName { get { return EntityData.Name; } }

        //[SerializeField, Min(1)]
        //int maxLife = 100;
        public float Life { get; protected set; }

        Func<float, float> _onTakeDamage;
        
        protected override void Start() {
            base.Start();
            Life = EntityData.MaxLife;
        }

        public float reciveDamage(float damage) {
            float damageRecived = damage;
            if(_onTakeDamage != null) {
                damageRecived = _onTakeDamage(damage);
                Life -= _onTakeDamage(damage);
            } else {
                Life -= damage;
            }

            return damageRecived;
        }

        public void forceChangeLife(float life) {
            Life += life;
        }

        public void addOnTakeDamage(Func<float, float> action) {
            _onTakeDamage += action;
        }

        protected virtual void OnDrawGizmos() {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(WorldPosition - RTSGameCtrl.FlatTileDes, Vector3.up * (Life / EntityData.MaxLife) * 3);
        }

    }
}
