﻿using RTS.Game.Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RTS.Game {
    public class ResourceMaterialEntity : RTSEntity {

        [SerializeField]
        int _resourceType;
        public int ResourceMaterialType { get { return _resourceType; } }

        [SerializeField]
        float lifePerResource = 1;

        float _lifeSinceLastResource = 0;

        protected override void Start() {
            base.Start();

            blockTiles(TilePosition, 0);
        }

        public int gatherResource(float damage, int maxResources) {

            if(maxResources <= 0) return 0;

            float totalDamage = reciveDamage(damage);

            if(lifePerResource <= 0) return 0;
            
            _lifeSinceLastResource += totalDamage;
            if(_lifeSinceLastResource >= lifePerResource) {

                int resources = Mathf.FloorToInt(_lifeSinceLastResource / lifePerResource);
                _lifeSinceLastResource -= lifePerResource * resources;

                if(resources > maxResources) {
                    addResources(resources - maxResources);
                    resources = maxResources;
                }
                

                return resources;
            }

            return 0;
        }

        public void addResources(int cuantity) {
            forceChangeLife(cuantity / lifePerResource);
        }

    }
}