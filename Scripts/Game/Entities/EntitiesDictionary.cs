﻿using RTS.Game.Entities.Agents;
using RTS.Game.Teams;
using Shared.Collections.Pools;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RTS.Game.Entities {

   // [CreateAssetMenu(fileName = "EntitiesDictionary", menuName = "RTS/Entities/EntitiesDictionary", order = 1)]
    [System.Serializable]
    public class EntitiesDictionary /*: ScriptableObject*/ {

        [SerializeField]
        RTSEntity[] _entityList;

        public SimpleGroupMonoBehaviourPool<RTSEntity> createEtitiesPool() {

            SimpleGroupMonoBehaviourPool<RTSEntity> pool = new SimpleGroupMonoBehaviourPool<RTSEntity>(_entityList);

            return pool;
        }

        public Material[] getTeamMaterials() {

            RTSTeamEntity teamEntity;
            Renderer[] teamRenderers;
            HashSet<Material> teamMaterials = new HashSet<Material>();

            foreach(var entity in _entityList) {

                teamEntity = entity as RTSTeamEntity;
                if(teamEntity) {

                    teamRenderers = teamEntity.calculateTeamRenderers();

                    foreach(var renderer in teamRenderers) {
                        teamMaterials.Add(renderer.sharedMaterial);
                    }
                }
            }

            return teamMaterials.ToArray();
        }

    }
}