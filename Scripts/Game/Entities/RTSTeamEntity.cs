﻿using RTS.Buildings;
using RTS.Data.Menus;
using RTS.Game.Entities.Agents;
using RTS.Game.Teams;
using RTS.Jobs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RTS.Game.Entities {

    public class RTSTeamEntity : RTSEntity {

        [SerializeField, Min(-2)]
        int _teamKey = -1;
        public int TeamKey { get { return _teamKey; } protected set { _teamKey = value; } }

        public float VisibleDistance { get { return EntityData.VisibleDistance; } }
        Vector3 _lastVisibilityCheckPos = Vector3.zero;
        
        HashSet<RTSTeamEntity> _visibleEntities;

        Action<float> _onAIUpdate;
        Action<RTSTeamEntity[]> _onEntitySpoted, _onEntityLost;

        public MeshRenderer MiniMapProjection { get; private set; }

        public Renderer[] TeamRenderers { get; private set; }
        //public HashSet<Vector3Int> VisibleAreas { get; private set; }

        public Vector2 ScreenPos {
            get {
                return MouseCtrl.worldToScreenPos(transform.position);
            }
        }

        protected override void Start() {
            TeamRenderers = calculateTeamRenderers();

            base.Start();

            _visibleEntities = new HashSet<RTSTeamEntity>();
            addOnEntitiesSpoted(checkSpotedResourcePile);
            addOnEntitiesLost(checkLostResourcePile);

            createMiniMapProjection();
        }

        public Renderer[] calculateTeamRenderers() {
            Renderer[] renderers = GetComponentsInChildren<Renderer>();

            HashSet<Renderer> temaRender = new HashSet<Renderer>();

            foreach(var item in renderers) {
                if(item.sharedMaterial.HasProperty(Team.TEAM_COLOR_PROPERTY)) {
                    temaRender.Add(item);
                }
            }

            return temaRender.ToArray();
        }

        protected void createMiniMapProjection() {
            MiniMapProjection = TeamsMng.createMiniMapProjectionFor(this);
        }

        public virtual void aiUpdate(float seconds) {
            _onAIUpdate?.Invoke(seconds);
        }

        protected void updateFog() {
            if(TeamKey == Player.RTSPlayerCtrl.PlayerTeam)
                FogOfWarMng.getInstance().addUpdatedArea(this, LogicAreasMng.getAreasPositionsAtDistance(WorldPosition, VisibleDistance));
        }

        protected void setTeam(int team) {

            //Debug.Log($"-----------SET TEAM: {Team} -> {team} --------");
            int preTeam = TeamKey;
            TeamKey = team;

            EntitiesCtlr.changeEntityTeam(preTeam, this);

            updateFog();
        }


        /*
         * Checks visible agents when moving
         */
        protected void checkVisivility() {

            if(Vector3.Distance(WorldPosition, _lastVisibilityCheckPos) >= RTSGameCtrl.TileSize) {

                updateFog();

                HashSet<RTSTeamEntity> visible = LogicAreasMng.getEntitiesAtDistance(WorldPosition, VisibleDistance);
                List<RTSTeamEntity> spoted = new List<RTSTeamEntity>();
                List<RTSTeamEntity> lost = new List<RTSTeamEntity>();

                foreach(var item in visible) {
                    if(!_visibleEntities.Contains(item)) {
                        spoted.Add(item);
                    }
                }
                foreach(var item in _visibleEntities) {
                    if(!visible.Contains(item)) {
                        lost.Add(item);
                    }
                }

                _visibleEntities = visible;
                _lastVisibilityCheckPos = WorldPosition;

                if(spoted.Count > 0) {
                    _onEntitySpoted?.Invoke(spoted.ToArray());
                }
                if(lost.Count > 0) {
                    _onEntityLost?.Invoke(lost.ToArray());
                }

            }
        }

        /*
         *Call when another Agent enter Visible range 
         */
        public void addVisibleEntity(RTSAgent agent) {
            if(!_visibleEntities.Contains(agent)) {
                _visibleEntities.Add(agent);
                _onEntitySpoted?.Invoke(new RTSAgent[] { agent });
            }
        }
        public void removeVisibleEntity(RTSAgent agent) {
            if(_visibleEntities.Contains(agent)) {
                _visibleEntities.Remove(agent);
                _onEntityLost?.Invoke(new RTSAgent[] { agent });
            }
        }

        protected void updateCurrentArea() {
            LogicAreasMng.updateEntityArea(this);
        }

        protected void subscribedToVisibleAreas() {
            
            updateFog();

            List<LogicArea> areas = LogicAreasMng.getAreasAtDistance(WorldPosition, VisibleDistance);
            foreach(var item in areas) {

                if(!SubscribedAreas.Contains(item)) {
                    item.subscribeAgent(this);
                }
            }

            foreach(var item in SubscribedAreas) {
                if(!areas.Contains(item)) {
                    item.unSubscribedAgent(this);
                }
            }

            SubscribedAreas = areas;
        }

        protected void clearSubscribedAreas() {
            foreach(var area in SubscribedAreas) {
                area.unSubscribedAgent(this);
            }
            SubscribedAreas.Clear();
        }

        public bool canSeeAgent(RTSAgent agent) {
            return Vector3.Distance(WorldPosition, agent.WorldPosition) <= VisibleDistance;
        }

        public Team getTeam() {
            return TeamsMng.getTeam(TeamKey);
        }


        public void addOnUpdateAI(Action<float> action) {
            _onAIUpdate += action;
        }
        public void addOnEntitiesSpoted(Action<RTSTeamEntity[]> action) {
            _onEntitySpoted += action;
        }
        public void addOnEntitiesLost(Action<RTSTeamEntity[]> action) {
            _onEntityLost += action;
        }

        public void checkSpotedResourcePile(RTSTeamEntity[] entities) {
            foreach(var item in entities) {
                if(item is ResourcePile) getTeam().addVisibleResourcePile(item as ResourcePile, this);
            }
        }
        public void checkLostResourcePile(RTSTeamEntity[] entities) {
            foreach(var item in entities) {
                if(item is ResourcePile) getTeam().removeVisibleResourcePile(item as ResourcePile, this);
            }
        }


        public MenuData getMenuData() {
            return EntityData.MenuData;
        }

        protected virtual void OnDrawGizmosSelected() {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(WorldPosition, VisibleDistance);

            Gizmos.DrawCube(WorldPosition, Vector3.one * .5f);
            
            if(_visibleEntities != null) {
                Gizmos.color = Color.blue;

                foreach(var item in _visibleEntities) {
                    Gizmos.DrawLine(WorldPosition, item.WorldPosition);
                }
            }

            /*Gizmos.color = Color.red;
            Gizmos.DrawRay(TilePosition, Vector3.up * 200);

            Gizmos.color = Color.green;
            Gizmos.DrawRay(RTSGameCtrl.tileToWorldPosition(TilePosition), Vector3.up * 200);

            Gizmos.color = Color.yellow;
            Gizmos.DrawCube(WorldPosition, Vector3.one);*/
        }

        
    }
}