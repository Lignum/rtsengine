﻿using RTS.AI;
using RTS.Game;
using RTS.Jobs;
using RTS.MapAI;
using RTS.MapAI.PathFinding;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace RTS.Game.Entities.Agents {

    public class RTSAgent : TileMapAgent {

        public Vector3 Forward {
            get { return Body.forward; }
        }

        public bool AI_StartMoving { get; private set; } = false;
        public bool AI_EndMoving { get; private set; } = false;

        public bool StartMoving { get; private set; } = false;
        public bool EndMoving { get; private set; } = false;

        public bool IsMoving { get; private set; } = false;
        public bool HasMasterOrder { get; private set; }

        public MapTile RequestedDestination { get; private set; }
        MapTile _nextDestination;
        bool _nextDestionationIsMaster;

        AgentsFormation _singleFormation;

        //EVENTS
        Action _onAgentUpdate;
        A_AgentAI _activeAI;

        protected override void Awake() {
            base.Awake();

            _singleFormation = new AgentsFormation(this);

            _activeAI = GetComponent<A_AgentAI>();
        }


        protected override void Start() {

            base.Start();
            LogicAreasMng.updateEntityArea(this);

            _onWalkPathStart += onWalkPathStart;
            _onWalkPathEnd += onWalkPathEnd;
            _onPathReceived += onPathReceived;

            setDestination(RTSTileMng.getTile(TilePosition), false);

            //EntitiesCtlr.addEntity(this);
            setTeam(TeamKey);
            //EntitiesCtlr.addEntity(this);
        }

        public void setState(int team, Vector3Int tilePos, AgentState state) {
            setTeam(team);
            warp(RTSGameCtrl.tileToWorldPosition(tilePos));

            Life = state.CurrentLife;
        }

        public override void aiUpdate(float seconds) {

            if(AI_StartMoving) {
                onFollowPathStart_AI();
            }

            if(IsMoving) {
                updateArea();
                checkVisivility();
            }

            if(AI_EndMoving) {
                onFollowPathEnd_AI();
            }

            base.aiUpdate(seconds);
            
            AI_StartMoving = false;
            AI_EndMoving = false;
        }

        public void moveUpdate() { 

            if(_nextDestination != null) {
                goToDestination();
            }

            /*if(IsMoving) {
                //WorldPosition = transform.position;
                lookTo(Direction);

                //checkPathEnd();
            }*/

            _onAgentUpdate?.Invoke();
            
            StartMoving = false;
            EndMoving = false;
        }

        void updateArea() {

            CurrentArea.setForUpdate(this);
            updateCurrentArea();
        }

        public void lookAt(RTSTeamEntity agent) {
            if(agent != null) lookTo(agent.WorldPosition - WorldPosition);
        }

        void onWalkPathStart() {
            IsMoving = true;
            AI_StartMoving = true;
            StartMoving = true;
        }
        void onWalkPathEnd() {
            AI_EndMoving = true;
            EndMoving = true;

            IsMoving = false;
        }

        void onFollowPathStart_AI() {
            clearSubscribedAreas();
        }

        void onFollowPathEnd_AI() {

            HasMasterOrder = false;

            subscribedToVisibleAreas();

            updateArea();
            checkVisivility();
        }

        public void setTarget(RTSEntity target) {
            _onRecibeMasterOrder?.Invoke(new AI.MasterOrder(target));
        }

        public void setDestination(MapTile destinationTile, bool isMasterOrder) {

            if(HasMasterOrder && !isMasterOrder) return;

            RequestedDestination = destinationTile;

            _nextDestination = destinationTile;
            _nextDestionationIsMaster = isMasterOrder;
            HasMasterOrder = isMasterOrder;

        }

        void goToDestination() {

            if(HasMasterOrder && !_nextDestionationIsMaster) {
                HasMasterOrder = false;
                _nextDestination = null;
                return;
            }
            
            _singleFormation.setAgentsDestinations(_nextDestination, _nextDestionationIsMaster);
            _nextDestination = null;

        }

        void onPathReceived() {
            RequestedDestination = null;
        }

        
        public void addOnAgentUpdate(Action action) {
            _onAgentUpdate += action;
        }


        public void setJob(Job job) {
            _activeAI?.setJob(job);
        }
        public Job getJob() {
            return _activeAI?.getJob();
        }
        public A_AgentAI getAI() {
            return _activeAI;
        }




    }
}
