﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Game.Entities.Agents {
    public struct AgentState {

        public int EntityID { get; private set; }
        public float CurrentLife { get; private set; }

        public AgentState(RTSAgent agent) {
            EntityID = agent.UniqueKey;
            CurrentLife = agent.Life;
        }

    }
}