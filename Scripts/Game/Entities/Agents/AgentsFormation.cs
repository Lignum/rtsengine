﻿using RTS.Game;
using RTS.MapAI;
using RTS.MapAI.PathFinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace RTS.Game.Entities.Agents {
    public class AgentsFormation {

        static NavMeshPath _auxPath;

        public RectInt FormationArea { get; private set; }

        public Dictionary<RTSAgent, Vector3Int> _basePositions;
        public HashSet<RTSAgent> _agents;

        int _totalColumns;

        int _lastCol = -1;
        int _lastRow = 0;

        public Vector3Int FormationTargetPos {
            get; private set;
        }

        public AgentsFormation(RTSAgent agent) : this(new HashSet<RTSAgent>() { agent }){}
        public AgentsFormation(HashSet<RTSAgent> agents) {

            _agents = new HashSet<RTSAgent>(agents);

            _basePositions = new Dictionary<RTSAgent, Vector3Int>(_agents.Count);
            foreach(var item in _agents) {
                _basePositions.Add(item, getNextPosition());
            }

            createFormationArea();
        }

        void createFormationArea() {
            Vector3Int min = Vector3Int.one * 1000;
            Vector3Int max = Vector3Int.zero;

            foreach(var item in _basePositions) {
                min = Vector3Int.Min(min, item.Value);
                max = Vector3Int.Max(max, item.Value);
            }
            
            FormationArea = new RectInt(min.toVector2IntZ(), (max - min).toVector2IntZ());
        }

        public void setAgentsDestinations(MapTile targetPos, bool isMasterOrder) {
            setAgentsDestinations(targetPos.TilePosition, isMasterOrder);
        }
        public void setAgentsDestinations(Vector3Int targetPos, bool isMasterOrder) {

            FormationTargetPos = targetPos;

            _totalColumns = 0;// Mathf.FloorToInt(Mathf.Sqrt(_basePositions.Count));

            _lastCol = -1;
            _lastRow = 0;

            setAgentsFinalDestinations(targetPos, isMasterOrder);
            //return destinations;
        }
        
        Vector3Int getNextPosition() {
            
            if(_lastRow < _totalColumns) {
                _lastRow++;

                if(_lastRow == _totalColumns) {
                    if(_totalColumns % 2 == 1) {
                        _lastCol = -(_lastCol - 1);
                    }
                }
            } else if(_lastRow == _totalColumns) {
                _lastCol++;                
            }

            if(_lastCol > (_totalColumns+1)/2f) {
                if(_totalColumns %2 == 1) {
                    _lastCol = -(_lastCol-1);
                }
                _totalColumns++;
                _lastRow = 0;
            }
            return new Vector3Int(_lastCol, 0, _lastRow);
        }

        Vector3 calculateEndDirection(Vector3Int targetPos) {
            Vector3 startPos = Vector3.zero;

            foreach(var agent in _agents) {
                startPos += agent.WorldPosition;
            }

            startPos /= _agents.Count;

            Vector3 dir = (RTSGameCtrl.tileToWorldPosition(targetPos) - startPos).normalized;


            return dir;//TiledObj.getTiledAngleDirection(dir);
        }

        void setAgentsFinalDestinations(Vector3Int targetPos, bool isMasterOrder) {

            Dictionary<RTSAgent, MapTile> finalPositions = new Dictionary<RTSAgent, MapTile>();

            Vector3 endDirection = calculateEndDirection(targetPos);
            int endRotation = TiledObj.getTiledAngleDirection(endDirection);
            endDirection = TiledObj.rotationToVector(endRotation);

           MapTile tile;

            foreach(var item in _basePositions) {
                tile = RTSTileMng.getClosestTileToTarget(targetPos + item.Value, item.Key, 5, (int)PathTileState.Empty);
                finalPositions.Add(item.Key, tile);
            }
            
            foreach(var item in _agents) {
                item.setDestination(targetPos, finalPositions[item], endDirection, isMasterOrder);
            }

            /*_auxPath = new NavMeshPath();
            RTSAgent currentAgent;

            Vector3 destination;
            Vector3 lastPathPoint;
            Vector3 currentPos;

            int totalTries = 0;
            bool found;

            NavMeshPath path = null;

            while(restAgents.Count > 0) {
                currentAgent = restAgents.Dequeue();
                destination = finalPositions[currentAgent];

                currentPos = currentAgent.transform.position;

                if(NavMesh.SamplePosition(currentPos, out NavMeshHit startHit, 10, NavMesh.AllAreas)) {
                    currentPos = startHit.position;
                }

                lastPathPoint = -Vector3.one;

                if(NavMesh.SamplePosition(destination, out NavMeshHit hit, 10, NavMesh.AllAreas)) {
                    lastPathPoint = hit.position;
                    destination.y = lastPathPoint.y;
                }
                found = false;

                if(Vector3.Distance(lastPathPoint, destination) < .1f) {

                    path = new NavMeshPath();
                    NavMesh.CalculatePath(currentPos, lastPathPoint, NavMesh.AllAreas, path);
                    
                    if(path.corners.Length > 0 && Vector3.Distance(path.corners[path.corners.Length - 1], destination) < .1f) {
                        finalPositions[currentAgent] = lastPathPoint;
                        found = true;
                    }
                }

                if(found) {
                    
                    //currentAgent.setPath(path, isMasterOrder);
                } else {
                    //finalPositions[currentAgent] = RTSTileMng.getClosestEmptyTilePos(RTSGameCtrl.worldToTilePosition(targetPos) + getNextPosition(), currentAgent); //getNextPosition();
                    
                    restAgents.Enqueue(currentAgent);
                    
                    totalTries++;
                }

                if(totalTries == 10000) {
                    Debug.LogError("TOTAL " + totalTries);
                    break;
                }
            }

            //return finalPositions;*/
        }


        public static TestPath samplePositions(Vector3 startPos, Vector3 targetPos) {
            if(NavMesh.SamplePosition(targetPos, out NavMeshHit startHit, 10, NavMesh.AllAreas)) {

                NavMeshPath path = new NavMeshPath();

                NavMesh.CalculatePath(startPos, targetPos, NavMesh.AllAreas, path);


                HashSet<Vector3Int> positions = null;// = RTSTileMng.getEmptyTiles(RTSGameCtrl.worldToTilePosition(path.corners[path.corners.Length - 1]), null, 10);

                return new TestPath(path, positions);
            }

            return new TestPath();
        }


        public void getFormationIn(Vector3Int tilePosition, System.Action<Dictionary<RTSAgent, Vector3Int>> endAction) {
            CoroutineManager.getInstace().StartCoroutine(calculateFormationIn(tilePosition, endAction));
        }

         IEnumerator calculateFormationIn(Vector3Int tilePosition, System.Action<Dictionary<RTSAgent, Vector3Int>> endAction) {
            Vector3Int pos = Vector3Int.zero;

            Dictionary<RTSAgent, Vector3Int> final = new Dictionary<RTSAgent, Vector3Int>();
            HashSet<Vector3Int> reserved = new HashSet<Vector3Int>();

            HashSet<RTSAgent> pending = new HashSet<RTSAgent>();

            HashSet<TiledObj> requesters = new HashSet<TiledObj>(_agents);

            Vector3Int tileP;

            RectInt area = FormationArea;

            NavMeshPath path = new NavMeshPath();

            HashSet<MapTile> agentTiles;
            bool isValidTile;

            float maxSize = Mathf.Max(FormationArea.size.x, FormationArea.size.y);
            bool found;
            
            foreach(var item in _basePositions) {
                tileP = item.Value + tilePosition;
                found = false;

                isValidTile = true;
                /*if(RTSTileMng.tryGetTiles(tileP, item.Key.Size, out agentTiles)) {

                    foreach(var t in agentTiles) {
                        if(t.State != NavMeshTileState.Empty && !requesters.Contains(t.ObjReference)) {
                            isValidTile = false;
                        }
                    }

                    if(isValidTile) {

                        yield return new WaitForEndOfFrame();
                        if(NavMesh.CalculatePath(RTSGameCtrl.tileToWorldPosition(tilePosition), RTSGameCtrl.tileToWorldPosition(tileP), NavMesh.AllAreas, path)) {

                            if(isValidPath(path, RTSGameCtrl.tileToWorldPosition(tileP))) {
                                found = true;
                                final.Add(item.Key, tileP);

                                //foreach(var oos in agentTiles) {
                                    //reserved.Add(oos.LastPosition);
                                //}

                            }
                        }
                        // }
                        //}
                    }
                }*/

                if(!found) {
                    pending.Add(item.Key);
                }
            }
            //return tiles;

            Debug.Log($"F:{final.Count} p: {pending.Count}");
            foreach(var item in final) {
                Debug.DrawLine(item.Key.WorldPosition + Vector3.up, item.Value+RTSGameCtrl.TileDes, Color.yellow);
            }

            if(pending.Count > 0) {

                HashSet<Vector3Int> possible = new HashSet<Vector3Int>();
                HashSet<Vector3Int> used;
                Vector3Int foundPos;

                HashSet<TiledObj> p = new HashSet<TiledObj>(pending);

                //possible = RTSTileMng.getEmptyTilesFor(tilePosition, p, 10);

                
                foreach(var reser in reserved) {
                    possible.Remove(reser);
                }
                
                foreach(var item in pending) {

                    tileP = _basePositions[item] + tilePosition;

                    yield return new WaitForEndOfFrame();
                    foundPos = findBestPositionFor(tilePosition, item, possible, out used);
                    
                    final.Add(item, foundPos);


                    foreach(var use in used) {
                        possible.Remove(use);
                    }

                }

            }

            endAction(final);
            //return final;
        }

        Vector3Int findBestPositionFor(Vector3Int tartgetPos, RTSAgent agent, HashSet<Vector3Int> possible, out HashSet<Vector3Int> used) {
            Vector3Int size = agent.Size;
            Vector3Int center = TiledObj.calculateMidSize(size);

            HashSet<Vector3Int> valid = new HashSet<Vector3Int>();

            Vector3Int pos = Vector3Int.zero;

            bool validPos;
            foreach(var item in possible) {

                validPos = true;

                for(pos.x = 0; pos.x < size.x; pos.x++) {
                    //for(pos.y = 0; pos.y < size.y; pos.y++) {
                        for(pos.z = 0; pos.z < size.z; pos.z++) {

                            if(!possible.Contains(pos + item - center)) {
                                validPos = false;
                                break;
                            }
                            //tiles.Add(pos + TilePosition - center);
                        }
                        if(!validPos) break;
                    //}
                    //if(!validPos) break;
                }

                if(validPos) {
                    valid.Add(item);
                }
            }

            

            Vector3Int best = Vector3Int.zero;

            NavMeshPath path = new NavMeshPath();
            used = new HashSet<Vector3Int>();

            float minDistance = 1000;
            float distance;

            foreach(var item in valid) {

                if(NavMesh.CalculatePath(RTSGameCtrl.tileToWorldPosition(tartgetPos), RTSGameCtrl.tileToWorldPosition(item), NavMesh.AllAreas, path)) {
                    
                    if(isValidPath(path, RTSGameCtrl.tileToWorldPosition(item))) {
                        distance = getPathDistance(path);
                        if(distance < minDistance) {
                            minDistance = distance;
                            best = item;
                        }
                    }
                }

            }

            for(pos.x = 0; pos.x < size.x; pos.x++) {
                for(pos.y = 0; pos.y < size.y; pos.y++) {
                    for(pos.z = 0; pos.z < size.z; pos.z++) {
                        used.Add(pos + best - center);
                    }
                }

            }


            return best;

        }



        static bool isValidPath(NavMeshPath path, Vector3 targetPos) {
            return Vector3.Distance(path.corners[path.corners.Length - 1], targetPos) < RTSGameCtrl.TileSize;
        }
        static float getPathDistance(NavMeshPath path) {
            float l = 0;
            for(int i = 0; i < path.corners.Length-1; i++) {
                l += Vector3.Distance(path.corners[i], path.corners[i + 1]);
            }
            return l;
        }

        public struct TestPath {
            public NavMeshPath Path;
            public HashSet<Vector3Int> Positions;

            public TestPath(NavMeshPath path, HashSet<Vector3Int> positions) {
                Path = path;
                Positions = positions;
            }
        }
    }
}