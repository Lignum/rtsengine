﻿using RTS.Game;
using RTS.MapAI.PathFinding;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.AI;

namespace RTS.MapAI {
    public static class RTSTileMng {

        public static bool IsMapLoaded {
            get { return LoadedMap != null && LoadedMap.isFinish; }
        }

        public static Map LoadedMap { get; private set; }

        public static void loadMap(Map map) {
            LoadedMap = map;
        }

        static TileStateMask blockPlaceMask = PathTileState.Ocuppied | PathTileState.Blocked;


        public static MapTile getTile(Vector3 position) {
            return getTile(RTSGameCtrl.worldToTilePosition(position));
        }
        public static MapTile getTile(Vector3Int tilePosition) {            
            return LoadedMap.getTile(tilePosition);
        }
        public static bool tryGetTile(Vector3Int tilePosition, out MapTile tile) {
            return LoadedMap.tryGetTile(tilePosition, out tile);
        }

        public static T getTileObjAt<T>(Vector3Int tilePosition) where T : TiledObj {
            return LoadedMap.getTileObjAt<T>(tilePosition);
        }

        public static bool canTilablePlaceAt(Vector3Int position, Vector3Int size, int tiledRotation) {

            HashSet<Vector3Int> tiles = TiledObj.calculateOcuppationTiles(size, tiledRotation); //obj.getOcuppiedTiles();
            MapTile tile;

            foreach(var item in tiles) {
                tile = getTile(position + item);

                if(tile != null && (blockPlaceMask.Value & (int)tile.State) > 0) {
                    return false;
                }
            }

            return true;
        }

        public static MapTile findClosestTile(Vector3Int targetPos, float maxDistance) {
                        
            MapTile tile;
            
            if(!LoadedMap.tryGetTile(targetPos, out tile)) {

                tile = getClosestTileToTarget(targetPos, null, maxDistance, TileStateMask.Any);
            }
            
            return tile;
        }

        public static MapTile getClosestEmptyTileToRequester(Vector3Int targetPos, TiledObj requester, float distance) {

            HashSet<MapTile> tiles = LoadedMap.getTile(targetPos).getExpansion(Mathf.RoundToInt(distance));
            return getClosestEmptyTileToRequester(targetPos, tiles, requester, distance);
        }

        public static MapTile getClosestEmptyTileToRequester(Vector3Int targetPos, HashSet<MapTile> tiles, TiledObj requester, float distance) {
            MapTile closests = null;
            float minDist = Vector3Int.Distance(requester.TilePosition, targetPos) * 2;

            float dist;

            foreach(var t in tiles) {

                if(Vector3Int.Distance(targetPos, t.TilePosition) > distance) continue;

                dist = Vector3Int.Distance(requester.TilePosition, t.TilePosition);

                if(dist < minDist) {
                    minDist = dist;
                    closests = t;
                }
            }

            return closests;
        }

        public static MapTile getClosestTileToRequester(TiledObj targetObj, TiledObj requester, float distance, int stateMask) {

            HashSet<MapTile> tiles = getTilesArround(targetObj, requester, distance, stateMask);
            
            MapTile closests = null;
            float minDist = requester.distanceTo(targetObj.nextTilePosition) * 2;

            float dist;

            foreach(var t in tiles) {

                dist = Vector3Int.Distance(requester.TilePosition, t.TilePosition);

                if(dist < minDist) {
                    minDist = dist;
                    closests = t;
                }
            }

            return closests;
        }

        public static MapTile getClosestTileToTarget(Vector3Int targetPos, TiledObj requester, float maxDistance, int stateMask) {

            HashSet<MapTile> empty = LoadedMap.getTile(targetPos).getExpansionTillState(RTSGameCtrl.worldToTileDistance(maxDistance), stateMask, requester);

            float minDis = int.MaxValue;
            float dis;

            MapTile closests = null;

            foreach(var item in empty) {

                dis = Vector3.Distance(item.WorldPosition, requester.WorldPosition);

                if(dis < minDis) {
                    minDis = dis;
                    closests = item;
                }
            }

            return closests;
        }

        public static HashSet<MapTile> getEmptyTilesArround(Vector3Int tilePos, TiledObj requester, float maxDistance) {
            Vector3Int pos = Vector3Int.zero;
            Vector3Int tileP;

            int iDistance = RTSGameCtrl.worldToTileDistance(maxDistance);

            HashSet<MapTile> tiles = new HashSet<MapTile>();

            for(pos.x = -iDistance; pos.x <= iDistance; pos.x++) {
                for(pos.z = -iDistance; pos.z <= iDistance; pos.z++) {
                    tileP = pos + tilePos;

                    if(Vector3Int.Distance(tilePos, tileP) <= iDistance) {

                        if(LoadedMap.tryGetTile(tileP, out MapTile t)) {
                            if(t.State == PathTileState.Empty || t.OcuppiedObj == requester) {
                                tiles.Add(t);
                            }
                        }

                    }
                }
            }

            return tiles;
        }

        public static HashSet<MapTile> getTilesArround(TiledObj tileObjTarget, TiledObj requester, float distance, int stateMask) {
            Vector3Int pos = Vector3Int.zero;
            Vector3Int tileP;

            int xDistance = Mathf.CeilToInt(distance) + Mathf.Abs(tileObjTarget.Size.x) - 1;
            int zDistance = Mathf.CeilToInt(distance) + Mathf.Abs(tileObjTarget.Size.z) - 1;

            HashSet<MapTile> tiles = new HashSet<MapTile>();

            for(pos.x = -xDistance; pos.x <= xDistance; pos.x++) {
                for(pos.z = -zDistance; pos.z <= zDistance; pos.z++) {
                    tileP = pos + tileObjTarget.nextTilePosition;

                    if(tileObjTarget.nextPositionDistanceTo(tileP) <= distance) {

                        if(LoadedMap.tryGetTile(tileP, out MapTile t)) {
                            if((stateMask & (int)t.State) > 0 || t.OcuppiedObj == requester) {
                                tiles.Add(t);
                            }
                        }

                    }
                }
            }

            return tiles;
        }

        public static T addTileAt<T>(Vector3 pos) where T : MapTile {
            T tile = (T)Activator.CreateInstance(typeof(T), pos, Vector3.up);

            if(LoadedMap.tryGetTile(RTSGameCtrl.worldToTilePosition(pos), out MapTile current)) {
                LoadedMap.overrideTile(tile);
            } else {
                LoadedMap.addTile(tile);
            }

            tile.recalculateArea();

            return tile;
        }
        public static T addTileAt<T>(Vector3Int pos) where T : MapTile{
            return addTileAt<T>(RTSGameCtrl.tileToWorldPosition(pos));
        }

        public static void drawGizmos() {
            LoadedMap?.drawGizmos();
        }
    }
}