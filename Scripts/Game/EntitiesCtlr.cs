﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System;
using RTS.Player;
using System.Collections.Concurrent;
using UnityEngine.AI;
using RTS.MapAI;
using RTS.Buildings;
using System.Linq;
using Shared.Collections.Pools;
using RTS.Game.Entities.Agents;
using RTS.Game.Entities;
using RTS.Game.Teams;

namespace RTS.Game {

    [RequireComponent(typeof(RTSGameCtrl))]
    public class EntitiesCtlr : MonoBehaviour  {

        static EntitiesCtlr INSTANCE;

        const int MAX_UPDATES = 1000;

        [SerializeField]
        float _AIThreadSleepTime = .5f;

        [Header("Teams")]
        [SerializeField]
        TeamConfig _teamConfig;

        static List<RTSAgent> _activeAgents = new List<RTSAgent>(100);
        static ConcurrentBag<RTSTeamEntity> _activeEntities = new ConcurrentBag<RTSTeamEntity>();
        
        static Dictionary<int, RTSAgent> _hiddenAgents = new Dictionary<int, RTSAgent>(20);        

        [SerializeField]
        EntitiesDictionary _entitiesDictionary;
        static SimpleGroupMonoBehaviourPool<RTSEntity> _entitiesPool;

        static ConcurrentQueue<RTSTeamEntity> _pendingSetTeamEntity = new ConcurrentQueue<RTSTeamEntity>();
        static ConcurrentQueue<RTSEntity> _pendingDeSpawnEntity = new ConcurrentQueue<RTSEntity>();

        int _currentPos = 0;
        int _iteration = 0;
        int _totalIteration = 0;

        Thread _aiUpdateThread;
        bool _isPlaying = true;

        RTSEntity _despawnedEntity;

        private void Awake() {
            if(INSTANCE != null) {
                Debug.LogError("TOO MANY EntitiesCtlr INSTANCES");
            }
            TeamsMng.createTeams(_teamConfig, _entitiesDictionary.getTeamMaterials());

            RTSGameCtrl.getInstance();
            _aiUpdateThread = new Thread(new ThreadStart(aiUpdate));
            _aiUpdateThread.Start();

            _entitiesPool = _entitiesDictionary.createEtitiesPool();

            INSTANCE = this;

            initPlacedEntities();

            _entitiesDictionary = null;
        }
        
        void aiUpdate() {

            Thread.Sleep(1000);
            float seconds;
            DateTime lastUpdate = DateTime.Now;


            while(_isPlaying) {

                seconds = (float)(DateTime.Now - lastUpdate).TotalSeconds;
                lastUpdate = DateTime.Now;

                if(seconds < _AIThreadSleepTime) {
                    Thread.Sleep((int)((_AIThreadSleepTime - seconds) * 1000));
                }

                foreach(var entity in _activeEntities) {
                    if(!entity.IsActive) continue;
                    if(entity.TeamKey < 0) continue;

                    entity.aiUpdate(seconds);
                }
                

                LogicAreasMng.updatePendingAreas();

                //Thread.Sleep(100);
            }

        }

        private void Update() {

            if(_pendingDeSpawnEntity.Count > 0) {
                deSpawnEntities();
            }

            if(_activeAgents.Count == 0 || !RTSTileMng.IsMapLoaded) return;

            _iteration = 0;
            _totalIteration = 0;

            while(_iteration < MAX_UPDATES && _totalIteration < 1000) {

                _totalIteration++;
                //if(!_allAgents[_currentPos].IsMoving) continue;

                _activeAgents[_currentPos].moveUpdate();

                _currentPos++;
                if(_currentPos >= _activeAgents.Count) {
                    _currentPos = 0;

                    if(_activeAgents.Count < MAX_UPDATES) break;
                }

                _iteration++;
            }
        }

        private void LateUpdate() {

            if(_pendingSetTeamEntity.Count > 0) {
                setBuildingsTeam();
            }
        }

        void deSpawnEntities() {
            while(_pendingDeSpawnEntity.Count > 0) {
                _despawnedEntity = _pendingDeSpawnEntity.Dequeue();
                _entitiesPool.despawn(_despawnedEntity);

                if(_despawnedEntity is RTSAgent) {
                    _activeAgents.Remove(_despawnedEntity as RTSAgent);
                }

                _despawnedEntity.IsActive = false;
                //_despawnedEntity.Position = Vector3.one * -100;
            }

            _despawnedEntity = null;
        }

        void setBuildingsTeam() {
            while(_pendingSetTeamEntity.Count > 0) {
                TeamsMng.addEntity(_pendingSetTeamEntity.Dequeue());
            }
        }
        

        void initPlacedEntities() {
            RTSAgent[] agents = FindObjectsOfType<RTSAgent>();

            foreach(var item in agents) {
                addEntity(item);
            }
        }

        public static void changeEntityTeam(int prevTeam, RTSTeamEntity entity) {
            TeamsMng.removeEntity(prevTeam, entity);
            _pendingSetTeamEntity.Enqueue(entity);
        }

        public static void addEntity(RTSTeamEntity entity) {

            //changeEntityTeam(TeamsMng.ALL_TEAMS, entity);


            if(entity is RTSAgent && !_activeAgents.Contains(entity)) {
                _activeAgents.Add(entity as RTSAgent);
            }

            if(!_activeEntities.Contains(entity)) {
                _activeEntities.Add(entity);
            }
        }

        /*public static int hideAgent(RTSAgent agent) {
            int key = 0;

            if(_hiddenAgents.Count > 0) {
                int[] keys = _hiddenAgents.Keys.ToArray();

                Array.Sort(keys);
                key = keys[keys.Length - 1] + 1;
            }

            _hiddenAgents.Add(key, agent);

            _pendingDeSpawnEntity.Enqueue(agent);

            return key;
        }*/

        public static void deSpawnEntity(RTSEntity entity) {
            _pendingDeSpawnEntity.Enqueue(entity);
        }

        public static RTSEntity spawnEntity(int key) {
            return spawnEntity(key, Vector3Int.zero);
        }
        public static RTSEntity spawnEntity(int key, Vector3Int tilePosition) {
            RTSEntity entity = _entitiesPool.spawn(key, RTSGameCtrl.tileToWorldPosition(tilePosition));

            entity.IsActive = true;

            if(entity is RTSTeamEntity) {
                addEntity(entity as RTSTeamEntity);
            }

            return entity;

        }


        private void OnApplicationQuit() {
            _aiUpdateThread.Abort();
            _isPlaying = false;
        }

    }
}