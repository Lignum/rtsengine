﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RTS.Game {
    public class MiniMap : MonoBehaviour {

        [SerializeField]
        Camera _cameraPre;

        public Rect MiniMapRect { get; private set; }
        Vector2 _worldSize;

        Camera _mainCamera;
        Plane _floorPlane;
        Vector2[] _frustumPlaneCorners;
        Ray _frustumRay;
        float _rayHit;
        RectTransform[] _frustrumImages;

        [SerializeField]
        Image _frustumLinePre;
        [SerializeField]
        float _frustumWidth = 3.5f;
        Vector2 _frustumSize;

        readonly static Vector3[] _frustumCorners = new Vector3[] { Vector3.zero, Vector3.up, Vector3.one, Vector3.right };

        void Awake() {

            calculateMiniMapRect();
            _worldSize = RTS.MapAI.MapCtr.WorldMapSize.toVector2Plane();

            createCamera();
            createFrustum();

        }

        private void Update() {
            //calculateMiniMapRect();
            calculateCameraFrustum();
        }

        void calculateMiniMapRect() {

            Vector3[] _corners = new Vector3[4];
            (transform as RectTransform).GetWorldCorners(_corners);

            Vector3 position = _corners[0];
            Vector3 size = _corners[2] - position;

            MiniMapRect = new Rect(position, size);
        }

        void createCamera() {
            Camera c = Instantiate(_cameraPre);

            c.transform.position = _worldSize.toVector3Z() / 2f + Vector3.up * 100;
            c.transform.forward = Vector3.down;

            Vector2 textureSize = Vector2.Min(_worldSize, MiniMapRect.size) * 2;
            
            RenderTexture miniMapTexture = new RenderTexture((int)textureSize.x, (int)textureSize.y, 0);
            c.targetTexture = miniMapTexture;
            c.orthographicSize = Mathf.Max(_worldSize.x, _worldSize.y) / 2;

            GetComponent<RawImage>().texture = miniMapTexture;
        }

        void createFrustum() {

            _mainCamera = Camera.main;
            _floorPlane = new Plane(Vector3.up, Vector3.zero);
            _frustumPlaneCorners = new Vector2[4];

            _frustrumImages = new RectTransform[4];
            Image frusImage;

            _frustumSize = new Vector2(_frustumWidth, 1);

            for(int i = 0; i < 4; i++) {
                frusImage = Instantiate(_frustumLinePre);// new GameObject("Fustrum_" + i).AddComponent<Image>();
                //frusImage.sprite = _frustumSprite;

                _frustrumImages[i] = frusImage.transform as RectTransform;

                _frustrumImages[i].SetParent(transform);
                _frustrumImages[i].pivot = new Vector2(.5f, 0);
            }

        }

        void calculateCameraFrustum() {

            for(int i = 0; i < 4; i++) {
                _frustumRay = _mainCamera.ViewportPointToRay(_frustumCorners[i]);
                if(_floorPlane.Raycast(_frustumRay, out _rayHit)) {
                    _frustumPlaneCorners[i] = worldToMiniMapPosition(_frustumRay.GetPoint(_rayHit));
                }
            }

            Vector2 next;
            for(int i = 0; i < 4; i++) {

                if(i < 3) {
                    next = _frustumPlaneCorners[i + 1];
                } else {
                    next = _frustumPlaneCorners[0];
                }

                _frustrumImages[i].position = MiniMapRect.position + _frustumPlaneCorners[i];
                _frustrumImages[i].up = next - _frustumPlaneCorners[i];

                _frustumSize.y = Vector2.Distance(next, _frustumPlaneCorners[i]) / _frustrumImages[0].lossyScale.y;

                _frustrumImages[i].sizeDelta = _frustumSize;
            }
        }

        public bool isPointOver(Vector2 screenPosition) {
            return MiniMapRect.Contains(screenPosition);
        }

        public Vector2 getPosition(Vector2 screenPosition) {
            return screenPosition - MiniMapRect.position;
        }

        public Vector3 miniMapToWorldPosition(Vector2 position) {
            return (_worldSize * (position / MiniMapRect.size)).toVector3Z();
        }

        public Vector2 worldToMiniMapPosition(Vector3 position) {
            return (position.toVector2Plane() * MiniMapRect.size) / _worldSize;
        }
    }
}