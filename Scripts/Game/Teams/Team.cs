﻿using RTS.AI;
using RTS.Buildings;
using RTS.Game;
using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using RTS.Jobs;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Game.Teams {

    public class Team {

        public const string TEAM_COLOR_PROPERTY = "_TeamColor";

        public Color TeamColor { get; private set; }
        protected Material _miniMapMat;
        protected Dictionary<string, Material> _teamMaterials;

        public HashSet<RTSAgent> agents;
        public HashSet<RTSTeamEntity> allEntities;

        //Dictionary<Type, HashSet<A_BuildingAI>> _buildings;
        HashSet<ResourceBuilding> _resourceBuildings;
        Dictionary<I_ResourceBuilding, HashSet<RTSTeamEntity>> _visibleResourcePiles;

        public int[] Resources { get; private set; }

        Action<int[]> _onResourcesChange;

        protected Team(Color color) {
            TeamColor = color;
        }

        public Team (Color color, Material[] teamMaterials) {
            TeamColor = color;

            createMinimapMat();
            createMaterials(teamMaterials);

            agents = new HashSet<RTSAgent>();
            allEntities = new HashSet<RTSTeamEntity>();

            //_buildings = new Dictionary<Type, HashSet<A_BuildingAI>>(5);
            _resourceBuildings = new HashSet<ResourceBuilding>();
            _visibleResourcePiles = new Dictionary<I_ResourceBuilding, HashSet<RTSTeamEntity>>();

            Resources = new int[GameDataMng.MaterialResourcesCount];
        }

        protected void createMinimapMat() {
            _miniMapMat = new Material(UnityEngine.Resources.Load<Material>("MiniMap/MiniMapProjection_MAT"));
            _miniMapMat.SetColor("_Color", TeamColor);
        }

        void createMaterials(Material[] teamMaterials) {
            _teamMaterials = new Dictionary<string, Material>(teamMaterials.Length);
            Material created;
            for(int i = 0; i < teamMaterials.Length; i++) {
                created = new Material(teamMaterials[i]);
                created.SetColor(TEAM_COLOR_PROPERTY, TeamColor);
                
                _teamMaterials.Add(created.name, created);
            }
        }

        public bool isTeamEntity(RTSTeamEntity entity) {
            return allEntities.Contains(entity);
        }

        public virtual void addEntity(RTSTeamEntity entity) {

            if(entity is RTSAgent) {
                addAgent(entity as RTSAgent);
            } else if(entity is RTSBuilding) {
                addBuilding(entity as RTSBuilding);
            }

            allEntities.Add(entity);

            updateMiniMapProjectionFor(entity);
            setTeamMaterials(entity);

        }

        public virtual void removeEntity(RTSTeamEntity entity) {
            if(entity is RTSAgent) {
                removeAgent(entity as RTSAgent);
            } else if(entity is RTSBuilding) {
                removeBuilding(entity as RTSBuilding);
            }

            allEntities.Remove(entity);
        }

        void addAgent(RTSAgent agent) {
            agents.Add(agent);
        }

        void addBuilding(RTSBuilding entity) {
            A_BuildingAI[] building = entity.BuildingsAI;

            if(entity is ResourceBuilding) {
                _resourceBuildings.Add(entity as ResourceBuilding);
            }
        }

        void removeAgent(RTSAgent agent) {
            agents.Remove(agent);
        }

        void removeBuilding(RTSBuilding entity) {
            A_BuildingAI[] building = entity.BuildingsAI;

            if(entity is ResourceBuilding) {
                _resourceBuildings.Remove(entity as ResourceBuilding);
            }
        }

        public void addVisibleResourcePile(ResourcePile pile, RTSTeamEntity viwer) {
            if(!_visibleResourcePiles.ContainsKey(pile)) {
                _visibleResourcePiles.Add(pile, new HashSet<RTSTeamEntity>());
            }
            _visibleResourcePiles[pile].Add(viwer);
        }

        public void removeVisibleResourcePile(ResourcePile pile, RTSTeamEntity viwer) {
            _visibleResourcePiles[pile].Remove(viwer);
            if(_visibleResourcePiles[pile].Count == 0) {
                _visibleResourcePiles.Remove(pile);
            }
        }

        protected virtual Material getTeamMaterial(string materialName) {
            if(_teamMaterials.TryGetValue(materialName, out Material mat)) {
                return mat;
            }
            return null;
        }

        protected void setTeamMaterials(RTSTeamEntity entity) {

            Material teamMaterial;
            Renderer[] renderers = entity.GetComponentsInChildren<Renderer>();

            foreach(var item in renderers) {

                if(item.sharedMaterial.HasProperty(TEAM_COLOR_PROPERTY)) {
                    teamMaterial = getTeamMaterial(item.sharedMaterial.name);

                    if(teamMaterial == null) {
                        Debug.LogError($"No Team material for: {item.sharedMaterial.name}");
                    }
                    item.sharedMaterial = teamMaterial;

                }
            }
        }

        protected void updateMiniMapProjectionFor(RTSTeamEntity entity) {
            entity.MiniMapProjection.material = _miniMapMat;
        }

        public HashSet<I_ResourceBuilding> getResourceBuildings(bool deposit, params int[] resourceType) {
            HashSet<I_ResourceBuilding> posibleDest = new HashSet<I_ResourceBuilding>();
            
            foreach(var item in _resourceBuildings) {

                if(deposit && item.IsFull) continue;
                if(!deposit && item.IsEmpty) continue;

                if(Array.IndexOf(resourceType, item.ResourceType)> -1) {
                    posibleDest.Add(item);
                }
            }

            if(!deposit) {
                foreach(var item in _visibleResourcePiles.Keys) {
                    if(item.IsEmpty) continue;

                    if(Array.IndexOf(resourceType, item.ResourceType) > -1) {
                        posibleDest.Add(item);
                    }
                }

            }

            return posibleDest;
        }


        public void onResourcesChange() {

            for(int i = 0; i < Resources.Length; i++) {
                Resources[i] = 0;
            }

            foreach(var item in _resourceBuildings) {
                Resources[item.ResourceType] += item.ResourcesCount;
            }
            
            _onResourcesChange?.Invoke(Resources);
        }

        public void addOnresourcesChange(Action<int[]> action) {
            _onResourcesChange += action;
        }

        public void drawGizmos() {
            foreach(var item in _visibleResourcePiles) {
                Gizmos.DrawCube(item.Key.Entity.WorldPosition, Vector3.one * .25f);
            }
        }
    }
}