﻿using RTS.Buildings;
using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Game.Teams {

    public static class TeamsMng  {

        public const int NO_TEAM = -1;
        public const int ALL_TEAMS = -2;

        const string PROJECTION_NAME = "MINI_MAP";

        static MeshRenderer MiniMapProjection;

        static Team _noTeam;
        static Team[] _teams;

        public static void createTeams(TeamConfig config, Material[] teamMaterials) {

            if(MiniMapProjection == null) {
                MiniMapProjection = UnityEngine.Resources.Load<MeshRenderer>("MiniMap/MiniMapProjection_PRE");
            }

            _teams = new Team[config.teamColors.Length];
            for(int i = 0; i < config.teamColors.Length; i++) {
                _teams[i] = new Team(config.teamColors[i], teamMaterials);
            }

            _noTeam = new EmptyTeam(config.noTeamColor, teamMaterials, config.noTeamMaterial);
        }


        public static void addEntity(RTSTeamEntity entity){
            getTeam(entity.TeamKey)?.addEntity(entity);
        }

        public static void removeEntity(int team, RTSTeamEntity entity) {
            getTeam(team)?.removeEntity(entity);
        }

        public static Team getTeam(int team) {

            if(team == NO_TEAM) return _noTeam;
            if(team == ALL_TEAMS) return null;

            return _teams[team];
        }


        public static MeshRenderer createMiniMapProjectionFor(RTSTeamEntity entity) {

            if(entity.TeamKey == TeamsMng.ALL_TEAMS) return null;

            MeshRenderer projection = entity.MiniMapProjection;
            if(projection == null) {
                projection = GameObject.Instantiate(MiniMapProjection);
                projection.name = PROJECTION_NAME;

                projection.transform.SetParent(entity.transform);
                projection.transform.localPosition = Vector3.zero;//new Vector3(0, .2f, 0);
            }

            Vector3 size = new Vector3(entity.Size.x * RTSGameCtrl.TileSize, 1, entity.Size.z * RTSGameCtrl.TileSize);
            projection.transform.localScale = size;

            return projection;

        }

    }
}