﻿using UnityEngine;

namespace RTS.Game.Teams {

    [System.Serializable]
    public struct TeamConfig  {
        public Color noTeamColor;
        public Material noTeamMaterial;

        public Color[] teamColors;

    }
}