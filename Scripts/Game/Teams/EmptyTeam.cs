﻿using System.Collections;
using System.Collections.Generic;
using RTS.Buildings;
using RTS.Game.Entities;
using UnityEngine;

namespace RTS.Game.Teams {
    public class EmptyTeam : Team {

        public EmptyTeam(Color color, Material[] teamMaterials, Material noTeamMaterial) : base (color) {
            createMinimapMat();

            createMaterials(teamMaterials);
        }

        void createMaterials(Material[] teamMaterials) {
            _teamMaterials = new Dictionary<string, Material>(teamMaterials.Length);
            Material created;
            for(int i = 0; i < teamMaterials.Length; i++) {
                created = new Material(teamMaterials[i]);
                created.SetColor(TEAM_COLOR_PROPERTY, TeamColor);
                created.SetFloat("_GrayScale_", 1);

                _teamMaterials.Add(created.name, created);
            }
        }

        public override void addEntity(RTSTeamEntity entity) {
            updateMiniMapProjectionFor(entity);
            setTeamMaterials(entity);
        }

        public override void removeEntity(RTSTeamEntity entity) {}
    }
}