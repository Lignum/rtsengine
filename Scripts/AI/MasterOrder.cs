﻿using RTS.MapAI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.AI {
    public struct MasterOrder  {

        public Vector3Int ClickedPosition;
        public MapTile Tile;
        public TiledObj TargetObj;

        public bool CtrlModifier;

        public MasterOrder(TiledObj target) : this (target.TilePosition, null, target) {}
        public MasterOrder(Vector3Int clickedPosition, MapTile tile, TiledObj target) {

            ClickedPosition = clickedPosition;
            Tile = tile;
            TargetObj = target;

            CtrlModifier = Input.GetKey(KeyCode.LeftControl);
        }
    }
}