﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.AI.Brain {

    public enum ComparationType { Equals, Lower, Bigger}

    public class TransitionValue {

        public string ValueName { get; private set; }

        readonly float _compareValue;
        readonly ComparationType _comparation;


        public TransitionValue(string name, ComparationType type, float compareValue) {
            ValueName = name;
            _comparation = type;
            _compareValue = compareValue;
        }

        public bool meetsCondition(float value) {
            switch(_comparation) {
                case ComparationType.Equals:
                    return _compareValue == value;
                case ComparationType.Lower:
                    return value < _compareValue;
                case ComparationType.Bigger:
                    return value > _compareValue;
            }

            return false;
        }
    }
}