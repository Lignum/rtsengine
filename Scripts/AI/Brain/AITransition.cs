﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.AI.Brain {
    public class AITransition {
        
        HashSet<TransitionValue> _valueConditions;
        public AINode TargetNode { get; private set; }

        Func<int> _priorityCalculation;

        public AITransition(TransitionValue condition, AINode target) {
            _valueConditions = new HashSet<TransitionValue>() { condition };
            TargetNode = target;
        }
        public AITransition(HashSet<TransitionValue> conditions, AINode target) {
            _valueConditions = conditions;
            TargetNode = target;
        }

        public bool checkConditions(Dictionary<string, float> values) {

            foreach(var item in _valueConditions) {
                if(!item.meetsCondition(values[item.ValueName])) return false;
            }

            return true;
        }

        public void setPriorityCalculation(Func<int> calcs) {
            _priorityCalculation = calcs;
        }
        

    }
}
    