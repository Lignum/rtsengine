﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.AI.Brain {
    public class AINode  {

        public string Name { get; private set; }

        HashSet<AITransition> _trasitions;
        System.Action<float> _actions;
        System.Action<float> _endActions;

        public AINode(string name) {
            _trasitions = new HashSet<AITransition>();
            Name = name;
        }

        public void addTransition(AITransition transition) {
            _trasitions.Add(transition);
        }

        public AITransition checkTransitions(Dictionary<string, float> values) {

            foreach(var item in _trasitions) {
                if(item.checkConditions(values)) return item;
            }

            return null;
        }

        public void addAction(System.Action<float> action) {
            _actions += action;
        }

        public void execute(float seconds) {
            _actions?.Invoke(seconds);
        }


        public void addEndAction(System.Action<float> action) {
            _endActions += action;
        }

        public void executeEnd(float seconds) {
            _endActions?.Invoke(seconds);
        }

    }
}