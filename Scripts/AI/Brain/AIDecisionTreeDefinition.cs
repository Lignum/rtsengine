﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RTS.AI.Brain.Definitions {

    [CreateAssetMenu(fileName = "DecisionTreeDefinition", menuName = "RTS/AI/DecisionTreeDefinition", order = 1)]
    public class AIDecisionTreeDefinition : ScriptableObject {

        public string startNode;
        public DecisionTreeNodeDefinition AnyNode;
        public DecisionTreeNodeDefinition[] nodes;

        public AIDecisionTree toDecisionTree() {

            if(startNode == "") Debug.LogError("Need to define a startNode", this);

            HashSet<string> variables = getVariableNames();
            Dictionary<string, AINode> aiNodes = createNodes();
            createTransitions(aiNodes);

            HashSet<AITransition> transitions = AnyNode.createTransitions(aiNodes);
            AINode any = AnyNode.toAINode();
            foreach(var item in transitions) {
                any.addTransition(item);
            }

            AIDecisionTree created = new AIDecisionTree(variables, any, aiNodes, aiNodes[startNode]);

            return created;
        }


        HashSet<string> getVariableNames() {
            List<string> names = new List<string>();
            foreach(var node in nodes) {
                names.AddRange(node.getVariableNames());
            }

            return new HashSet<string>(names);
        }

        Dictionary<string, AINode> createNodes() {
            Dictionary<string, AINode> dic = new Dictionary<string, AINode>(nodes.Length);

            foreach(var node in nodes) {
                dic.Add(node.name, node.toAINode());
            }

            return dic;
        }

        void createTransitions(Dictionary<string, AINode> aiNodes) {
            HashSet<AITransition> transitions;
            foreach(var node in nodes) {
                transitions = node.createTransitions(aiNodes);
                foreach(var t in transitions) {
                    aiNodes[node.name].addTransition(t);
                }
            }
        }
        
        [System.Serializable]
        public class DecisionTreeNodeDefinition {

            public string name;

            public DecisionTreeTransitionDefinition[] transitions;

            public List<string> getVariableNames() {
                List<string> names = new List<string>();
                foreach(var transition in transitions) {
                    names.AddRange(transition.getVariableNames());
                }

                return names;
            }

            public AINode toAINode() {
                AINode node = new AINode(name);

                return node;
            }

            public HashSet<AITransition> createTransitions(Dictionary<string, AINode> aiNodes) {
                HashSet<AITransition> transition = new HashSet<AITransition>();

                foreach(var t in transitions) {
                    transition.Add(t.toAITransition(aiNodes[t.nodeTarget]));
                }

                return transition;
            }
        }

        [System.Serializable]
        public class DecisionTreeTransitionDefinition {

            public string nodeTarget;
            public TransitionValueDefinition[] variables;

            public HashSet<string> getVariableNames() {
                HashSet<string> names = new HashSet<string>();
                foreach(var item in variables) {
                    names.Add(item.ValueName);
                }
                return names;
            }

            public AITransition toAITransition(AINode target) {
                return new AITransition(createConditions(), target);
            }

            HashSet<TransitionValue> createConditions() {
                HashSet<TransitionValue> conditions = new HashSet<TransitionValue>();
                foreach(var var in variables) {
                    conditions.Add(var.toTransitionValue());
                }
                return conditions;
            }

        }

        [System.Serializable]
        public class TransitionValueDefinition {

            public string ValueName;
            public ComparationType comparationType;
            public float compareValue;

            public TransitionValue toTransitionValue() {
                return new TransitionValue(ValueName, comparationType, compareValue);
            }
        }

    }



}