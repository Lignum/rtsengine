﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.AI.Brain {
    public class AIDecisionTree {

        const float DECISION_TIME = .5f;

        AINode _anyNode;
        AINode _currentNode;
        Dictionary<string, float> _values;
        Dictionary<string, AINode> _nodes;

        float _lastChangeTime = 0;

        public AIDecisionTree(HashSet<string> variables, AINode anyNode, Dictionary<string, AINode> nodes, AINode startNode) {

            _anyNode = anyNode;

            _values = new Dictionary<string, float>(variables.Count);
            _nodes = nodes;

            foreach(var item in variables) {
                _values.Add(item, 0);
            }

            _currentNode = startNode;
        }

        public void addAction(string node, System.Action<float> action) {
            _nodes[node].addAction(action);
        }
        public void addEndAction(string node, System.Action<float> action) {
            _nodes[node].addEndAction(action);
        }

        public void addAnyNodeTransition(AITransition trasintion) {
            _anyNode.addTransition(trasintion);
        }

        public void setValue(string name, float val) {
            _values[name] = val;
        }


        public void execute(float seconds) {
            if(_lastChangeTime == 0) {
                AITransition anyTrans = _anyNode.checkTransitions(_values);
                AITransition trans = _currentNode.checkTransitions(_values);

                AITransition chosen = null;
                
                if(anyTrans != null) chosen = anyTrans;
                else chosen = trans;

                //if(trans != null) chosen = trans;
                //else chosen = anyTrans;

                if(chosen != null) {
                    _currentNode.executeEnd(seconds);
                    _currentNode = chosen.TargetNode;
                    _lastChangeTime += seconds;
                } else {
                    _currentNode.execute(seconds);
                }
            }else if(_lastChangeTime < DECISION_TIME) {
                _lastChangeTime += seconds;

            } else if(_lastChangeTime >= DECISION_TIME) {
                _lastChangeTime = 0;
                _currentNode.execute(seconds);
            }


        }

        public override string ToString() {
            return $"AIDecisionTree State - Current: {_currentNode.Name}";
        }

    }
}