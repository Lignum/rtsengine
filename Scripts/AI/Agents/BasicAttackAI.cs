﻿using System.Collections;
using System.Collections.Generic;
using RTS.Game;
using RTS.Game.Entities;
using RTS.Jobs;
using RTS.MapAI;
using RTS.MapAI.PathFinding;
using UnityEngine;

namespace RTS.AI {
    
    public class BasicAttackAI : BasicMoveAI {
        

        protected override void updateAI(float seconds) {

            if(_target != null) {
                attack(seconds);
            } else {
                findNextTarget();
            }
        }

        void attack(float seconds) {
            attackAction(_target);
        }

        protected override void onRecibeMasterOrder(MasterOrder order) {
            RTSTeamEntity entityTarget = order.TargetObj as RTSTeamEntity;

            if(entityTarget != null && entityTarget.TeamKey != Agent.TeamKey) {
                _target = entityTarget;
            } else {
                _target = null;
            }
        }

        /*public override void doAttack() {
            if(_target != null) {
                _target.reciveDamage(damage);
            }
        }*/

        void findNextTarget() {

            if(!Agent.HasMasterOrder && !Agent.IsMoving && CanTakeDecision && visibleEnemies.Count > 0) {
                RTSTeamEntity[] enemyOrderer = getOrderedEntitiesByDistance(visibleEnemies, Agent.WorldPosition);

                foreach(var item in enemyOrderer) {
                    //Debug.LogError("if(RTSTileMng.getClosestEmptyTilePos(item.TilePosition, _agent, AttackRange) != Vector3Int.zero)");
                    //if(_agent.Team == 1) Debug.Log($"{enemyOrderer.Length} -- { RTSTileMng.getClosestEmptyTile(item, _agent, AttackRange)}");
                    if(RTSTileMng.getClosestTileToRequester(item, Agent, AIData.AttackRange, (int)PathTileState.Empty) != null) {
                        _target = item;
                        break;
                    }
                }
                resetReactionTime();
            }
        }

        protected override void onAgentsSpoted(RTSTeamEntity[] agents, HashSet<RTSTeamEntity> enemyAgents, HashSet<RTSTeamEntity> teamAgents) {
            
            if(_target == null && enemyAgents.Count > 0) {
                findNextTarget();
            }

        }

        protected override void onAgentsLost(RTSTeamEntity[] agents, HashSet<RTSTeamEntity> enemyAgents, HashSet<RTSTeamEntity> teamAgents) {

            if(_target != null) {
                foreach(var item in enemyAgents) {
                    if(item == _target) {
                        _target = null;
                        break;
                    }
                }
            }
        }


        private void OnDrawGizmosSelected() {
            Gizmos.color = Color.red;
            foreach(var item in visibleEnemies) {
                Gizmos.DrawLine(Agent.WorldPosition, item.WorldPosition);
            }


            if(_target != null) {
                //Debug.Log(Vector3.Distance(_agent.WorldPosition, _attackTarget.WorldPosition));
                Gizmos.color = Color.blue;
                Gizmos.DrawCube(_target.WorldPosition, Vector3.one * .5f);
            }
        }

        public override void setJob(Job job) {
            throw new System.NotImplementedException();
        }
    }
}