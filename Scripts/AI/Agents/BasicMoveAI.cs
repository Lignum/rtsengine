﻿using RTS.Game;
using RTS.Game.Entities;
using RTS.MapAI;
using RTS.MapAI.PathFinding;
using System;
using UnityEngine;

namespace RTS.AI {

    public enum AttackingState { Start, Attacking, End, Waiting }

    public abstract class BasicMoveAI : A_AgentAI {
        
        protected RTSEntity _target;
        public bool HasTarget { get { return _target != null; } }

        protected bool _attacking = false;
        protected AttackingState _attackingState = AttackingState.Waiting;

        Animator animator;
        protected RTSAnimator _agentAnimations;
        protected Action<RTSEntity> _attackAction;

        protected bool _stopInteract = false;

        protected override void Awake() {
            base.Awake();

            animator = GetComponent<Animator>();
            _agentAnimations = new RTSAnimator(this);
        }

        protected virtual void Start() {
            
        }

        protected override float takeDamage(float damage) {
            return damage;
        }

        protected override void updateAgent() {
            
            if(Agent.StartMoving) {
                _attackingState = AttackingState.Waiting;
                _agentAnimations.stopLooking();
            }

            animator.SetBool("StartWalking", Agent.StartMoving || Agent.IsMoving);
            animator.SetBool("StopWalking", Agent.EndMoving);

            animator.SetBool("StartAttack", _attackingState == AttackingState.Attacking);
            animator.SetBool("StopAttack", _attackingState == AttackingState.End || _attackingState == AttackingState.Waiting);


            if(!Agent.IsMoving) {
                if(_target != null) {
                    _agentAnimations.playAgentLookTo(Agent, _target, 1);
                }

                if(_attackingState == AttackingState.Start) {//attack
                    _attackingState = AttackingState.Attacking;
                } else if(_attackingState == AttackingState.End) {
                    _attackingState = AttackingState.Waiting;
                }
            }

        }

        protected void interactTarget(RTSEntity target, float range, Action interactAction) {

            _target = target;
            _attacking = false;

            if(!Agent.HasMasterOrder) {
                
                if(!isTargetInRange(target, range) && !hasPathToTarget(target, range)) {
                    MapTile attackPos = RTSTileMng.getClosestTileToRequester(target, Agent, range, (int)PathTileState.Empty);

                    if(attackPos != null) {
                        Agent.setDestination(attackPos, false);
                    } else {
                        _target = null;
                        endAttack();
                    }
                    resetReactionTime();

                } else if(!Agent.IsMoving && isTargetInRange(target, range)) {
                    interactAction();
                }
            }
        }


        protected void attackAction(RTSEntity target) {

            if(target != null) {
                interactTarget(target, AIData.AttackRange, () => startAttack());
            }

            if(_attacking) {
                if(_attackingState == AttackingState.End || _attackingState == AttackingState.Waiting) _attackingState = AttackingState.Start;
            } else {
                if(_attackingState == AttackingState.Start || _attackingState == AttackingState.Attacking) _attackingState = AttackingState.End;
            }
        }

        void startAttack() {
            _attacking = true;
        }

        public void setTarget(RTSEntity target) {
            _target = target;
        }
        
        public void doAttack() {
            _attackAction(_target);
        }

        protected void endAttack() {
            _attacking = false;
            _target = null;
            if(_attackingState == AttackingState.Start || _attackingState == AttackingState.Attacking) _attackingState = AttackingState.End;
        }

    }
}