﻿using RTS.Game;
using RTS.Game.Entities;
using RTS.Jobs;
using RTS.MapAI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.AI {
    public class AttackerAI : WorkerAI {

        protected override void Awake() {
            base.Awake();

            setJob(new AttackJob(this));
            //_activeJob = new AttackJob(this);
        }

        protected override void onAgentsSpoted(RTSTeamEntity[] agents, HashSet<RTSTeamEntity> enemyAgents, HashSet<RTSTeamEntity> teamAgents) {
            foreach(var target in enemyAgents) {
                _activeJob.addTarget(target);
            }
        }

        protected override void onAgentsLost(RTSTeamEntity[] agents, HashSet<RTSTeamEntity> enemyAgents, HashSet<RTSTeamEntity> teamAgents) {
            foreach(var target in enemyAgents) {
                _activeJob.removeTarget(target);
            }
        }

        protected override void onRecibeMasterOrder(MasterOrder order) {
            
            if(order.TargetObj is RTSTeamEntity) {
                RTSTeamEntity entityTarget = order.TargetObj as RTSTeamEntity;
                if(entityTarget.TeamKey != Agent.TeamKey) {
                    _activeJob.addTarget(entityTarget);
                    setTarget(entityTarget);
                }
            } else if(order.Tile != null){
                setTarget(null);
                setOnMasterOrder();
            }

        }
    }
}