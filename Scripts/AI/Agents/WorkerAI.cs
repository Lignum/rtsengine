﻿using System;
using System.Collections;
using System.Collections.Generic;
using RTS.AI.Brain;
using RTS.Buildings;
using RTS.Game;
using RTS.Game.Entities;
using RTS.Jobs;
using RTS.MapAI;
using RTS.MapAI.PathFinding;
using UnityEngine;

namespace RTS.AI {

    public class WorkerAI : BasicMoveAI {

        public int CurrentMateriaResourceType { get; private set; } = -1;
        public int MaterialResourcesCount { get; private set; }

        public bool CanCarryMore { get { return MaterialResourcesCount < AIData.MaxCarryMaterialResources; } }

        public JobState State { get; private set; } = JobState.Idle;
        
        float _interactTime = 0;

        protected override void Awake() {
            base.Awake();
            Agent.addOnWalkPathEnd(onWalkPathEnd);
        }

        protected override void onAgentsSpoted(RTSTeamEntity[] agents, HashSet<RTSTeamEntity> enemyAgents, HashSet<RTSTeamEntity> teamAgents) { }

        protected override void onAgentsLost(RTSTeamEntity[] agents, HashSet<RTSTeamEntity> enemyAgents, HashSet<RTSTeamEntity> teamAgents) { }

        protected override void onRecibeMasterOrder(MasterOrder order) {
            
            if(order.CtrlModifier){
                if(_activeJob != null) {
                    _activeJob.checkMasterOrder(order);
                }
                
            } else {
                clearJob();
                setOnMasterOrder();

                setJob(AIData.JobsFilter.getJob(Agent, order.TargetObj));
            }


        }

        protected override void updateAgent() {
            base.updateAgent();
        }

        protected override void updateAI(float seconds) {

            if(_activeJob != null) {
                _activeJob.execute(seconds);
            }
        }

        public int getMaxCarryReourceMaterial(int resourceType) {
            return AIData.MaxCarryMaterialResources;
        }

        public void clearJob() {
            setJob(null);
            setIdle();
        }

        public override void setJob(Job job) {
            base.setJob(job);

            if(_activeJob != null) {
                _activeJob.setWorker(this);
            }
        }

        public void attackTarget() {
            attackEntity(_target);
        }
        public void attackEntity(RTSEntity target) {

            _attackAction = attackEntityAction;
            attackAction(target);

            State = JobState.Attack;
        }

        void attackEntityAction(RTSEntity target) {
            target?.reciveDamage(AIData.Damage);
        }

        public void attackResource(RTSEntity target) {
            _attackAction = attackResourceAction;
            CurrentMateriaResourceType = (target as ResourceMaterialEntity).ResourceMaterialType;

            attackAction(target);
            State = JobState.Gather;
        }

        void attackResourceAction(RTSEntity target) {
            if(target != null) {
                MaterialResourcesCount += (target as ResourceMaterialEntity).gatherResource(AIData.Damage, AIData.MaxCarryMaterialResources - MaterialResourcesCount);
            }

            if(MaterialResourcesCount >= AIData.MaxCarryMaterialResources) {
                endAttack();
            }
        }

        /*public override void doAttack() {
            if(_target != null) {
                Resources += (_target as ResourceEntity).gatherResource(damage, MaxCarryResources - Resources);
            }
        }*/

        public void build(BuildingOrder building, float seconds) {
            interactTarget(building, AIData.InteractRange, () => doInteract(seconds, building.build));
        }
        
        public void depositResources(I_ResourceBuilding storage, float seconds) {
            interactTarget(storage.Entity, AIData.InteractRange, () => doInteract(seconds, () => depositResourcesAction(storage)));
            State = JobState.Deposit;
        }

        void depositResourcesAction(I_ResourceBuilding storage) {
            if(MaterialResourcesCount > 0) {
                MaterialResourcesCount--;
                MaterialResourcesCount += storage.addResource(this, CurrentMateriaResourceType, 1);

                if(MaterialResourcesCount == 0){
                    CurrentMateriaResourceType = -1;
                }
            }
        }

        public void takeResources(I_ResourceBuilding storage, int maxCuantity, float seconds) {
            if(CurrentMateriaResourceType == -1) {
                CurrentMateriaResourceType = storage.ResourceType;
            } else if(CurrentMateriaResourceType != storage.ResourceType) {
                Debug.LogError($"Incompatilble Resource Material Types: {CurrentMateriaResourceType} -> {storage.ResourceType}");
                return;
            }

            if(!storage.IsEmpty && MaterialResourcesCount < maxCuantity) {
                interactTarget(storage.Entity, AIData.InteractRange, () => doInteract(seconds, () => takeResourcesAction(storage)));
                State = JobState.Take;
            } else {
                State = JobState.Idle;
            }
        }

        void takeResourcesAction(I_ResourceBuilding storage) {
            if(MaterialResourcesCount < AIData.MaxCarryMaterialResources) {
                MaterialResourcesCount += storage.takeResource(1);
            }
        }

        public void enterBuilding(RTSBuilding building, float seconds) {
            interactTarget(building, AIData.InteractRange, () => enterBuildingAction(building));
        }

        void enterBuildingAction(RTSBuilding building) {
            building.enterAgent(Agent);
            _target = null;
        }

        protected void doInteract(float seconds, Action interactAction) {
            if(_interactTime >= AIData.InteractTimeIntervals) {

                interactAction();

                _interactTime = 0;
            } else {
                _interactTime += seconds;
            }
        }

        public bool isCarringResources(params int[] resourceType) {
            return Array.IndexOf(resourceType, CurrentMateriaResourceType) > -1 && MaterialResourcesCount > 0;
        }
        public bool isCarringOtherResources(params int[] resourceType) {
            return Array.IndexOf(resourceType, CurrentMateriaResourceType) == -1 && MaterialResourcesCount > 0;
        }

        public void setIdle() {
            _stopInteract = true;
            State = JobState.Idle;
            endAttack();
        }

        public void setOnMasterOrder() {
            State = JobState.OnMasterOrder;
            endAttack();

            //_masterOrderTime = 1;
            resetReactionTime();

            //Debug.Log("Master");
        }

        public void onWalkPathEnd() {
            //Debug.Log("END: " + State);
            if(State == JobState.OnMasterOrder) {
                setIdle();
            }
        }

        private void OnDrawGizmos() {            
            if(_activeJob != null) {
                _activeJob.drawGizmos();
            }

            if(Agent != null) {
                GameDataMng.drawMaterialResourcesGizmos(Agent.WorldPosition, CurrentMateriaResourceType, MaterialResourcesCount);
            }
        }
    }

}