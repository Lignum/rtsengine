﻿using RTS.Data.Menus;
using RTS.Game;
using RTS.Game.Entities;
using RTS.Game.Entities.Agents;
using RTS.Jobs;
using RTS.MapAI;
using RTS.MapAI.PathFinding;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.AI {

    [RequireComponent(typeof(RTSAgent))]
    public abstract class A_AgentAI : MonoBehaviour {

        const float REACTION_TIME = 1f;

        [SerializeField]
        public EntityAIData AIData;

        public RTSAgent Agent { get; private set; }
        public int Team { get { return Agent.TeamKey; } }

        public float TimeForNextDecision { get; private set; } = 0;
        public bool CanTakeDecision { get { return TimeForNextDecision <= 0; } }

        protected HashSet<RTSTeamEntity> visibleEnemies = new HashSet<RTSTeamEntity>();
        protected HashSet<RTSTeamEntity> visibleTeam = new HashSet<RTSTeamEntity>();

        Action<Job> _onJobSet;
        protected Job _activeJob { get; private set; }

        protected virtual void Awake() {
            Agent = GetComponent<RTSAgent>();

            Agent.addOnUpdateAI(onUpdateAI);
            Agent.addOnAgentUpdate(updateAgent);

            Agent.addOnTakeDamage(takeDamage);
            Agent.addOnEntitiesSpoted(agentsSpoted);
            Agent.addOnEntitiesLost(agentsLost);

            Agent.addOnRecibeMasterOrder(recibeMasterOrder);
            
        }

        protected virtual void recibeMasterOrder(MasterOrder order) {
            resetReactionTime();
            onRecibeMasterOrder(order);
        }
        protected abstract void onRecibeMasterOrder(MasterOrder order);
        
        public virtual void setJob(Job job) {
            _activeJob = job;
            _onJobSet?.Invoke(job);
        }
        public Job getJob() {
            return _activeJob;
        }
        public void addOnJobSetEvent(Action<Job> action) {
            _onJobSet += action;
        }
        public void removeOnJobSetEvent(Action<Job> action) {
            _onJobSet -= action;
        }

        protected abstract float takeDamage(float damage);

        void onUpdateAI(float seconds) {
            updateAI(seconds);

            //_decisionTree.execute(seconds);

            if(TimeForNextDecision > 0) {
                TimeForNextDecision -= seconds;
            }
        }
        protected abstract void updateAI(float seconds);

        protected abstract void updateAgent();

        protected virtual void agentsSpoted(RTSTeamEntity[] agents) {
            getEnemyAgents(agents, out HashSet<RTSTeamEntity> enemies, out HashSet<RTSTeamEntity> team);
            foreach(var item in enemies) {
                visibleEnemies.Add(item);
            }
            foreach(var item in team) {
                visibleTeam.Add(item);
            }

            onAgentsSpoted(agents, enemies, team);

        }
        protected virtual void agentsLost(RTSTeamEntity[] agents) {
            getEnemyAgents(agents, out HashSet<RTSTeamEntity> enemies, out HashSet<RTSTeamEntity> team);
            foreach(var item in enemies) {
                visibleEnemies.Remove(item);
            }
            foreach(var item in team) {
                visibleTeam.Remove(item);
            }

            onAgentsLost(agents, enemies, team);
        }

        protected abstract void onAgentsSpoted(RTSTeamEntity[] agents, HashSet<RTSTeamEntity> enemyAgents, HashSet<RTSTeamEntity> teamAgents);
        protected abstract void onAgentsLost(RTSTeamEntity[] agents, HashSet<RTSTeamEntity> enemyAgents, HashSet<RTSTeamEntity> teamAgents);

        protected virtual void getEnemyAgents(RTSTeamEntity[] agents, out HashSet<RTSTeamEntity> enemyAgents, out HashSet<RTSTeamEntity> teamAgents) {
            enemyAgents = new HashSet<RTSTeamEntity>();
            teamAgents = new HashSet<RTSTeamEntity>();

            foreach(var item in agents) {
                if(item.TeamKey == Agent.TeamKey) teamAgents.Add(item);
                else enemyAgents.Add(item);
            }
        }
        

        protected RTSAgent getClosestAgent(RTSAgent[] agents) {
            if(agents.Length <= 0) return null;

            RTSAgent closests = agents[0];
            float minDistance = Vector3.Distance(Agent.WorldPosition, closests.WorldPosition);
            float dist;
            foreach(var agent in agents) {
                if(agent == closests) continue;

                dist = Vector3.Distance(Agent.WorldPosition, Agent.WorldPosition);
                if(dist < minDistance) {
                    minDistance = dist;
                    closests = agent;
                }
            }

            return closests;
        }

        void orderedEntities(RTSTeamEntity[] agents, Vector3 center) {

            RTSTeamEntity a1;
            RTSTeamEntity a2;

            bool end = false;
            while(!end) {

                end = true;

                for(int i = 0; i < agents.Length - 1; i++) {
                    a1 = agents[i];
                    a2 = agents[i + 1];
                    if(Vector3.Distance(center, a2.WorldPosition) < Vector3.Distance(center, a1.WorldPosition)) {
                        agents[i] = a2;
                        a2 = agents[i + 1] = a1;

                        end = false;
                    }
                }
            }
        }

        protected RTSTeamEntity[] getOrderedEntitiesByDistance(HashSet<RTSTeamEntity> agents, Vector3 center) {
            RTSTeamEntity[] orderer = new RTSTeamEntity[agents.Count];

            int i = 0;
            foreach(var item in agents) {
                orderer[i] = item;
                i++;
            }

            orderedEntities(orderer, center);

            return orderer;
        }

        protected void resetReactionTime() {
            TimeForNextDecision = REACTION_TIME;
        }

        protected bool hasPathToTarget(TiledObj target, float range) {
            if(target == null) return false;

            if(Agent.HasPath && target.nextPositionDistanceTo(Agent.PathDestination.TilePosition) <= range) return true;
            if(Agent.RequestedDestination != null && target.nextPositionDistanceTo(Agent.RequestedDestination.TilePosition) <= range) return true;

            return false;
        }

        protected bool isTargetInRange(TiledObj target, float range) {
            if(target == null) return false;

            return target.distanceTo(Agent.TilePosition) <= range;
        }
    }
}