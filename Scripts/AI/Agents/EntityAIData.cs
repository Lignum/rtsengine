﻿using RTS.Game;
using RTS.Jobs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.AI {

    [CreateAssetMenu(fileName = "EntityAIData", menuName = "RTS/Entities/EntityAIData", order = 1)]
    public class EntityAIData : ScriptableObject {

        [Header("Battle Data")]
        [SerializeField]
        int _damage = 1;
        public int Damage { get { return _damage; } }

        [SerializeField, Min(1)]
        float _attackRange = 1;
        public float AttackRange { get { return (_attackRange + .75f) * RTSGameCtrl.TileSize; } }

        public float InteractRange { get { return 1.75f * RTSGameCtrl.TileSize; } }

        [SerializeField]
        float _interactTimeIntervals = 1;
        public float InteractTimeIntervals { get { return _interactTimeIntervals; } }

        [SerializeField]
        int _maxCarryMaterialResources = 1;
        public int MaxCarryMaterialResources { get { return _maxCarryMaterialResources; } }

        [SerializeField]
        JobFilter _jobsFilter;
        public JobFilter JobsFilter { get { return _jobsFilter; } }
    }
}