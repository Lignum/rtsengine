﻿using RTS.Buildings;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.AI {

    [RequireComponent(typeof(RTSBuilding))]
    public abstract class A_BuildingAI : MonoBehaviour {

        public RTSBuilding Building { get; private set; }

        public Vector3Int TilePosition {
            get {
                return Building.TilePosition;
            }
        }

        private void Awake() {
            Building = GetComponent<RTSBuilding>();
        }

        public string BuildingType { get { return this.GetType().Name; } }
        public abstract void onBuildingSelected();
        
    }
}