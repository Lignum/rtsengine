﻿using RTS.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.CameraCtrls {
    
    public class RTSCameraController : MonoBehaviour {

        public static RTSCameraController INSTANCE;

        [Header("Input Configuration")]
        public bool useKeyboardInput = true;
        public bool useMouseForMovement = true;
        public bool useMouseForRotation = true;

        [Header("Speed Configurations")]
        public float moveSpeed = 10;
        public float rotationSpeed = 10;
        public float zoomSpeed = 10;

        [Header("Mouse Configurations")]
        public float mouseMoveSensitivity = 10;
        public float mouseRotationSensitivity = 10;

        [Header("Camera Pos")]
        public float minZoom = 10;
        public float maxZoom = 100;

        /*[Header("Key Configurations")]
        public KeyCode keyForward = KeyCode.W;
        public KeyCode keyBack = KeyCode.B;
        public KeyCode keyRight = KeyCode.R;
        public KeyCode keyLeft = KeyCode.L;*/

        Vector3 _targetPos;

        Vector3 _lastMousePos = Vector3.zero;
        float _targetDistance;

        float _moveH;
        float _moveV;

        float _rotH;
        float _rotV;

        public float Zoom { get; private set; }

        Plane _floorPlane = new Plane(Vector3.up, 0);
        Camera _camera;

        private void Awake() {
            INSTANCE = this;
        }

        void Start() {

            createTarget();

            _camera = GetComponent<Camera>();
            _targetDistance = Vector3.Distance(transform.position, _targetPos);
        }

        void Update() {

            if(MouseCtrl.getMiniMapMouseButtonDown(0)) {
                _targetPos = MouseCtrl.MouseMiniMapGroundPos;
            }

            translateCamera();

            transform.position = _targetPos;
            transform.Translate(Vector3.back * _targetDistance);

            rotateCamera();
            zoomCamera();

            _lastMousePos = Input.mousePosition;
            _targetDistance = Vector3.Distance(transform.position, _targetPos);

        }
        

        void createTarget() {
            Vector3 pos;

            Ray r = new Ray(transform.position, transform.forward);

            if(_floorPlane.Raycast(r, out float dis)) {
                pos = r.GetPoint(dis);

                _targetPos = pos;
            }
        }

        void translateCamera() {

            if(useKeyboardInput) {
                _moveH = Input.GetAxis("Horizontal");
                _moveV = Input.GetAxis("Vertical");

                Vector3 forward = transform.forward;
                forward.y = 0;
                forward.Normalize();

                _targetPos += transform.right * _moveH * moveSpeed * Time.deltaTime;
                _targetPos += forward * _moveV * moveSpeed * Time.deltaTime;

            }

            if(useMouseForMovement) {
                if(Input.GetMouseButton(2)) {
                    Vector3 start = Vector3.zero;
                    Vector3 end = Vector3.zero;

                    Ray r = _camera.ScreenPointToRay(_lastMousePos);
                    float distance;
                    if(_floorPlane.Raycast(r, out distance)){
                        start = r.GetPoint(distance);
                    }

                    r = _camera.ScreenPointToRay(Input.mousePosition);
                    if(_floorPlane.Raycast(r, out distance)) {
                        end = r.GetPoint(distance);
                    }

                    _targetPos += (start - end) * mouseMoveSensitivity;
                }
            }

        }

        void rotateCamera() {

            _rotH = 0;
            _rotV = 0;

            if(useKeyboardInput) {
                if(Input.GetKey(KeyCode.Q)) {
                    _rotH = 1.0f;
                }
                if(Input.GetKey(KeyCode.E)) {
                    _rotH = -1.0f;
                }
            }

            if(useMouseForRotation) {
                if(Input.GetMouseButton(1)) {
                    var deltaMousePos = (Input.mousePosition - _lastMousePos);
                    _rotH += deltaMousePos.x * mouseRotationSensitivity;
                    _rotV -= deltaMousePos.y * mouseRotationSensitivity;
                }
            }
            
            transform.localEulerAngles = new Vector3(
                Mathf.Clamp(transform.localEulerAngles.x + _rotV * Time.deltaTime * rotationSpeed, 5, 80),
                transform.localEulerAngles.y + _rotH * Time.deltaTime * rotationSpeed,
                transform.localEulerAngles.z);
            
            transform.position = _targetPos;
            transform.Translate(Vector3.back * _targetDistance);
        }

        private void zoomCamera() {
            _moveH = Input.GetAxisRaw("Mouse ScrollWheel") * 500;
            Vector3 pos = transform.position + transform.forward * (_moveH * zoomSpeed * Time.deltaTime);

            Zoom = Mathf.Clamp(Vector3.Distance(pos, _targetPos), minZoom, maxZoom);

            transform.position = _targetPos - transform.forward * Zoom;
        }

        private void OnDrawGizmos() {

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(_targetPos, 1);
        }
        
    }
}