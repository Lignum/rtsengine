﻿using RTS.Game;
using Sebastian.Geometry;
//using Shared.MeshUtil;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

namespace RTS.Map {

    public class MapCreator : MonoBehaviour {

        public static Vector2Int[] NEIGHTBOURS = new Vector2Int[] { Vector2Int.up, Vector2Int.right, Vector2Int.down, Vector2Int.left };

        [Header("Save data")]
        [SerializeField]
        string meshSaveDirectory;
        [SerializeField]
        string meshGroupName;

        [SerializeField]
        string worldLayer;

        [Header("Build data")]
        [SerializeField]
        Texture2D heightMap;

        [SerializeField]
        int floorsNum = 4;

        [SerializeField]
        float floorsHeight = 10;


        [Header("Prefabs")]
        [SerializeField]
        MeshFilter floorPre;

        [SerializeField]
        Mesh wallMesh;
        [SerializeField]
        Mesh wallCornerMesh;

        [SerializeField]
        MeshFilter wallPre;


        Color[] _pixels;
        int _width;
        int _height;
        HashSet<Section> _sections;

        Dictionary<Section, Transform> createdSections;
        

        public void createObjects() {

            while(transform.childCount > 0) {
                DestroyImmediate(transform.GetChild(0).gameObject);
            }

            _pixels = heightMap.GetPixels();
            _width = heightMap.width;
            _height = heightMap.height;

            createdSections = new Dictionary<Section, Transform>();


            calculateZones();
            createFloors();
            createWalls();
            
            AssetDatabase.SaveAssets();
        }


        void calculateZones() {
            Vector2Int pos = Vector2Int.zero;
            Color c;

            HashSet<Vector2Int>[] positions = new HashSet<Vector2Int>[floorsNum + 1];
            for(int i = 0; i < positions.Length; i++) {
                positions[i] = new HashSet<Vector2Int>();
            }

            int floor;

            for(int x = 0; x < _width; x++) {
                for(int y = 0; y < _height; y++) {

                    c = _pixels[(y * _width) + x];
                    floor = Mathf.RoundToInt(c.r * floorsNum);

                    pos.x = x;
                    pos.y = y;

                    for(int i = floor; i >= 0; i--) {
                        positions[i].Add(pos);
                    }
                }
            }
            
            _sections = new HashSet<Section>();
            
            for(int i = 0; i < positions.Length; i++) {
                HashSet<Vector2Int> floorPositions = positions[i];

                while(floorPositions.Count > 0) {
                    _sections.Add(new Section(i, createSection(floorPositions)));
                    
                }
            }
            

        }

        HashSet<Vector2Int> createSection(HashSet<Vector2Int> positions) {
            HashSet<Vector2Int> section = new HashSet<Vector2Int>();


            Vector2Int start = Vector2Int.zero;
            foreach(var item in positions) {
                start = item;
                break;
            }
            section.Add(start);
            positions.Remove(start);
            
            Queue<Vector2Int> pending = new Queue<Vector2Int>();
            pending.Enqueue(start);

            Vector2Int current;
            Vector2Int pos;

            while(pending.Count > 0) {
                current = pending.Dequeue();

                foreach(var dir in NEIGHTBOURS) {
                    pos = current + dir;

                    //Debug.Log($"{current} -> {pos}");
                    
                    if(positions.Contains(pos)) {
                        //Debug.DrawRay(new Vector3(pos.x, 0, pos.y), Vector3.up * 10, Color.green, 5);
                        pending.Enqueue(pos);
                        section.Add(pos);

                        positions.Remove(pos);
                    }

                }
            }

            return section;
        }

        void createFloors() {

            Mesh m;
            MeshFilter created;

            int c = 0;
            //Triangulator triangulator;

            foreach(var s in _sections) {

                //m = Triangulator.TriangulateToMesh( new List<Vector2>(s.MeshCorners), Quaternion.Euler(90, 0, 0));
                m = Triangulator.Triangulate(s.Polygon, -floorsHeight);

                created = Instantiate(floorPre);
                created.mesh = m;
                created.GetComponent<MeshCollider>().sharedMesh = m;

                created.transform.SetParent(transform);
                created.transform.localPosition = Vector3.up * s.Level * floorsHeight;
                
                created.gameObject.layer = LayerMask.NameToLayer( worldLayer);

                createdSections.Add(s, created.transform);

                AssetDatabase.CreateAsset(m, $"{meshSaveDirectory}/{meshGroupName}_Floor_{c++}.mesh");
            }
        }

        void createWalls() {

            List< CombineInstance> combineConf;
            CombineInstance combine;

            Vector3 position;
            Quaternion rotation;

            int c = 0;

            int groupCount;


            foreach(var s in _sections) {

                if(s.Level == 0) continue;

                HashSet<KeyValuePair<Vector2Int, Vector2Int>> walls = s.Walls;

                HashSet<WallElement> jWalls = jointWalls(s.Level, s.Walls, s.WallsCorners);
                HashSet<HashSet<WallElement>> groupList = groupWalls(jWalls);

                combineConf = new List<CombineInstance>();

                foreach(var group in groupList) {

                    foreach(var item in group) {
                        combine = new CombineInstance();
                        combine.mesh = item.mesh;
                        combine.transform = Matrix4x4.TRS(item.position, item.rotation, Vector3.one);

                        combineConf.Add(combine);
                    }

                    createCombineMesh(combineConf, createdSections[s], c++);
                    combineConf.Clear();
                }

                /*groupCount = 0;

                foreach(var wall in walls) {
                    combine = new CombineInstance();
                    combine.mesh = wallMesh;

                    position = wall.Key.toVector3IntZ() + RTSGameCtrl.FlatTileDes + Vector3.up * floorsHeight * (s.Level - 1);
                    rotation = Quaternion.LookRotation(wall.Value.toVector3IntZ());
                    combine.transform = Matrix4x4.TRS(position, rotation, Vector3.one);

                    combineConf.Add(combine);

                    groupCount++;
                    //if(groupCount >= group) {
                        createCombineMesh(combineConf, createdSections[s], c++);
                        combineConf.Clear();
                        groupCount = 0;
                   // }
                }

                createCombineMesh(combineConf, createdSections[s], c++);
                combineConf.Clear();

                foreach(var corner in s.WallsCorners) {
                    combine = new CombineInstance();
                    combine.mesh = wallCornerMesh;

                    position = corner.Key.toVector3IntZ() + RTSGameCtrl.FlatTileDes + Vector3.up * floorsHeight * (s.Level - 1); ;
                    rotation = Quaternion.LookRotation(corner.Value.toVector3IntZ());
                    combine.transform = Matrix4x4.TRS(position, rotation, Vector3.one);

                    combineConf.Add(combine);
                }
                createCombineMesh(combineConf, createdSections[s], c++);*/



            }
        }

        HashSet<WallElement> jointWalls(int level, HashSet<KeyValuePair<Vector2Int, Vector2Int>> walls, HashSet<KeyValuePair<Vector2Int, Vector2Int>> corners) {
            HashSet<WallElement> elements = new HashSet<WallElement>();

            foreach(var wall in walls) {
                WallElement element = new WallElement();
                element.position = wall.Key.toVector3IntZ() + RTSGameCtrl.FlatTileDes + Vector3.up * floorsHeight * (level - 1);
                element.rotation = Quaternion.LookRotation(wall.Value.toVector3IntZ());
                element.mesh = wallMesh;

                elements.Add(element);
            }
            foreach(var corner in corners) {
                WallElement element = new WallElement();
                element.position = corner.Key.toVector3IntZ() + RTSGameCtrl.FlatTileDes + Vector3.up * floorsHeight * (level - 1);
                element.rotation = Quaternion.LookRotation(corner.Value.toVector3IntZ());
                element.mesh = wallCornerMesh;
                
                elements.Add(element);
            }

            return elements;
        }

        HashSet<HashSet<WallElement>> groupWalls(HashSet<WallElement> elements) {

            HashSet<HashSet<WallElement>> groupList = new HashSet<HashSet<WallElement>>();
            HashSet<WallElement> group;

            int maxVertex = 65535;

            int totalVertex = 0;

            WallElement current;

            current = elements.First();

            group = new HashSet<WallElement>();

            while(elements.Count > 0 ){

                if(current.mesh != null) {
                    totalVertex += current.mesh.vertexCount;
                }

                if(totalVertex < maxVertex && current.mesh != null) {
                    group.Add(current);
                    elements.Remove(current);

                    current = getClossestWallElement(elements, group);
                } else {
                    totalVertex = 0;

                    groupList.Add(group);
                    group = new HashSet<WallElement>();

                    if(current.mesh == null) {
                        current = elements.First();
                        elements.Remove(current);
                    }
                }
            }

            if(group.Count > 0) {
                groupList.Add(group);
            }

            Debug.Log(groupList.Count);
            return groupList;
        }

        WallElement getClossestWallElement(HashSet<WallElement> elements, HashSet<WallElement> group) {

            foreach(var gElement in group) {
                foreach(var item in elements) {
                    if(Vector3.Distance(item.position, gElement.position) < 1.25f * RTSGameCtrl.TileSize) {
                        return item;
                    }
                }
            }

            return new WallElement();
        }


        void createCombineMesh(List<CombineInstance> combineConf, Transform parent, int count) {

            MeshFilter created = Instantiate(wallPre);
            Mesh m = new Mesh();
            m.CombineMeshes(combineConf.ToArray());
            m.Optimize();

            created.sharedMesh = m;

            created.transform.SetParent(parent);

            created.gameObject.layer = LayerMask.NameToLayer(worldLayer);

            AssetDatabase.CreateAsset(m, $"{meshSaveDirectory}/{meshGroupName}_Wall_{count}.mesh");
        }

        struct WallElement {
            public Mesh mesh;
            public Vector3 position;
            public Quaternion rotation;
        }


        private void OnDrawGizmos() {
            

            if(_sections != null) {

                foreach(var sec in _sections) {
                    sec.drawGizmos(floorsHeight);
                }

            }
        }

    }
}

#endif