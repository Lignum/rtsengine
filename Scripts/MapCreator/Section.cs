﻿using RTS.Game;
using Sebastian.Geometry;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

namespace RTS.Map {
    public class Section {

        public static Vector2Int[] NEIGHTBOURS = new Vector2Int[] {
            Vector2Int.up + Vector2Int.left, Vector2Int.up, Vector2Int.up + Vector2Int.right, Vector2Int.right,
            Vector2Int.down + Vector2Int.right, Vector2Int.down, Vector2Int.down + Vector2Int.left, Vector2Int.left};
            
        //public static Vector2Int[] NEIGHTBOURS = new Vector2Int[] { Vector2Int.up, Vector2Int.right, Vector2Int.down, Vector2Int.left };
        public static Vector2Int[] WALL_CORNERS = new Vector2Int[] {
            Vector2Int.down, Vector2Int.left, Vector2Int.up, Vector2Int.right};

        static Vector2 meshCornerDes = new Vector2(.5f, .5f);

        public int Level;

        public HashSet<Vector2Int> Positions;

        //public HashSet<Vector2> MeshCorners;
        public Polygon Polygon;

        public HashSet<KeyValuePair<Vector2Int, Vector2Int>> Walls;
        public HashSet<KeyValuePair<Vector2Int, Vector2Int>> WallsCorners;


        public Section(int level, HashSet<Vector2Int> positions) {

            Level = level;
            Positions = positions;
            
            HashSet<Vector2Int> border = calculateBorders();

            calculateMeshCorners(border);
            calculateWalls(border);
        }

        HashSet<Vector2Int> calculateBorders() {
            HashSet<Vector2Int> sides = new HashSet<Vector2Int>();

            Vector2Int pos;
            bool isSide;

            foreach(var p in Positions) {

                isSide = false;

                foreach(var dir in NEIGHTBOURS) {
                    pos = p + dir;

                    if(!Positions.Contains(pos)) {
                        isSide = true;
                        break;
                    }
                }

                if(isSide) {
                    sides.Add(p);                    
                } 
            }

            return sides;
        }

        void calculateMeshCorners(HashSet<Vector2Int> border) {
            List<Vector2> corners = new List<Vector2>();

            Vector2Int pos;

            foreach(var p in border) {

                foreach(var dir in NEIGHTBOURS) {
                    pos = p + dir;

                    if(!Positions.Contains(pos)) {
                        corners.Add(p + dir.toVector2()/2 + meshCornerDes);
                    }
                    //Debug.DrawRay(p + dir.toVector2() / 2 + meshCornerDes, Vector3.up * 10, Color.red, 5);
                    //Debug.Log($"{p} -- {dir} -> {pos} -- {p + dir.toVector2() / 2 + meshCornerDes}");
                }
                //Debug.Log("#######");
            }

            List<List<Vector2>> sections = getHoles(corners);
            List<HashSet<Vector2>> Holes = new List<HashSet<Vector2>>();

            foreach(var item in sections) {
                Holes.Add(orderVectorList(item));
            }

            Vector2[] hull = new List<Vector2>(Holes[0]).ToArray();

            List<Vector2[]> holesList = new List<Vector2[]>(Holes.Count-1);
            for(int i = 1; i < Holes.Count; i++) {                
                holesList.Add(new List<Vector2>(Holes[i]).ToArray());
            }

            Polygon = new Polygon(hull, holesList.ToArray(), true);
        }

        HashSet<Vector2> orderVectorList(List<Vector2> corners) {
            HashSet<Vector2> orderedCorners = new HashSet<Vector2>();
            Vector2 current = corners[0];
            corners.Remove(current);
            orderedCorners.Add(current);

            while(corners.Count > 0) {
                current = getClosetsCorner(current, corners);
                corners.Remove(current);
                orderedCorners.Add(current);
            }

            return orderedCorners;
        }

        Vector2 getClosetsCorner(Vector2 pos, List<Vector2> corners) {

            Vector2 closets = corners[0];

            float minDistance = Vector2.Distance(pos, closets);
            float dis;

            foreach(var item in corners) {
                dis = Vector2.Distance(pos, item);

                if(dis < minDistance) {
                    minDistance = dis;
                    closets = item;
                }

            }

            return closets;
        }

        List<List<Vector2>> getHoles(List<Vector2> corners) {
            
            List<List<Vector2>> sections = new List<List<Vector2>>();

            List<int> insideSec = new List<int>();

            foreach(var v in corners) {

                insideSec.Clear();

                for(int i = 0; i < sections.Count; i++) {
                    foreach(var item in sections[i]) {

                        if(Vector2.Distance(item, v) < RTSGameCtrl.TileSize) {
                            insideSec.Add(i);
                            break;
                        }
                    }
                }
                

                if(insideSec.Count == 0) {
                    List<Vector2> created = new List<Vector2>();
                    created.Add(v);
                    sections.Add(created);
                } else {
                    sections[insideSec[0]].Add(v);

                    List<List<Vector2>> remove = new List<List<Vector2>>();

                    for(int i = 1; i < insideSec.Count; i++) {
                        sections[insideSec[0]].AddRange(sections[insideSec[i]]);
                        remove.Add(sections[insideSec[i]]);
                    }

                    foreach(var item in remove) {
                        sections.Remove(item);
                    }

                }
            }

            sections.Sort(compareListCount);

            return sections;
        }

        int compareListCount(List<Vector2> l1, List<Vector2> l2) {
            return l2.Count - l1.Count;
        }

        void calculateWalls(HashSet<Vector2Int> border) {
            Walls = new HashSet<KeyValuePair<Vector2Int, Vector2Int>>();
            WallsCorners = new HashSet<KeyValuePair<Vector2Int, Vector2Int>>();
            
            Vector2Int pos;

            Vector2Int corner1;
            Vector2Int corner2;

            foreach(var p in border) {

                foreach(var dir in MapCreator.NEIGHTBOURS) {
                    pos = p + dir;

                    if(!Positions.Contains(pos)) {
                        Walls.Add(new KeyValuePair<Vector2Int, Vector2Int>(pos, -dir));
                    }
                }

                //Vector2Int.up, Vector2Int.right, Vector2Int.down, Vector2Int.left
                for(int i = 0; i < WALL_CORNERS.Length; i++) {

                    corner1 = WALL_CORNERS[i];
                    corner2 = WALL_CORNERS[(i + 1) % WALL_CORNERS.Length];

                    if(!Positions.Contains(p + corner1) && !Positions.Contains(p + corner2)) {
                        WallsCorners.Add(new KeyValuePair<Vector2Int, Vector2Int>( p + corner1 + corner2, MapCreator.NEIGHTBOURS[i]));
                    }

                }
            }
        }

        public void drawGizmos(float floorsHeight) {
            /*Gizmos.color = Color.blue;
            foreach(var pos in Positions) {
                Gizmos.DrawCube(pos.toVector3z() + Vector3.up * floorsHeight * Level, Vector3.one);
            }*/

            Gizmos.color = Color.yellow;
            /*foreach(var pos in MeshCorners) {
               Gizmos.DrawCube(pos.toVector3Z() + Vector3.up * floorsHeight * Level, Vector3.one);
            }*/
            
        
            Gizmos.color = Color.green;
            /*foreach(var wall in WallsCorners) {
                Gizmos.DrawCube(wall.Key.toVector3z() + meshCornerDes.toVector3Z() + Vector3.up * floorsHeight * (Level-.5f), new Vector3(1, floorsHeight, 1));
            }*/

        }
    }
}

#endif