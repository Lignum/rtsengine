﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Map {

    public class MapPlataformCreator : MonoBehaviour {
        
        [SerializeField]
        GameObject planePre;

        [SerializeField]
        GameObject wallPre;

        [SerializeField]
        GameObject cornerPre;

        [SerializeField]
        Vector2Int size = new Vector2Int(10,10);
        
        public void createPlataform() {
            Transform piece = new GameObject().transform;

            createPlane(size, piece);
            createSides(size, piece);
            createCorners(size, piece);
        }

        void createPlane(Vector2Int size, Transform parent) {

            GameObject plane = Instantiate(planePre);
            plane.transform.SetParent(parent);

            plane.transform.localScale = new Vector3(size.x, 1, size.y);
            plane.transform.localPosition = Vector3.up * 3;
        }

        void createSides(Vector2Int size, Transform parent) {

            Transform[] walls = new Transform[4];
            for(int i = 0; i < walls.Length; i++) {
                walls[i] = Instantiate(wallPre).transform;
                walls[i].SetParent(parent);
            }

            walls[0].localScale = new Vector3(size.x, 1, 1);
            walls[0].localPosition = new Vector3(0, 0, -size.y / 2f);

            walls[1].localScale = new Vector3(size.y, 1, 1);
            walls[1].localPosition = new Vector3(-size.x / 2f, 0, 0);

            walls[2].localScale = new Vector3(size.x, 1, 1);
            walls[2].localPosition = new Vector3(0, 0, size.y / 2f);
            
            walls[3].localScale = new Vector3(size.y, 1, 1);
            walls[3].localPosition = new Vector3(size.x / 2f, 0, 0);

            for(int i = 0; i < walls.Length; i++) {
                walls[i].localRotation = Quaternion.Euler(0, i * 90, 0);
            }

        }

        void createCorners(Vector2Int size, Transform parent) {

            Transform[] corners = new Transform[4];

            for(int i = 0; i < corners.Length; i++) {
                corners[i] = Instantiate(cornerPre).transform;
                corners[i].SetParent(parent);
            }
            corners[0].localPosition = new Vector3(size.x / 2f, 0, -size.y / 2f);
            corners[0].localRotation = Quaternion.Euler(0, 0, 0);

            corners[1].localPosition = new Vector3(-size.x / 2f, 0, -size.y / 2f);
            corners[1].localRotation = Quaternion.Euler(0, 90, 0);

            corners[2].localPosition = new Vector3(-size.x / 2f, 0, size.y / 2f);
            corners[2].localRotation = Quaternion.Euler(0, 180, 0);

            corners[3].localPosition = new Vector3(size.x / 2f, 0, size.y / 2f);
            corners[3].localRotation = Quaternion.Euler(0, 270, 0);

            /*corners[0].localPosition = new Vector3(-size.x / 2f, 0, size.y / 2f);
            corners[0].localRotation = Quaternion.Euler(0, 0, 0);

            corners[1].localPosition = new Vector3(size.x / 2f, 0, size.y / 2f);
            corners[1].localRotation = Quaternion.Euler(0, 90, 0);

            corners[2].localPosition = new Vector3(size.x / 2f, 0, -size.y / 2f);
            corners[2].localRotation = Quaternion.Euler(0, 180, 0);

            corners[3].localPosition = new Vector3(-size.x / 2f, 0, -size.y / 2f);
            corners[3].localRotation = Quaternion.Euler(0, 270, 0);*/
        }
    }
}