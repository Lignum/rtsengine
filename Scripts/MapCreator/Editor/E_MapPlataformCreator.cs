﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using RTS.Map;

namespace RTS.Editors {

    [CustomEditor(typeof(MapPlataformCreator))]
    public class E_MapPlataformCreator : Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            if(GUILayout.Button("Create Plataform")) {
                (target as MapPlataformCreator).createPlataform();
            }
        }
    }
}