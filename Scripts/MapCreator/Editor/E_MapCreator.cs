﻿using RTS.Map;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace RTS.Editors {

    [CustomEditor(typeof(MapCreator))]
    public class E_MapCreator : Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            if(GUILayout.Button("Create Map")) {
                (target as MapCreator).createObjects();
            }
        }
    }
}
   